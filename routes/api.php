<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Rutas para autenticacion e informacion de usuario.
//..plataforma.com/auth/...
Route::group([

    'middleware' => ['api', 'cors'],
    'prefix' => 'auth'

], function ($router) {

    //Login
    Route::post('login', 'AuthController@login');

    //Logout
    Route::post('logout', 'AuthController@logout');

    //Refresh token
    Route::post('refresh', 'AuthController@refresh');

    //Informacion de usuario
    Route::post('me', 'AuthController@me');
    
});

//Rutas de recursos de plataforma
//..plataforma.com/res/...
Route::group([

    'middleware' => 'api',
    'prefix' => 'res'

], function ($router) {

    //Rutas para departamentos (Resources contiene los request: GET, POST, PUT, DELETE)
    Route::resource('departamentos', 'DepartamentoController');

    //Rutas para derechos de usuarios 
    Route::resource('derechos', 'DerechoController');

    //Rutas para roles
    Route::resource('roles', 'RoleController');

    //Rutas para sistemas
    Route::resource('sistemas', 'SistemaController');

    //Rutas para UserDerechoSistema
    // Route::resource('user_derecho_sistema', 'UserDerechoSistemaController');

    //Ruta para obtener lista de usuarios
    Route::get('users','UserController@getUsers');

    //Ruta para crear nuevos usuarios con sus sistemas, departamentos y derechos respectivos.
    Route::post('createUser','UserController@createUser');

    //Ruta para editar usuarios
    Route::post('editUser','UserController@editUser');

    //Rutas para usuarios
    Route::resource('user','UserController');

    


    
});

Route::group([

    'middleware' => 'api',
    'prefix' => 'desk'

], function ($router) {

    //Desk
    Route::resource('adjunto', 'AdjuntoController');
    Route::resource('comentario', 'ComentarioController');
    Route::resource('edificio', 'EdificioController');
    Route::resource('estado_ticket', 'EstatoTicketController');
    Route::resource('prioridad', 'PrioridadController');
    Route::resource('reporte', 'ReporteController');
    Route::resource('ticket', 'TicketController');
    Route::resource('ticket_estado_ticket', 'TicketEstadoTicketController');
    Route::resource('ticket_prioridad', 'TicketPrioridadController');
    Route::resource('ticket_reporte', 'TicketReporteController');
    Route::resource('tipo_edificio', 'TipoEdificioController');
    Route::resource('tipo_ticket', 'TipoTicketController');

    Route::get('tiposTicket', 'TicketController@getTipoTicket');

    Route::get('ticketActive', 'TicketController@getActiveTickets');
    Route::get('ticketInactive', 'TicketController@getInactiveTickets');

    Route::get('deskData', 'TicketController@getDeskData');
});

Route::group([

    'middleware' => 'api',
    'prefix' => 'adq'

], function ($router) {

    //ADQ
    // Route::get('cpctest', 'ProductoCPCController@addTipoUserProceso');
    Route::post('updateProceso', 'ProcesoController@update');
    Route::get('adqData', 'ProcesoController@getData');
    Route::get('getDataAuto', 'ProcesoController@getDataAuto');

    Route::get('getDept', 'ProcesoController@getDept');
    Route::get('getCPCS', 'ProcesoController@getCPCS');
    Route::get('getAuthDoc', 'ProcesoController@getPdf');

    Route::post('adqStore', 'ProcesoController@store');
    
    Route::get('getInfimas', 'ProcesoController@getInfimas');
    Route::get('getAutogestion', 'ProcesoController@getAutogestion');
    Route::post('verifCPC', 'ProcesoController@verifCPC');
    Route::post('searchProc', 'ProcesoController@searchProc');


    Route::post('item', 'ItemsController@store');
    Route::delete('item/{id}', 'ItemsController@destroy');
    Route::put('item/{id}', 'ItemsController@update');

    Route::post('cotizacion', 'CotizacionController@store');
    Route::delete('cotizacion/{id}', 'CotizacionController@destroy');
    Route::put('cotizacion/{id}', 'CotizacionController@update');

    // Route::get('proveedores', 'ProveedorController@index');
    Route::resource('proveedor', 'ProveedorController');
    Route::post('searchProv', 'ProveedorController@searchProveedor');

    Route::post('cuadro_comparativo', 'CuadroComparativoController@store');
    Route::put('cuadro_comparativo', 'CuadroComparativoController@update');
    Route::put('cuadro_comparativo/{id}', 'CuadroComparativoController@update');
    Route::get('cuadro_comparativo/{id}/aprov', 'CuadroComparativoController@completarCuadComp');

    Route::post('resolucion_compra', 'ResolucionCompraController@store');
    Route::put('resolucion_compra/{id}', 'ResolucionCompraController@update');

    Route::get('control', 'ControlController@index');


    Route::get('cpcGet', 'ProductoCPCController@getCPCcsv');

    Route::put('cpcUpdate/{id}', 'ProductoCPCController@update');

    Route::post('cpcSave', 'ProductoCPCController@saveCPC');


    Route::get('addTipoUserProceso', 'ProductoCPCController@addTipoUserProceso');

    //Ruta para obtener lista de gerencia
    Route::get('getConfigAdq','UserSistemasControlController@getConfigAdq');

    //Ruta para guardar configuracion
    Route::post('saveConfig','UserSistemasControlController@postConfigAdq');

    Route::post('fillControl','ControlController@fillControl');

    Route::put('ordenUpdate/{id}', 'ResolucionCompraController@updateOrden');

    Route::post('reportes', 'ProcesoController@generarReporte');


    

    // Route::get('files/infima/{filename}',function($filename){
    //     $path=storage_path($filename);
    
    //     if(!\File::exists($path)){
    //         abort(404);
    //     }
    
    //     $file = \File::get($path);
    
    //     $type = \File::mimeType($path);
    
    //     $response = Response::make($file,200);
    
    //     $response->header("Content-Type",$type);
    
    //     return $response;
    // });
});


// Route::get('storage/{filename}', function ($filename)
// {
//     $path = storage_path('public/' . $filename);

//     if (!File::exists($path)) {
//         abort(404);
//     }

//     $file = File::get($path);
//     $type = File::mimeType($path);

//     $response = Response::make($file, 200);
//     $response->header("Content-Type", $type);

//     return $response;
// });
