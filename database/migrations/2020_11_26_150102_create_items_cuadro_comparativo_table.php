<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsCuadroComparativoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_cuadro_comparativo', function (Blueprint $table) {
            $table->id();

            //Referencia items
            $table->bigInteger('item_id')->unsigned()->nullable();
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');

            //Referencia items
            $table->bigInteger('cuadro_comparativo_id')->unsigned()->nullable();
            $table->foreign('cuadro_comparativo_id')->references('id')->on('cuadro_comparativo')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_cuadro_comparativo');
    }
}
