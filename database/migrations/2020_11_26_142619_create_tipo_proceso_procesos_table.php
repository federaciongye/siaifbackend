<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipoProcesoProcesosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_proceso_procesos', function (Blueprint $table) {
            $table->id();

            //Referencia tipo proceso
            $table->bigInteger('tipo_proceso_id')->unsigned()->nullable();
            $table->foreign('tipo_proceso_id')->references('id')->on('tipo_proceso')->onDelete('cascade');

            //Referencia proceso
            $table->bigInteger('proceso_id')->unsigned()->nullable();
            $table->foreign('proceso_id')->references('id')->on('procesos')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_proceso_procesos');
    }
}
