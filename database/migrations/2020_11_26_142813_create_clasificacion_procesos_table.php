<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClasificacionProcesosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clasificacion_procesos', function (Blueprint $table) {
            $table->id();

            //Referencia clasificacion
            $table->bigInteger('clasificacion_id')->unsigned()->nullable();
            $table->foreign('clasificacion_id')->references('id')->on('clasificacion')->onDelete('cascade');

            //Referencia proceso
            $table->bigInteger('proceso_id')->unsigned()->nullable();
            $table->foreign('proceso_id')->references('id')->on('procesos')->onDelete('cascade');

            $table->longText('plazo')->nullable();
            $table->longText('f_pago')->nullable();
            $table->longText('f_emisión')->nullable();
            $table->longText('f_aprobado')->nullable();
            $table->longText('observaciones')->nullable();
            $table->longText('file_ordenes')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clasificacion_procesos');
    }
}
