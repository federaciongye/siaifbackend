<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosMontosContratacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos_montos_contratacion', function (Blueprint $table) {
            $table->id();

            //Referencia clasificacion
            $table->bigInteger('monto_contratacion_id')->unsigned()->nullable();
            $table->foreign('monto_contratacion_id')->references('id')->on('montos_contratacion')->onDelete('cascade');

            //Referencia clasificacion
            $table->bigInteger('producto_id')->unsigned()->nullable();
            $table->foreign('producto_id')->references('id')->on('productos_cpc')->onDelete('cascade');

            $table->longText('acumulado')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos_montos_contratacion');
    }
}
