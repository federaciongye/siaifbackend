<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcesosUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procesos_usuarios', function (Blueprint $table) {
            $table->id();

            //Referencia user
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            //Referencia proceso
            $table->bigInteger('proceso_id')->unsigned()->nullable();
            $table->foreign('proceso_id')->references('id')->on('procesos')->onDelete('cascade');

            //Referencia tipo usuario proceso
            $table->bigInteger('tipo_usuario_proceso_id')->unsigned()->nullable();
            $table->foreign('tipo_usuario_proceso_id')->references('id')->on('tipo_usuario_proceso')->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procesos_usuarios');
    }
}
