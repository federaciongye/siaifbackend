<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsCotizacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_cotizaciones', function (Blueprint $table) {
            $table->id();

            //Referencia cotizaciones
            $table->bigInteger('cotizacion_id')->unsigned()->nullable();
            $table->foreign('cotizacion_id')->references('id')->on('cotizaciones')->onDelete('cascade');

            //Referencia item
            $table->bigInteger('item_id')->unsigned()->nullable();
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');

            $table->longText('p_unitario')->nullable();
            $table->longText('desc_porcentaje')->nullable();
            $table->longText('desc_valor')->nullable();
            $table->longText('subtotal')->nullable();
            $table->longText('iva')->nullable();
            $table->longText('total')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_cotizaciones');
    }
}
