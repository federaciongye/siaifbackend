<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEdificioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edificios', function (Blueprint $table) {
            $table->id();
            $table->longText('nombre')->nullable();
            $table->longText('descripcion')->nullable();
            $table->longText('direccion')->nullable();

            //Referencia tipo edificio
            $table->bigInteger('tipo_edificio_id')->unsigned()->nullable();
            $table->foreign('tipo_edificio_id')->references('id')->on('tipos_edificio')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edificio');
    }
}
