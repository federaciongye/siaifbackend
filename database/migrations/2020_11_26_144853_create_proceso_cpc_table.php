<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcesoCpcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proceso_productos_cpc', function (Blueprint $table) {
            $table->id();

            //Referencia procesos
            $table->bigInteger('proceso_id')->unsigned()->nullable();
            $table->foreign('proceso_id')->references('id')->on('procesos')->onDelete('cascade');

            //Referencia producto cpc
            $table->bigInteger('producto_cpc_id')->unsigned()->nullable();
            $table->foreign('producto_cpc_id')->references('id')->on('productos_cpc')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proceso_cpc');
    }
}
