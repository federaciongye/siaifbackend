<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSistemasControlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_sistemas_control', function (Blueprint $table) {
            $table->id();

            //Referencia user_sistemas
            $table->bigInteger('sistema_user_id')->unsigned()->nullable();
            $table->foreign('sistema_user_id')->references('id')->on('sistema_user');

            //Referencia control
            $table->bigInteger('control_id')->unsigned()->nullable();
            $table->foreign('control_id')->references('id')->on('control');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_sistemas_control');
    }
}
