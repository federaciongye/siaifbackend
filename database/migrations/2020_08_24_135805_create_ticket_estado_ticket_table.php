<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketEstadoTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_estado_ticket', function (Blueprint $table) {
            $table->id();

            //Referencia estado ticket
            $table->bigInteger('estado_ticket_id')->unsigned()->nullable();
            $table->foreign('estado_ticket_id')->references('id')->on('estados_ticket')->onDelete('cascade');

            //Referencia ticket
            $table->bigInteger('ticket_id')->unsigned()->nullable();
            $table->foreign('ticket_id')->references('id')->on('tickets')->onDelete('cascade');

            //Referencia tecnico
            $table->bigInteger('tech_id')->unsigned()->nullable();
            $table->foreign('tech_id')->references('id')->on('users')->onDelete('cascade');

            //Referencia admin (superior de tecnico)
            $table->bigInteger('admin_id')->unsigned()->nullable();
            $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_estado_ticket');
    }
}
