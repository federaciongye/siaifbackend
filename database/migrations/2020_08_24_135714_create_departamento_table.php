<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartamentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departamentos', function (Blueprint $table) {
            $table->id();
            $table->longText('codigo')->nullable();
            $table->longText('nombre')->nullable();
            $table->longText('descripcion')->nullable();

            //Referencia departamento superior
            $table->bigInteger('dep_sup_id')->unsigned()->nullable();
            $table->foreign('dep_sup_id')->references('id')->on('departamentos')->onDelete('cascade');

            //Referencia tipo edificio
            $table->bigInteger('edificio_id')->unsigned()->nullable();
            $table->foreign('edificio_id')->references('id')->on('edificios')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departamento');
    }
}
