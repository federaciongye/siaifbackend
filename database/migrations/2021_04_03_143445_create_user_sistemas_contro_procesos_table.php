<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSistemasControProcesosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_sistemas_control_procesos', function (Blueprint $table) {
            $table->id();

            //Referencia proceso
            $table->bigInteger('proceso_id')->unsigned()->nullable();
            $table->foreign('proceso_id')->references('id')->on('procesos');

            //Referencia user_sistemas_control
            $table->bigInteger('user_sistemas_control_id')->unsigned()->nullable();
            $table->foreign('user_sistemas_control_id')->references('id')->on('user_sistemas_control');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_sistemas_contro_procesos');
    }
}
