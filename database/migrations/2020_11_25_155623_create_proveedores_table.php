<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProveedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proveedores', function (Blueprint $table) {
            $table->id();
            $table->longText('nombre_comercial',1500)->nullable();
            $table->longText('razon_social',1500)->nullable();
            $table->longText('nombres',1500)->nullable();
            $table->longText('apellidos',1500)->nullable();
            $table->longText('ruc',1500)->nullable();
            $table->longText('telefono',1500)->nullable();
            $table->longText('celular',1500)->nullable();
            $table->longText('email_personal',1500)->nullable();
            $table->longText('email_empresa',1500)->nullable();
            $table->longText('observaciones',1500)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proveedores');
    }
}
