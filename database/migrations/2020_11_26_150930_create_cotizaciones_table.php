<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCotizacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cotizaciones', function (Blueprint $table) {
            $table->id();

            //Referencia cuadro comparativo
            $table->bigInteger('cuadro_comparativo_id')->unsigned()->nullable();
            $table->foreign('cuadro_comparativo_id')->references('id')->on('cuadro_comparativo')->onDelete('cascade');

            //Referencia proveedor
            $table->bigInteger('proveedor_id')->unsigned()->nullable();
            $table->foreign('proveedor_id')->references('id')->on('proveedores')->onDelete('cascade');

            $table->boolean('califica')->default(true);
            $table->longText('garantia')->nullable();
            $table->longText('f_pago')->nullable();
            $table->longText('f_entrega')->nullable();
            $table->boolean('seleccionado')->default(false);
            $table->longText('observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cotizaciones');
    }
}
