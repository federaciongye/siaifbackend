<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemCpcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_cpc', function (Blueprint $table) {
            $table->id();

            //Referencia producto coc
            $table->bigInteger('producto_cpc_id')->unsigned()->nullable();
            $table->foreign('producto_cpc_id')->references('id')->on('productos_cpc')->onDelete('cascade');

            //Referencia items
            $table->bigInteger('item_id')->unsigned()->nullable();
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_cpc');
    }
}
