<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();

            $table->longText('codigo')->nullable();

            $table->longText('descripcion')->nullable();
            $table->boolean('activo')->default(true);
            
            //if evento
            $table->boolean('evento')->default(false);
            $table->date('fecha_evento')->nullable();

            //calificacion
            // $table->integer('calificacion');
            // $table->longText('comentario')->nullable();

            //Referencia usuario
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            //Referencia usuario
            $table->bigInteger('tipo_ticket_id')->unsigned()->nullable();
            $table->foreign('tipo_ticket_id')->references('id')->on('tipos_ticket')->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
