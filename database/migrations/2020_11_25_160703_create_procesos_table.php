<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcesosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procesos', function (Blueprint $table) {
            $table->id();
            $table->longText('fecha_inicio')->nullable();
            $table->longText('objeto')->nullable();
            $table->longText('codigo')->nullable();
            $table->longText('observaciones')->nullable();
            $table->longText('autorizado_scan')->nullable(); 

            //Referencia regularizacion
            $table->bigInteger('regularizacion_id')->unsigned()->nullable();
            $table->foreign('regularizacion_id')->references('id')->on('regularizaciones')->onDelete('cascade');

            //Referencia user revisa
            $table->bigInteger('user_revisa_id')->unsigned()->nullable();
            $table->foreign('user_revisa_id')->references('id')->on('users');

            //Referencia user autoriza
            $table->bigInteger('user_autoriza_id')->unsigned()->nullable();
            $table->foreign('user_autoriza_id')->references('id')->on('users');

            $table->longText('fecha_verif_cpc')->nullable();
            $table->longText('verif_cpc_file')->nullable();
            // $table->longText('fecha_emision')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procesos');
    }
}
