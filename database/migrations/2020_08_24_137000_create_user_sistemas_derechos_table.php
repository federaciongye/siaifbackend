<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSistemasDerechosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sistema_user_derecho', function (Blueprint $table) {
            $table->id();

            //Referencia derechos
            $table->bigInteger('derecho_id')->unsigned()->nullable();
            $table->foreign('derecho_id')->references('id')->on('derechos')->onDelete('cascade');

            //Referencia user_sistemas
            $table->bigInteger('user_sistema_id')->unsigned()->nullable();
            $table->foreign('user_sistema_id')->references('id')->on('sistema_user')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_sistemas_derechos');
    }
}
