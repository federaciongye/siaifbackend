<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResolucionInfimaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resolucion_compra', function (Blueprint $table) {
            $table->id();

            $table->longText('fecha_aprobado')->nullable();

            //Referencia Cotizacion ganadora
            $table->bigInteger('cot_id')->unsigned()->nullable();
            $table->foreign('cot_id')->references('id')->on('cotizaciones')->onDelete('cascade');

             //Referencia procesos
             $table->bigInteger('proceso_id')->unsigned()->nullable();
             $table->foreign('proceso_id')->references('id')->on('procesos')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resolucion_infima');
    }
}
