<?php

use Illuminate\Database\Seeder;
use App\Clasificacion;


class ClasificacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $clasificacion1 = Clasificacion::create([
            'nombre' => "Bienes",
            'descripcion' => "...",
            'codigo' => "B"
        ]);
        $clasificacion2 = Clasificacion::create([
            'nombre' => "Servicios",
            'descripcion' => "...",
            'codigo' => "S"
        ]);
        $clasificacion3 = Clasificacion::create([
            'nombre' => "Obras",
            'descripcion' => "...",
            'codigo' => "O"
        ]);
    }
}
