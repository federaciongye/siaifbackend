<?php

use Illuminate\Database\Seeder;
use App\ProductoCPC;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Departamento;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

class CpcSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv=$this->csv_to_array("../storage/app/public/csv/personal.csv",",");
        foreach ($csv as $user){
            $us=null;
            $us_sup=null;

            if($user['cedula']!=""){
                if(strlen($user['cedula'])==9){
                    $user['cedula']="0".$user['cedula'];
                }
                $splitName=explode(' ',$user['nombres']);
                $splitLastName=explode(' ',$user['apellidos']);
                $us=User::create(['name' => ucwords(mb_strtolower($user['nombres'],'UTF-8')),
                'last_name'=>ucwords(mb_strtolower($user['apellidos'],'UTF-8')), 
                'cargo'=>ucwords(mb_strtolower($user['cargo'],'UTF-8')),
                'email'=> mb_strtolower($user['correo'],'UTF-8'),
                'password' => Hash::make(mb_strtolower(substr($splitName[0], 0,3),'UTF-8')."".mb_strtolower(substr($splitLastName[0], 0,3),'UTF-8')."".substr($user['cedula'], -3).""),
                'cedula'=>$user['cedula']]);

                $jefesplt=explode(' ',$user['jefe']);
                $search=ucwords(mb_strtolower($jefesplt[0]),'UTF-8');
                $lastNames=ucwords(mb_strtolower($jefesplt[0],'UTF-8'))." ".ucwords(mb_strtolower($jefesplt[1],'UTF-8'));
                $names=ucwords(mb_strtolower($jefesplt[2],'UTF-8'))." ".ucwords(mb_strtolower($jefesplt[3],'UTF-8'));
                $us_sup = User::all()->filter(function($record) use($lastNames, $names) {
                    if(strpos($record->last_name,$lastNames) !== false && strpos($record->name,$names) !== false) {
                        return $record;
                    }
                })->first();
                $us->jefe()->associate($us_sup);
                $us->save();
            }
            else{
                $splitName=explode(' ',$user['nombres']);
                $splitLastName=explode(' ',$user['apellidos']);
                $us=User::create(['name' => ucwords(mb_strtolower($user['nombres'],'UTF-8')),
                'last_name'=>ucwords(mb_strtolower($user['apellidos'],'UTF-8')), 
                'cargo'=>ucwords(mb_strtolower($user['cargo'],'UTF-8')),
                'email'=> mb_strtolower($user['correo'],'UTF-8'),
                'password' => Hash::make(mb_strtolower(substr($splitName[0], 0,3),'UTF-8')."".mb_strtolower(substr($splitLastName[0], 0,3),'UTF-8'))]);
            }

            $split=explode(' - ',$user['departamento']);
            $dept=null;
            $dept2=null;
            if(count($split)==1){
                $clean=ucwords(mb_strtolower($split[0],'UTF-8'));
                $dept=Departamento::where('nombre',$clean)->first();
                if(!$dept){
                    $dept=Departamento::create(
                        [
                            'nombre'=>ucwords(mb_strtolower($split[0],'UTF-8')),
                            'descripcion'=>"...",
                            'dep_sup_id'=>null,
                            'edificio_id'=>null,
                            'codigo'=>$user['codigo']
                        ]
                    );
                    if($us_sup){
                        $dept_sup=Departamento::find($us_sup->departamento_id);
                        if($dept->dep_sup_id == null){
                            $dept->dep_sup()->associate($dept_sup);
                            $dept->save();
                        }
                    }
                }
                $us->departamento()->associate($dept);
                $us->save();
            }
            else{
                $clean=ucwords(mb_strtolower($split[0],'UTF-8'));
                $clean2=ucwords(mb_strtolower($split[1],'UTF-8'));

                $dept=Departamento::where('nombre',$clean)->first();
                $dept2=Departamento::where('nombre',$clean2)->first();
                if(!$dept){
                    $dept=Departamento::create(
                        [
                            'nombre'=>ucwords(mb_strtolower($split[0],'UTF-8')),
                            'descripcion'=>"...",
                            'dep_sup_id'=>null,
                            'edificio_id'=>null,
                            'codigo'=>$user['codigo']
                        ]
                    );
                }
                if(!$dept2){
                    $dept2=Departamento::create(
                        [
                            'nombre'=>ucwords(mb_strtolower($split[1],'UTF-8')),
                            'descripcion'=>"...",
                            'dep_sup_id'=>null,
                            'edificio_id'=>null,
                            'codigo'=>$user['codigo']
                        ]
                    );
                }
                $dept2->dep_sup()->associate($dept);
                $dept2->save();

                $us->departamento()->associate($dept2);
                $us->save();
            }
        }
    }

    function csv_to_array($filename, $delimiter)
    {
        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = NULL;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                if(!$header)
                    $header = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row);
                else
                    $data[] = array_combine(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $header), $row);
            }
            fclose($handle);
        }
        return $data;
    }
}
