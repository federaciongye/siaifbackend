<?php

use Illuminate\Database\Seeder;
use App\EstadoTicket;

class EstadoTicketsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $ticket1 = EstadoTicket::create([
            'nombre' => "En proceso",
            'descripcion' => "..."
        ]);
        $ticket2 = EstadoTicket::create([
            'nombre' => "Terminado",
            'descripcion' => "..."
        ]);
    }
}
