<?php

use Illuminate\Database\Seeder;
use App\TipoTicket;

class TipoTickeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $ticket1 = TipoTicket::create([
            'nombre' => "Soporte Tecnico",
            'codigo'=>"ST",
            'descripcion' => "..."
        ]);

        $ticket2 = TipoTicket::create([
            'nombre' => "Tareas Programadas",
            'codigo'=>"TP",
            'descripcion' => "..."
        ]);

        $ticket3 = TipoTicket::create([
            'nombre' => "Requerimientos",
            'codigo'=>"R",
            'descripcion' => "..."
        ]);
    }
}
