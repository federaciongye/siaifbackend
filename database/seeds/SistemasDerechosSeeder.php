<?php

use Illuminate\Database\Seeder;
use App\Sistema;

class SistemasDerechosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        // $derecho1 = Derecho::create([
        //     'nombre' => "Crear",
        //     'descripcion' => "Este derecho permite crear objetos de un sistema."
        // ]);

        // $derecho2 = Derecho::create([
        //     'nombre' => "Editar",
        //     'descripcion' => "Este derecho permite editar objetos de un sistema."
        // ]);

        // $derecho3 = Derecho::create([
        //     'nombre' => "Elimnar",
        //     'descripcion' => "Este derecho permite eliminar objetos de un sistema."
        // ]);

        // $derecho4= Derecho::create([
        //     'nombre' => "Ver",
        //     'descripcion' => "Este derecho permite ver objetos de un sistema."
        // ]);
        
        $sis1 = Sistema::create(['nombre' => 'Sistemas','descripcion'=>'Desc']);
        // $sis2 = Sistema::create(['nombre' => 'Help Desk','descripcion'=>'Desc']);
        $sis3 = Sistema::create(['nombre' => 'Departamentos','descripcion'=>'Desc']);
        $sis4 = Sistema::create(['nombre' => 'Roles','descripcion'=>'Desc']);
        $sis5 = Sistema::create(['nombre' => 'Personal','descripcion'=>'Desc']);
    }
}
