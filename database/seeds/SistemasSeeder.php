<?php

use Illuminate\Database\Seeder;
use App\Sistema;
use App\User;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Crypt;

class SistemasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
        // $sis1 = Sistema::create(['nombre' => 'Sistemas','descripcion'=>'Secciones del portal']);
        // // $sis2 = Sistema::create(['nombre' => 'Help Desk','descripcion'=>'Desc']);
        // $sis3 = Sistema::create(['nombre' => 'Departamentos','descripcion'=>'Departamentos de la institución']);
        // $sis4 = Sistema::create(['nombre' => 'Roles','descripcion'=>'Accesos y derechos']);
        // $sis5 = Sistema::create(['nombre' => 'Personal','descripcion'=>'Empleados de la institución.']);
        $sis6 = Sistema::create(['nombre' => 'Adquisiciones','descripcion'=>'Departamento que procesa las compras que se realizan en la institución por estado o por autogestión.']);
        $sis7 = Sistema::create(['nombre' => 'Nuevo proceso','descripcion'=>'Nuevo proceso de compra','sistema_sup_id'=>$sis6->id]);
        $sis8 = Sistema::create(['nombre' => 'Infimas','descripcion'=>'Compra por estado','sistema_sup_id'=>$sis6->id]);
        $sis9 = Sistema::create(['nombre' => 'Autogestión','descripcion'=>'Compra por autogestión','sistema_sup_id'=>$sis6->id]);
        $sis10 = Sistema::create(['nombre' => 'Productos','descripcion'=>'Productos CPC','sistema_sup_id'=>$sis6->id]);
        // $sis2 = Sistema::create(['nombre' => 'I']);
        $sis11 = Sistema::create(['nombre' => 'Reportes','descripcion'=>'Reportes de procesos','sistema_sup_id'=>$sis6->id]);
        $sis12 = Sistema::create(['nombre' => 'Configuraciones','descripcion'=>'Configuración de las sección','sistema_sup_id'=>$sis6->id]);


        
    }
}
