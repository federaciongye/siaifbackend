<?php

use Illuminate\Database\Seeder;
use App\TipoProceso;
use App\TipoUsuarioProceso;


class TipoProcesoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $tp1 = TipoProceso::create([
            'nombre' => "Ínfima Cuantía",
            'descripcion' => "...",
            'codigo' => "IC"
        ]);
        $tp2 = TipoProceso::create([
            'nombre' => "Autogestión",
            'descripcion' => "...",
            'codigo' => "A"
        ]);

        $tipo1 = new TipoUsuarioProceso;
        $tipo1->nombre="Requieriente";
        $tipo1->descripcion="Director/Coordinador de departamento";
        $tipo1->codigo="req";
        $tipo1->save();

        $tipo2 = new TipoUsuarioProceso;
        $tipo2->nombre="Administrador";
        $tipo2->descripcion="Director/Coordinador de departamento";
        $tipo2->codigo="adm";
        $tipo2->save();

        $tipo3 = new TipoUsuarioProceso;
        $tipo3->nombre="Encargada de proceso";
        $tipo3->descripcion="Coordinador/Analista del departamento";
        $tipo3->codigo="enc";
        $tipo3->save();

        $tipo4 = new TipoUsuarioProceso;
        $tipo4->nombre="Elaborado por";
        $tipo4->descripcion="Elabora el proceso en el sistema";
        $tipo4->codigo="elb";
        $tipo4->save();
    }
}
