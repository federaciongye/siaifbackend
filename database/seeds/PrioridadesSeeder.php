<?php

use Illuminate\Database\Seeder;
use App\Prioridad;


class PrioridadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $prioridad1 = Prioridad::create([
            'nombre' => "Baja",
            'descripcion' => "...",
            'horas' => "...",
            'dias' => "7 - 15"
        ]);
        $prioridad2 = Prioridad::create([
            'nombre' => "Media",
            'descripcion' => "...",
            'horas' => "24 - 48",
            'dias' => "..."
        ]);
        $prioridad3 = Prioridad::create([
            'nombre' => "Alta",
            'descripcion' => "...",
            'horas' => "4 - 8",
            'dias' => "..."
        ]);
        $prioridad4 = Prioridad::create([
            'nombre' => "Critica",
            'descripcion' => "...",
            'horas' => "2 - 3",
            'dias' => "..."
        ]);
    }
}
