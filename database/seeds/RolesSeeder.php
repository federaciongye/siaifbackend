<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Crypt;

use App\Sistema;
use App\Derecho;
use App\UserSistema;
use App\Departamento;


class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Usuarios & roles
        // $user3 = User::create(['name' => "Natalia",'last_name'=>"FDG", 'email'=> 'natalia@gmail.com', 'password' => Hash::make('12345678')]);
        $admin = Role::create(['name' => "Director",'descripcion'=>"NaN",'codigo'=>"001"]);
        // $user3->assignRole($admin);

        // $user2 = User::create(['name' => "Angie",'last_name'=>"Mogrovejo", 'email'=> 'angie@gmail.com', 'password' => Hash::make('12345678')]);
        $coord = Role::create(['name' => "Coordinador",'descripcion'=>"NaN",'codigo'=>"002",'rol_sup_id'=>$admin->id]);
        // $user2->assignRole($coord);

        $user1 = User::create(['name' => "Jorge Ivan",'last_name'=>"Fierro Paladines", 'email'=> 'jorge.fierro@fedeguayas.com.ec', 'password' => Hash::make('et2345muerte4')]);
        $operator = Role::create(['name' => "Analista",'descripcion'=>"NaN",'codigo'=>"003",'rol_sup_id'=>$coord->id]);
        $user1->assignRole($operator);

        $user2 = User::create(['name' => "Washington Alfredo",'last_name'=>"Guerrero Lara", 'email'=> 'washington.guerrero@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user2->assignRole($admin);

        $user3 = User::create(['name' => "Isabel",'last_name'=>"Chavez Pincay", 'email'=> 'isabel.chavez@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user3->assignRole($operator);

        $user4 = User::create(['name' => "Luis Alberto",'last_name'=>"Paucar Cevallos", 'email'=> 'luis.paucar@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user4->assignRole($coord);

        $user5 = User::create(['name' => "Shirley Valeria",'last_name'=>"Mendoza Cusme", 'email'=> 'shirley.mendoza@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user5->assignRole($coord);

        $user6 = User::create(['name' => "Carlos Arturo",'last_name'=>"Knezevich Mariscal", 'email'=> 'carlos.knezevich@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user6->assignRole($admin);

        $user7 = User::create(['name' => "Miguel Angel",'last_name'=>"Vera Pico", 'email'=> 'miguel.vera@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user7->assignRole($admin);

        $user8 = User::create(['name' => "Joselin Oslinda",'last_name'=>"Yanez Carvajal", 'email'=> 'joselin.yanez@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user8->assignRole($admin);

        $user9 = User::create(['name' => "Andres Dario",'last_name'=>"Tobon Castrellon", 'email'=> 'andres.tobon@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user9->assignRole($admin);

        $user10 = User::create(['name' => "Christian Raul",'last_name'=>"Lopez Armendariz", 'email'=> 'coordinacionsistemas@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user10->assignRole($coord);

        // $user11 = User::create(['name' => "Shirley Valeria",'last_name'=>"Mendoza Cusme", 'email'=> 'shirley.mendoza@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        // $user11->assignRole($coord);

        $user12 = User::create(['name' => "Jhony Keybin",'last_name'=>"Morales Salazar", 'email'=> 'jhony.morales@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user12->assignRole($operator);

        $user13 = User::create(['name' => "Agustin Valentin",'last_name'=>"Figueroa Salvatierra", 'email'=> 'valentin.figueroa@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user13->assignRole($operator);

        $user14 = User::create(['name' => "Sergio Armando",'last_name'=>"Mera Romero", 'email'=> 'sergio.mera@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user14->assignRole($operator);

        $user15 = User::create(['name' => "Karen Vanessa",'last_name'=>"Vargas Sanchez", 'email'=> 'karen.vargas@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user15->assignRole($operator);

        $user16 = User::create(['name' => "Jennipher Joselim",'last_name'=>"Guerrero Urquiza", 'email'=> 'jennipher.guerrero@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user16->assignRole($operator);

        $user17 = User::create(['name' => "Mariana Elizabeth",'last_name'=>"Holguin Garcia", 'email'=> 'mariana.holguin@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user17->assignRole($coord);

        $user18 = User::create(['name' => "Hector Patricio",'last_name'=>"Panta Mera", 'email'=> 'hector.panta@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user18->assignRole($operator);

        $user19 = User::create(['name' => "Carla Solange",'last_name'=>"Medina Montiel", 'email'=> 'solange.medina@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user19->assignRole($operator);

        $user20 = User::create(['name' => "Oscar Fabricio",'last_name'=>"Paucar Cevallos", 'email'=> 'oscar.paucar@fedeguayas.com.ec', 'password' => Hash::make('12345678')]);
        $user20->assignRole($operator);
        
        //Derechos
        $derecho1 = Derecho::create([
            'nombre' => "Crear",
            'descripcion' => "Este derecho permite crear objetos de un sistema."
        ]);

        $derecho2 = Derecho::create([
            'nombre' => "Editar",
            'descripcion' => "Este derecho permite editar objetos de un sistema."
        ]);

        $derecho3 = Derecho::create([
            'nombre' => "Eliminar",
            'descripcion' => "Este derecho permite eliminar objetos de un sistema."
        ]);

        $derecho4= Derecho::create([
            'nombre' => "Ver",
            'descripcion' => "Este derecho permite ver objetos de un sistema."
        ]);

        //Sistemas
        $sis3 = Sistema::create(['nombre' => 'Departamentos','descripcion'=>'Cada una de las partes que esta divida la Federación Deportiva del Guayas con actividades y roles especificas para el optimo funcionamiento al favor del deporte de la provincia.']);
        $sis1 = Sistema::create(['nombre' => 'Sistemas','descripcion'=>'Secciones del portal para el manejo de la información.']);
        $sis4 = Sistema::create(['nombre' => 'Roles','descripcion'=>'Asignación de derechos de funcionalidades en el portal.']);
        $sis5 = Sistema::create(['nombre' => 'Personal','descripcion'=>'Gestion de empleados y sus funcionalidades en el portal.']);
        // $sis2 = Sistema::create(['nombre' => 'Help Desk','descripcion'=>'Mesa de ayuda']);

        $sis6 = Sistema::create(['nombre' => 'Adquisiciones','descripcion'=>'Sistema para hacer compras en la institución.']);
        $sis7 = Sistema::create(['nombre' => 'Nuevo Proceso','descripcion'=>'Crear nuevo proceso de compras.','sistema_sup_id'=>$sis6->id]);
        $sis8 = Sistema::create(['nombre' => 'Ínfimas','descripcion'=>'Procesos realizados con recursos de estado.','sistema_sup_id'=>$sis6->id]);
        $sis9 = Sistema::create(['nombre' => 'Autogestión','descripcion'=>'Procesos realizados con recursos de Autogestión.','sistema_sup_id'=>$sis6->id]);
        $sis10 = Sistema::create(['nombre' => 'Productos','descripcion'=>'Catálogo de productos catalogados o no del SERCOP.','sistema_sup_id'=>$sis6->id]);
        $sis11 = Sistema::create(['nombre' => 'Reportes','descripcion'=>'Reportes de compras.','sistema_sup_id'=>$sis6->id]);
        $sis12 = Sistema::create(['nombre' => 'Configuraciones','descripcion'=>'Configuraciones de modulo adquisiciones.','sistema_sup_id'=>$sis6->id]);

        //Bindings

        //User 1
        $sisUser1=UserSistema::create(['user_id'=>$user1->id,'sistema_id'=>$sis1->id]);
        $sisUser2=UserSistema::create(['user_id'=>$user1->id,'sistema_id'=>$sis4->id]);
        $sisUser5=UserSistema::create(['user_id'=>$user1->id,'sistema_id'=>$sis3->id]);
        $sisUser6=UserSistema::create(['user_id'=>$user1->id,'sistema_id'=>$sis5->id]);
        $sisUser7=UserSistema::create(['user_id'=>$user1->id,'sistema_id'=>$sis6->id]);
        $sisUser8=UserSistema::create(['user_id'=>$user1->id,'sistema_id'=>$sis7->id]);
        $sisUser9=UserSistema::create(['user_id'=>$user1->id,'sistema_id'=>$sis8->id]);
        $sisUser11=UserSistema::create(['user_id'=>$user1->id,'sistema_id'=>$sis9->id]);
        $sisUser12=UserSistema::create(['user_id'=>$user1->id,'sistema_id'=>$sis10->id]);
        $sisUser13=UserSistema::create(['user_id'=>$user1->id,'sistema_id'=>$sis11->id]);
        $sisUser14=UserSistema::create(['user_id'=>$user1->id,'sistema_id'=>$sis12->id]);
        $sisUser1->derechos()->attach($derecho1);
        $sisUser1->derechos()->attach($derecho2);
        $sisUser1->derechos()->attach($derecho3);
        $sisUser1->derechos()->attach($derecho4);

        $sisUser2->derechos()->attach($derecho1);
        $sisUser2->derechos()->attach($derecho2);
        $sisUser2->derechos()->attach($derecho3);
        $sisUser2->derechos()->attach($derecho4);

        $sisUser5->derechos()->attach($derecho1);
        $sisUser5->derechos()->attach($derecho2);
        $sisUser5->derechos()->attach($derecho3);
        $sisUser5->derechos()->attach($derecho4);

        $sisUser6->derechos()->attach($derecho1);
        $sisUser6->derechos()->attach($derecho2);
        $sisUser6->derechos()->attach($derecho3);
        $sisUser6->derechos()->attach($derecho4);

        $sisUser7->derechos()->attach($derecho1);
        $sisUser7->derechos()->attach($derecho2);
        $sisUser7->derechos()->attach($derecho3);
        $sisUser7->derechos()->attach($derecho4);

        $sisUser8->derechos()->attach($derecho1);
        $sisUser8->derechos()->attach($derecho2);
        $sisUser8->derechos()->attach($derecho3);
        $sisUser8->derechos()->attach($derecho4);

        $sisUser9->derechos()->attach($derecho1);
        $sisUser9->derechos()->attach($derecho2);
        $sisUser9->derechos()->attach($derecho3);
        $sisUser9->derechos()->attach($derecho4);

        $sisUser14->derechos()->attach($derecho1);
        $sisUser14->derechos()->attach($derecho2);
        $sisUser14->derechos()->attach($derecho3);
        $sisUser14->derechos()->attach($derecho4);

        $sisUser11->derechos()->attach($derecho1);
        $sisUser11->derechos()->attach($derecho2);
        $sisUser11->derechos()->attach($derecho3);
        $sisUser11->derechos()->attach($derecho4);

        $sisUser12->derechos()->attach($derecho1);
        $sisUser12->derechos()->attach($derecho2);
        $sisUser12->derechos()->attach($derecho3);
        $sisUser12->derechos()->attach($derecho4);

        $sisUser13->derechos()->attach($derecho1);
        $sisUser13->derechos()->attach($derecho2);
        $sisUser13->derechos()->attach($derecho3);
        $sisUser13->derechos()->attach($derecho4);

        // //User 2
        // $sisUser3=UserSistema::create(['user_id'=>$user2->id,'sistema_id'=>$sis2->id]);
        // $sisUser4=UserSistema::create(['user_id'=>$user2->id,'sistema_id'=>$sis1->id]);
        // $sisUser5=UserSistema::create(['user_id'=>$user2->id,'sistema_id'=>$sis3->id]);
        // $sisUser6=UserSistema::create(['user_id'=>$user2->id,'sistema_id'=>$sis5->id]);
        // $sisUser3->derechos()->attach($derecho1);
        // $sisUser3->derechos()->attach($derecho2);
        // $sisUser3->derechos()->attach($derecho3);
        // $sisUser3->derechos()->attach($derecho4);

        // $sisUser4->derechos()->attach($derecho1);
        // $sisUser4->derechos()->attach($derecho2);
        // $sisUser4->derechos()->attach($derecho3);
        // $sisUser4->derechos()->attach($derecho4);

        // $sisUser5->derechos()->attach($derecho1);
        // $sisUser5->derechos()->attach($derecho2);
        // $sisUser5->derechos()->attach($derecho3);
        // $sisUser5->derechos()->attach($derecho4);

        // $sisUser6->derechos()->attach($derecho1);
        // $sisUser6->derechos()->attach($derecho2);
        // $sisUser6->derechos()->attach($derecho3);
        // $sisUser6->derechos()->attach($derecho4);

        // //User 3
        // $sisUser7=UserSistema::create(['user_id'=>$user3->id,'sistema_id'=>$sis2->id]);
        // $sisUser8=UserSistema::create(['user_id'=>$user3->id,'sistema_id'=>$sis1->id]);
        // $sisUser9=UserSistema::create(['user_id'=>$user3->id,'sistema_id'=>$sis3->id]);
        // $sisUser10=UserSistema::create(['user_id'=>$user3->id,'sistema_id'=>$sis5->id]);
        // $sisUser11=UserSistema::create(['user_id'=>$user3->id,'sistema_id'=>$sis4->id]);
        // $sisUser7->derechos()->attach($derecho1);
        // $sisUser7->derechos()->attach($derecho2);
        // $sisUser7->derechos()->attach($derecho3);
        // $sisUser7->derechos()->attach($derecho4);

        // $sisUser8->derechos()->attach($derecho1);
        // $sisUser8->derechos()->attach($derecho2);
        // $sisUser8->derechos()->attach($derecho3);
        // $sisUser8->derechos()->attach($derecho4);

        // $sisUser9->derechos()->attach($derecho1);
        // $sisUser9->derechos()->attach($derecho2);
        // $sisUser9->derechos()->attach($derecho3);
        // $sisUser9->derechos()->attach($derecho4);

        // $sisUser10->derechos()->attach($derecho1);
        // $sisUser10->derechos()->attach($derecho2);
        // $sisUser10->derechos()->attach($derecho3);
        // $sisUser10->derechos()->attach($derecho4);

        // $sisUser11->derechos()->attach($derecho1);
        // $sisUser11->derechos()->attach($derecho2);
        // $sisUser11->derechos()->attach($derecho3);
        // $sisUser11->derechos()->attach($derecho4);

        //Departamentos

        $dept = Departamento::create([
            'codigo' => "PSDC",
            'nombre' => "Presidencia",
        'descripcion'=>"Presidencia", 
        'dep_sup_id'=> null, 
        'edificio_id'=> null, ]);

        $dept2 = Departamento::create([
            'codigo' => "GG",
            'nombre' => "Gerencia General",
        'descripcion'=>"Gerencia General", 
        'dep_sup_id'=> $dept->id, 
        'edificio_id'=> null, ]);

        $dept3 = Departamento::create([
            'codigo' => "ADM",
            'nombre' => "Administrativo",
        'descripcion'=>"Administrativo", 
        'dep_sup_id'=> $dept2->id, 
        'edificio_id'=> null, ]);

        $dept4 = Departamento::create([
            'codigo' => "FN",
            'nombre' => "Financiero",
        'descripcion'=>"Financiero", 
        'dep_sup_id'=> $dept3->id, 
        'edificio_id'=> null, ]);

        $dept5 = Departamento::create([
            'codigo' => "ADQ",
            'nombre' => "Adquisiciones",
        'descripcion'=>"Adquisiciones", 
        'dep_sup_id'=> $dept3->id, 
        'edificio_id'=> null, ]);

        $dept6 = Departamento::create([
            'codigo' => "SIS",
            'nombre' => "Informática y Soporte",
        'descripcion'=>"Informática y Soporte", 
        'dep_sup_id'=> $dept2->id, 
        'edificio_id'=> null, ]);

        $dept7 = Departamento::create([
            'codigo' => "DTM",
            'nombre' => "Técnico Metodológico",
        'descripcion'=>"Técnico Metodológico", 
        'dep_sup_id'=> $dept2->id, 
        'edificio_id'=> null, ]);

        $dept8 = Departamento::create([
            'codigo' => "ADMB",
            'nombre' => "Administración de Bienes",
        'descripcion'=>"Administración de Bienes", 
        'dep_sup_id'=> $dept3->id, 
        'edificio_id'=> null, ]);

        $dept9 = Departamento::create([
            'codigo' => "SG",
            'nombre' => "Servicios Generales",
        'descripcion'=>"Servicios Generales", 
        'dep_sup_id'=> $dept3->id, 
        'edificio_id'=> null, ]);

        $dept10 = Departamento::create([
            'codigo' => "CONT",
            'nombre' => "Contabilidad",
        'descripcion'=>"Contabilidad", 
        'dep_sup_id'=> $dept4->id, 
        'edificio_id'=> null, ]);

        $dept11 = Departamento::create([
            'codigo' => "TSR",
            'nombre' => "Tesorería",
        'descripcion'=>"Tesorería", 
        'dep_sup_id'=> $dept4->id, 
        'edificio_id'=> null, ]);

        $dept12 = Departamento::create([
            'codigo' => "BD",
            'nombre' => "Bienestar Deportivo",
        'descripcion'=>"Bienestar Deportivo", 
        'dep_sup_id'=> $dept7->id, 
        'edificio_id'=> null, ]);

        $dept13 = Departamento::create([
            'codigo' => "LD",
            'nombre' => "Logística Deportiva",
        'descripcion'=>"Logística Deportiva", 
        'dep_sup_id'=> $dept7->id, 
        'edificio_id'=> null, ]);

        $dept14 = Departamento::create([
            'codigo' => "FD",
            'nombre' => "Formación Deportiva",
        'descripcion'=>"Formación Deportiva", 
        'dep_sup_id'=> $dept7->id, 
        'edificio_id'=> null, ]);

        $dept15 = Departamento::create([
            'codigo' => "INFRA",
            'nombre' => "Infrastructura",
        'descripcion'=>"Infrastructura", 
        'dep_sup_id'=> $dept2->id, 
        'edificio_id'=> null, ]);

        $dept16 = Departamento::create([
            'codigo' => "JD",
            'nombre' => "Jurídico",
        'descripcion'=>"Jurídico", 
        'dep_sup_id'=> $dept2->id, 
        'edificio_id'=> null, ]);

        $dept17 = Departamento::create([
            'codigo' => "MKT",
            'nombre' => "Marketing",
        'descripcion'=>"Marketing", 
        'dep_sup_id'=> $dept2->id, 
        'edificio_id'=> null, ]);

        $dept18 = Departamento::create([
            'codigo' => "PLN",
            'nombre' => "Planificación",
        'descripcion'=>"Planificación", 
        'dep_sup_id'=> $dept->id, 
        'edificio_id'=> null, ]);

        $dept19 = Departamento::create([
            'codigo' => "TH",
            'nombre' => "Talento Humano",
        'descripcion'=>"Talento Humano", 
        'dep_sup_id'=> $dept->id, 
        'edificio_id'=> null, ]);

        // $dept20 = Departamento::create([
        //     'codigo' => "Washington Alfredo",
        //     'nombre' => "Washington Alfredo",
        // 'descripcion'=>"Guerrero Lara", 
        // 'dep_sup_id'=> $dept->id, 
        // 'edificio_id'=> null, ]);

        // $dept21 = Departamento::create([
        //     'codigo' => "Washington Alfredo",
        //     'nombre' => "Washington Alfredo",
        // 'descripcion'=>"Guerrero Lara", 
        // 'dep_sup_id'=> $dept->id, 
        // 'edificio_id'=> null, ]);

        // $dept22 = Departamento::create([
        //     'codigo' => "Washington Alfredo",
        //     'nombre' => "Washington Alfredo",
        // 'descripcion'=>"Guerrero Lara", 
        // 'dep_sup_id'=> $dept->id, 
        // 'edificio_id'=> null, ]);

        // $dept23 = Departamento::create([
        //     'codigo' => "Washington Alfredo",
        //     'nombre' => "Washington Alfredo",
        // 'descripcion'=>"Guerrero Lara", 
        // 'dep_sup_id'=> $dept->id, 
        // 'edificio_id'=> null, ]);

        
    }
}
