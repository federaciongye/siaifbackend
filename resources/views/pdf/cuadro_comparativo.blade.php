<!DOCTYPE html>
<html>
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CUADRO COMPARATIVO CC-{{$data->proceso->codigo}}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <style>
        body {
        font-family: 'Helvetica';
        font-size:10px;
        }
        @page { 
            margin: 20px 10px 15px; 
        }

        header {
            font-family: 'Helvetica';
            font-size:10px;
            margin: 5px;
        }

        footer {
            font-family: 'Helvetica';
            font-size:8px;
            margin: 20px;
            position: fixed;
            bottom: -45px; 
            left: 0px; 
            right: 0px;
            height: 50px; 
            text-align: center;
        }
    </style>
  </head>
  <body>
  <footer>
        <font style="font-family:arial; font-size:10px; color:#0f70b7;">
            <b>Oficina: Av. las Américas y Av. Kennedy. Telfs: 3815500. fedeguayas.com.ec. email: fdg@fedeguayas.com.ec<br>FEDEGUAYAS. Guayaquil - Ecuador
            </b>
        </font>
    </footer>
    <main>
        <table border="0" cellspacing="0" cellpadding="0" style="width:1080px;text-align:center">
            <tr>
                <td style="text-align:center;"><img  width="300px" src="{{ public_path('/storage/images/fdg2.jpg') }}"></td>
            </tr>
                <tr>
                <td style="text-align:center"><font style="font-family:arial; font-size:15px; color:#000000;"><b>FEDERACIÓN DEPORTIVA DEL GUAYAS</b></font></td>
            </tr>
                <tr>
                <td style="text-align:center"><font style="font-family:arial; font-size:15px; color:#000000;">CUADRO COMPARATIVO CC-{{$data->proceso->codigo}}</font></td>
            </tr>
        </table>
        <br>
        <table border="1" cellpadding="5px" style="width:1080px">
            <tr>
                <td style="background-color: #ececec;width: 25%;padding: 5px"><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font style="font-family:arial; font-size:15px; color:#000000;">OBJETO</font></td>
                <td>{{$data->proceso->objeto}}</td>
            </tr>
        </table>
        <br>
        <table border="1" cellpadding="5px" style="width:1080px;font-size: 9px">
        <tr>
            @php
                $ancho = 27;
                if(count($data->cotizaciones)<=2){
                    $ancho = 50;
                }
                else if(count($data->cotizaciones)==3){
                    $ancho = 40;
                }
            @endphp
            <!-- <td>{{$ancho}}</td>
            <td>{{count($data->cotizaciones)}}</td> -->
            
            @if(count($data->cotizaciones)<=2)         
                <td colspan="2" style="width: <?php echo $ancho; ?>%"></td>
            @elseif (count($data->cotizaciones)==3)
                <td colspan="2" style="width: <?php echo $ancho; ?>%"></td> 
            @else
                <td colspan="2" style="width: <?php echo $ancho; ?>%"></td>
            @endif
            @foreach($data->cotizaciones as $cot)
                <td colspan="2" style="text-align:center;width: 15%">
                    @if(!isset($cot->proveedor->nombre_comercial) && isset($cot->proveedor->razon_social))       
                        {{$cot->proveedor->razon_social}}
                    @elseif (isset($cot->proveedor->nombre_comercial) && !isset($cot->proveedor->razon_social))
                        {{$cot->proveedor->nombre_comercial}}
                    @elseif (isset($cot->proveedor->nombre_comercial) && isset($cot->proveedor->razon_social))
                        {{$cot->proveedor->razon_social}}
                    @endif
                </td>
            @endforeach
        </tr>
        <tr>
            <td style="width: <?php echo $ancho-7; ?>%"><b>DESCRIPCIÓN</b></td>
            <td style="width: 7%"><b>CANTIDAD</b></td>
            @foreach($data->cotizaciones as $cot)
                <td><b>VALOR U.</b></td><td><b>TOTAL</b></td>
            @endforeach
        </tr>
        @foreach($data->items as $it)
            <tr>
                <td>
                    {{$it->descripcion}}
                </td>
                <td>
                    {{$it->cantidad}}
                </td>
                
                @foreach($it->cotizaciones as $cot)
                    @if((int)$cot->pivot->item_id===(int)$it->id) 
                        <td>
                            {{number_format((float)$cot->pivot->p_unitario, 2, '.', '')}}
                        </td>       
                        <td>
                            {{number_format(
                                (float)($cot->pivot->p_unitario * $it->cantidad)
                                , 2, '.', '')}}
                        </td>  
                    @endif
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td colspan="2"></td>
            @foreach($data->cotizaciones as $cot)
                @php
                    $subtotal = 0;
                @endphp
                @foreach($cot->items as $it)
                    @php
                        $cant = $it->cantidad;
                        $valor = $it->pivot->p_unitario;
                        if($it->pivot->iva_ap){
                            $subtotal = $subtotal+($cant*$valor);
                        }
                    @endphp
                @endforeach
                <td><b>SUBTOTAL 12%</b></td>
                <td>{{number_format((float)$subtotal, 2, '.', '')}}
                </td>
            @endforeach
        </tr>
        <tr>
            <td colspan="2"></td>
            @foreach($data->cotizaciones as $cot)
                @php
                    $subtotal2 = 0;
                @endphp
                @foreach($cot->items as $it)
                    @php
                        $cant = $it->cantidad;
                        $valor = $it->pivot->p_unitario;
                        if(!$it->pivot->iva_ap){
                            $subtotal2 = $subtotal2+($cant*$valor);
                        }
                    @endphp
                @endforeach
                <td><b>SUBTOTAL 0%</b></td>
                <td>{{number_format((float)$subtotal2, 2, '.', '')}}</td>
            @endforeach
        </tr>
        <tr>
            <td colspan="2"></td>
            @foreach($data->cotizaciones as $cot)
                @php
                    $subtotal = 0;
                    $descuento = 0;
                @endphp
                @foreach($cot->items as $it)
                    @php
                        $cant = $it->cantidad;
                        $valor = $it->pivot->p_unitario;
                        $subtotal = $subtotal+($cant*$valor);
                    @endphp
                @endforeach
                <td><b>DESCUENTO</b></td>
                <td>0
                </td>
            @endforeach
        </tr>
        <tr>
            <td colspan="2"></td>
            @foreach($data->cotizaciones as $cot)
                @php
                    $subtotal = 0;
                @endphp
                @foreach($cot->items as $it)
                    @php
                        $cant = $it->cantidad;
                        $valor = $it->pivot->p_unitario;
                        $subtotal = $subtotal+($cant*$valor);
                    @endphp
                @endforeach
                <td><b>SUBTOTAL</b></td>
                <td>{{number_format((float)$subtotal, 2, '.', '')}}
                </td>
            @endforeach
        </tr>
        <tr>
            <td colspan="2"></td>
            @foreach($data->cotizaciones as $cot)
                @php
                    $subtotal = 0;
                    $iva=0;
                @endphp
                @foreach($cot->items as $it)
                    @php
                        $subtotal = 0;
                        $cant = $it->cantidad;
                        $valor = $it->pivot->p_unitario;
                        $subtotal = $subtotal+($cant*$valor);
                        if($it->pivot->iva_ap){
                            $iva=$iva+($subtotal*0.12);
                        }
                    @endphp
                @endforeach
                <td><b>IVA 12%</b></td>
                <td>{{number_format((float)$iva, 2, '.', '')}}
                </td>
            @endforeach
        </tr>
        <tr>
            <td colspan="2"></td>
            @foreach($data->cotizaciones as $cot)
                @php
                    $subtotal = 0;
                    $iva=0;
                    $total=0;
                @endphp
                @foreach($cot->items as $it)
                    @php
                        $cant = $it->cantidad;
                        $valor = $it->pivot->p_unitario;
                        $subtotal = $subtotal+($cant*$valor);
                        if($it->pivot->iva_ap){
                            $iva=$iva+(($cant*$valor)*0.12);
                        }
                    @endphp
                @endforeach
                @php
                    $total=$subtotal+$iva;
                @endphp
                <td><b>TOTAL</b></td>
                <td>{{number_format((float)$total, 2, '.', '')}}
                </td>
            @endforeach
        </tr>
        <tr>
            <td colspan="2"><b>Tiempo de entrega</b></td>
            @foreach($data->cotizaciones as $cot)
            <td colspan="2">{{$cot->f_entrega}}</td>
            @endforeach
        </tr>
        <tr>
            <td colspan="2"><b>Garantía</b></td>
            @foreach($data->cotizaciones as $cot)
            <td colspan="2">{{$cot->garantia}}</td>
            @endforeach
        </tr>
        <tr>
            <td colspan="2"><b>Forma de pago</b></td>
            @foreach($data->cotizaciones as $cot)
            <td colspan="2">{{$cot->f_pago}}</td>
            @endforeach
        </tr>
        <tr>
            <td colspan="2"><b>Calificación técnica de oferta</b></td>
            @foreach($data->cotizaciones as $cot)
            @if($cot->califica)
                <td colspan="2">Si</td>
            @else
                <td colspan="2">No</td>
            @endif
            
            @endforeach
        </tr>
        <tr>
            <td colspan="2"><b>Observaciones</b></td>
            @foreach($data->cotizaciones as $cot)
            <td colspan="2">{{$cot->observaciones}}</td>
            @endforeach
            
        </tr>
        
        
        </table>
        <br><br>
        <table border="0">
            <tr>
                <td><b>OBSERVACIONES GENERALES:</b>
                </td>
            </tr>
            <tr>
                <td>
                    {{$data->observaciones}}
                </td>
            </tr>
        </table>
        <br>
        <table border="0">
            <tr>
                <td style="width: 20%">
                    <b>ELABORADO POR:</b>
                </td>
                <td>
                @foreach($data->proceso->tipo_procesos_usuarios_get as $us)
                    @if($us->codigo=="elb") 
                    {{ $us->name}} {{$us->last_name}}
                    @endif
                @endforeach
                </td>
            </tr>
        </table>
    </main>
    
    
  </body>
</html>