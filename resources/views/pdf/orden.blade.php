@php
    $subtotalIVA=0;
    foreach($data->resolucion_compra->cotizacion->items as $it)
    {
        if($it->pivot->iva_ap){
            $subtotalArt=0;
            $subtotalArt=$it->pivot->p_unitario*$it->cantidad;
            $subtotalIVA=$subtotalIVA+$subtotalArt;
        }
        
    }
    $subtotalZ=0;
    foreach($data->resolucion_compra->cotizacion->items as $it)
    {
        if(!$it->pivot->iva_ap){
            $subtotalArt=0;
            $subtotalArt=$it->pivot->p_unitario*$it->cantidad;
            $subtotalZ=$subtotalZ+$subtotalArt;
        }
        
    }

    $subtotalG=$subtotalIVA+$subtotalZ;
    $IVA=$subtotalIVA*0.12;
    
    $total=number_format((float)$subtotalG, 2, '.', '')+number_format((float)$IVA, 2, '.', '');
@endphp

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>ORDEN DE 
    @if($data->clasificacion[0]->codigo=='B')
        COMPRA OC-{{$data->codigo}}
    @else
        TRABAJO OT-{{$data->codigo}}
        
    @endif    
    </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <style>
        body {
        font-family: DejaVu Sans, sans-serif; 
        font-size:10px;
        margin: 27px 15px 20px 15px; 
        }
        @page { 
            margin: 15px 15px 25px 15px; 
        }

        header {
            font-family: 'Helvetica';
            font-size:10px;
            margin: 5px;
        }

        footer {
            font-family: 'Helvetica';
            font-size:8px;
            margin: 20px;
            position: fixed;
            bottom: -45px; 
            left: 0px; 
            right: 0px;
            height: 50px; 
            text-align: center;
        }
    </style>
  </head>
  <body>
      <script type="text/php">
      if ( isset($pdf) ) {
        $w = $pdf->get_width();
        $h = $pdf->get_height();
        
        $size = 6;
        $color = array(0,0,0);
        $font = $fontMetrics->getFont("helvetica");
        $font_bold = $fontMetrics->getFont("helvetica", "bold");
        $text_height = $fontMetrics->getFontHeight($font, $size);
        $y = $h - 2 * $text_height - 24;

        // a static object added to every page
        $foot = $pdf->open_object();
        // Draw a line along the bottom
        $y += $text_height;
        $pdf->text(16, $y, "Oficina: Av. de las Américas y Av. Kennedy. Telf: (04)-3815500 fedeguayas.com.ec - FEDEGUAYAS. Guayaquil, Ecuador.", $font, $size, $color);
        $pdf->close_object();
        $pdf->add_object($foot, "all");

        // generated text written to every page after rendering
        $pdf->page_text(540, $y, "Página {PAGE_NUM} de {PAGE_COUNT}", $font, $size, $color);
      }
    </script>
    @php
        setlocale(LC_ALL, 'es_ES');
        $fecha = Carbon\Carbon::parse($data->clasificacion[0]->pivot->f_aprobado);
        $fecha->format("m"); // Inglés.
        $dia=$fecha->day;
        $ano=$fecha->year;
        $m=$fecha->month;
    @endphp
    <table style="width: 100%" border="1" cellspacing="0" cellpadding="2">
        <tr>
            <td colspan=12 style="font-size: 12px; text-align: center;background-color: #ececec"><b>ORDEN DE 
            @if($data->clasificacion[0]->codigo=='S' || $data->clasificacion[0]->codigo=='O')
                TRABAJO COD. OT-{{$data->codigo}}

            @else
                COMPRA COD. OC-{{$data->codigo}}
            @endif
            </b></td>
        </tr>
        <tr>
            <td colspan=5 style="font-size: 12px; text-align: left"><b>FECHA:</b> {{date('Y-m-d',strtotime($data->clasificacion[0]->pivot->f_aprobado))}}</td>
            <td colspan=7 style="font-size: 12px; text-align: left"><b>MONTO: USD ${{number_format((float)$subtotalG, 2, '.', '')}}
            @if(($subtotalIVA>0 && $subtotalZ == 0) || ($subtotalIVA>0 && $subtotalZ > 0))
            + IVA
            @elseif($subtotalZ>0 && $subtotalIVA==0)
            SIN IVA
            @endif 

            </b></td>
        </tr>
        <tr>
            <td rowspan="2"  style="width: 20%;padding: 5px;font-size: 12px; text-align: left;">
            @if($data->clasificacion[0]->codigo=='S')
                ___:EJEC. OBRA<br>
                ___:ADQ. BIENES<br>
                <u> X </u>:PREST. SERV.<br>
                ___:OTROS<br>
            @elseif($data->clasificacion[0]->codigo=='O')
                <u> X </u>:EJEC. OBRA<br>
                ___:ADQ. BIENES<br>
                ___:PREST. SERV.<br>
                ___:OTROS<br>
            @else
                ___:EJEC. OBRA<br>
                <u> X </u>:ADQ. BIENES<br>
                ___:PREST. SERV.<br>
                ___:OTROS<br>
            @endif    
           
            </td>
            <td colspan=11 style="font-size: 12px;width: 80%"><b>DESCRIPCIÓN:</b><br>
            {{mb_strtoupper($data->objeto, 'UTF-8')}}<br>
            </td>
            
        </tr>
        <tr>
            <td colspan=11 style="font-size: 12px"><b>PROVEEDOR:</b><br>
            {{$data->resolucion_compra->cotizacion->proveedor->razon_social}}
            </td>
        </tr>
        <tr>
            @if(isset($data->resolucion_compra->cotizacion->proveedor->nombre_comercial))
            <td colspan=3 style="font-size: 12px; text-align: left"><b>Nombre Comercial:</b></td>
            @endif   
            <td colspan=5 style="font-size: 12px; text-align: left"><b>Razón Social:</b></td>
            <td colspan=4 style="font-size: 12px; text-align: left"><b>RUC:</b></td>
        </tr>
        <tr>
            @if(isset($data->resolucion_compra->cotizacion->proveedor->nombre_comercial))
            <td colspan=3 style="font-size: 12px; text-align: left">
                
                {{$data->resolucion_compra->cotizacion->proveedor->nombre_comercial}}                
            </td>
            @endif   
            <td colspan=5 style="font-size: 12px; text-align: left">
                {{$data->resolucion_compra->cotizacion->proveedor->razon_social}}
            </td> 
            <td colspan=4 style="font-size: 12px; text-align: left">
                {{$data->resolucion_compra->cotizacion->proveedor->ruc}}
            </td>
        </tr>
        <tr>
            <td colspan=3 style="font-size: 12px; text-align: left"><b>Teléfono:</b></td>
            <td colspan=5 style="font-size: 12px; text-align: left"><b>E-mail de representante legal:</b></td>
            <td colspan=4 style="font-size: 12px; text-align: left"><b>E-mail de la empresa:</b></td>
        </tr>
        <tr>
            <td colspan=3 style="font-size: 12px; text-align: left">
            {{$data->resolucion_compra->cotizacion->proveedor->telefono}}
            </td>
            <td colspan=5 style="font-size: 12px; text-align: left">
            
            </td>
            <td colspan=4 style="font-size: 12px; text-align: left">
            {{$data->resolucion_compra->cotizacion->proveedor->email_empresa}}
            </td>
        </tr>
        <tr>
            <td colspan="12" style="font-size: 12px; text-align: left"><b>SUPERVISOR/FISCALIZACIÓN: </b> FEDERACIÓN DEPORTIVA DEL GUAYAS</td>
        </tr>
        <tr>
            <td colspan=1 style="font-size: 8px; text-align: center;background-color: #ececec">
                <b>CPC</b>
            </td>
            <td colspan=1 style="font-size: 8px; text-align: center;background-color: #ececec">
                <b>UNIDAD</b>
            </td>
            <td colspan=4 style="font-size: 8px; text-align: center;background-color: #ececec">
                <b>DESCRIPCIÓN</b>
            </td>
            <td colspan=1 style="font-size: 8px; text-align: center;background-color: #ececec">
                <b>CANTIDAD</b>
            </td>
            <td colspan=1 style="font-size: 8px; text-align: center;background-color: #ececec">
                <b>PRECIO</b>
            </td>
            <td colspan=1 style="font-size: 8px; text-align: center;background-color: #ececec">
                <b>SUBTOTAL</b>
            </td>
            <td colspan=1 style="font-size: 8px; text-align: center;background-color: #ececec">
                <b>DESC.</b>
            </td>
            <td colspan=1 style="font-size: 8px; text-align: center;background-color: #ececec">
                <b>V. TOTAL</b>
            </td>
            <td colspan=1 style="font-size: 8px; text-align: center;background-color: #ececec">
                <b>PARTIDA</b>
            </td>
        </tr>
        @php
        $globalDesc=0;
        @endphp 
        @foreach($data->resolucion_compra->cotizacion->items as $it)
            <tr>
                @if($data->tipo_procesos[0]->codigo=='IC')
                <td colspan=1 style="font-size: 8px; text-align: center;">
                    {{$it->cpc->codigo}}
                </td>
                @else
                <td colspan=1 style="font-size: 8px; text-align: center;">
                    
                </td>
                @endif
                <td colspan=1 style="font-size: 10px; text-align: center;">
                    @if($it->unidad=="㎡")
                    <img width="15px"  src="{{ public_path('/storage/images/13217.png') }}">
                    @elseif ($it->unidad=="㎢")
                    <img width="15px"  src="{{ public_path('/storage/images/13218.png') }}">
                    @elseif ($it->unidad=="㎠")
                    <img width="15px"  src="{{ public_path('/storage/images/13216.png') }}">
                    @elseif ($it->unidad=="㎦")
                    <img width="15px"  src="{{ public_path('/storage/images/13222.png') }}">
                    @elseif ($it->unidad=="㎤")
                    <img width="15px"  src="{{ public_path('/storage/images/13220.png') }}">
                    @elseif ($it->unidad=="㎥")
                    <img width="15px"  src="{{ public_path('/storage/images/13221.png') }}">
                    @elseif ($it->unidad=="㎣")
                    <img width="15px"  src="{{ public_path('/storage/images/13219.png') }}">
                    @else
                    {{$it->unidad}}
                    @endif
                    
                </td>
                <td colspan=4>
                    {{$it->descripcion}}
                </td>
                <td colspan=1 style="font-size: 8px; text-align: center;">
                    {{$it->cantidad}}
                </td>
                <td colspan=1 style="font-size: 8px; text-align: center;">
                    {{$it->pivot->p_unitario}}
                </td>
                <td colspan=1 style="font-size: 8px; text-align: center;">
                    @php
                        $subtotal=0;
                        $subtotal=$it->pivot->p_unitario * $it->cantidad;
                    @endphp 
                    {{number_format((float)($subtotal), 2, '.', '')}}
                </td>
                <td colspan=1 style="font-size: 8px; text-align: center;">
                    @php
                        $desc=0;
                        $desc=($subtotal*$it->pivot->desc_porcentaje)/100;
                    @endphp 
                    {{number_format((float)($desc), 2, '.', '')}}
                </td>
                <td colspan=1 style="font-size: 8px; text-align: center;">
                    @php
                        $tot=0;
                        $tot=$subtotal-$desc;
                    @endphp 
                    {{number_format((float)($tot), 2, '.', '')}}
                </td>
                
                
                <td colspan=1>

                </td>
            </tr>
        @endforeach
        <tr>
            <td colspan=10 style="font-size: 8px; text-align: left;">
            </td>
            <td colspan=1 style="font-size: 8px; text-align: left;background-color: #ececec">
                <b>SUB TOTAL 12%</b>
            </td>
            <td colspan=1 style="font-size: 8px; text-align: center;">
                
                {{number_format((float)$subtotalIVA, 2, '.', '')}}
                    
            </td>
            
        </tr>
        <tr>
            <td colspan=10 style="font-size: 8px; text-align: left;">
            </td>
            <td colspan=1 style="font-size: 8px; text-align: left;background-color: #ececec">
                <b>SUB TOTAL 0%</b>
            </td>
            <td colspan=1 style="font-size: 8px; text-align: center;">
                
                {{number_format((float)$subtotalZ, 2, '.', '')}}
            </td>
            
        </tr>
        <tr>
            <td colspan=10 style="font-size: 8px; text-align: left;">
            </td>
            <td colspan=1 style="font-size: 8px; text-align: left;background-color: #ececec">
                <b>DESC</b>
            </td>
            <td colspan=1 style="font-size: 8px; text-align: center;">
            </td>
            
        </tr>
        <tr>
            <td colspan=10 style="font-size: 8px; text-align: left;">
            </td>
            <td colspan=1 style="font-size: 8px; text-align: left;background-color: #ececec">
                <b>SUBTOTAL</b>
            </td>
            <td colspan=1 style="font-size: 8px; text-align: center;">
                
                {{number_format((float)$subtotalG, 2, '.', '')}}
            </td>
            
        </tr>
        <tr>
            <td colspan=10 style="font-size: 8px; text-align: left;">
            </td>
            <td colspan=1 style="font-size: 8px; text-align: left;background-color: #ececec">
                <b>IVA 12%</b>
            </td>
            <td colspan=1 style="font-size: 8px; text-align: center;">
                
                {{number_format((float)$IVA, 2, '.', '')}}
            </td>
            
        </tr>
        <tr>
            <td colspan=10 style="font-size: 8px; text-align: left;">
            </td>
            <td colspan=1 style="font-size: 8px; text-align: left;background-color: #ececec">
                <b>TOTAL</b>
            </td>
            <td colspan=1 style="font-size: 8px; text-align: center;">
                
                {{number_format((float)$total, 2, '.', '')}}
            </td>
            
        </tr>
        <tr>
            <td colspan=10 style="font-size: 8px; text-align: left;">
            </td>
            <td colspan=1 style="font-size: 8px; text-align: left;background-color: #ececec">
                <b>ITEMS</b>
            </td>
            <td colspan=1 style="font-size: 8px; text-align: center;">
            {{count($data->resolucion_compra->cotizacion->items)}}
            </td>
            
        </tr>
        <tr>
            <td colspan=12 style="font-size: 8px; text-align: left;">
                <b>PLAZO: </b> <br>
                El plazo para el cumplimiento del objeto de la Orden de 
                @if($data->clasificacion[0]->codigo=='S')
                    Trabajo,
                @else
                    Compra,
                @endif
                 a entera satisfacción de FEDERACIÓN DEPORTIVA DEL GUAYAS, es de {{$data->resolucion_compra->cotizacion->f_entrega}}  
                contados a partir de la firma de la presente orden de 
                @if($data->clasificacion[0]->codigo=='S')
                    trabajo.
                @else
                    compra.
                @endif
                <br>
                <b>PRECIO: </b> <br>
                El precio que FEDERACIÓN DEPORTIVA DEL GUAYAS cancelará al 
                @if($data->clasificacion[0]->codigo=='S')
                    CONTRATISTA,
                @else
                    PROVEEDOR,
                @endif
                 será de: <br>
                @if($subtotalZ==0 && $subtotalIVA !=0)
                    @php

                        $formatter = new Luecano\NumeroALetras\NumeroALetras();

                        $formatter->conector = '';

                    @endphp
                    <b>
                    {{
                        $formatter->toInvoice($subtotalG,2,'dólares')
                    }} DE LOS ESTADOS UNIDOS DE AMÉRICA (USD ${{number_format($subtotalG, 2, ',', '.')}} 
                    más IVA)
                    )
                @elseif($subtotalZ !=0 && $subtotalIVA ==0)
                    @php

                        $formatter = new Luecano\NumeroALetras\NumeroALetras();

                        $formatter->conector = '';

                    @endphp
                    <b>
                    {{
                        $formatter->toInvoice($subtotalG,2,'dólares')
                    }} DE LOS ESTADOS UNIDOS DE AMÉRICA (USD ${{number_format($subtotalG, 2, ',', '.')}} 
                    )
                    )
                @else
                    @php
                        $formatter = new Luecano\NumeroALetras\NumeroALetras();

                        $formatter->conector = '';
                    @endphp
                    <b>
                    {{
                        $formatter->toInvoice($subtotalG,2,'dólares')
                    }} DE LOS ESTADOS UNIDOS DE AMÉRICA (USD ${{number_format($subtotalG, 2, ',', '.')}} 
                    más IVA)
                    </b>
                    <br>
                    y de: 
                    <b>
                    {{
                        $formatter->toInvoice($subtotalG,2,'dólares')
                    }} DE LOS ESTADOS UNIDOS DE AMÉRICA (USD ${{number_format($subtotalG, 2, ',', '.')}} 
                    )
                @endif    
                    </b>
                    <br> 
                </td>
            </td>
            
            
        </tr>
        <tr>
            <td colspan=12 style="font-size: 8px; text-align: left;">
                <b>FORMA DE PAGO: </b> <br>
                {{$data->resolucion_compra->cotizacion->f_pago}}
            </td>
        </tr>
        <tr>
            <td colspan=12 style="font-size: 8px; text-align: left;">
                <b>OBLIGACIONES DE LAS PARTES: </b> <br>
                @if($data->clasificacion[0]->codigo=='S')
                    EL PROVEEDOR conviene y se obliga a lo siguiente:
                    <br>
                    Concurrir al lugar de ejecución del objeto de contratación de manera permanente a través de un profesional designado para el efecto, para garantizar el adecuado avance de los trabajos;
                    <br>Asumir la supervisión y dirección de la ejecución del trabajo, con el objeto de garantizar el cumplimiento del mismo;Seleccionar y garantizar, los materiales y la mano de obra calificada requerida para la realización de todas y cada una de las etapas del trabajo;Ejecutar el trabajo en el plazo acordado, lo que en caso de incumplimiento facultará a LA CONTRATANTE a sancionar a EL PROVEEDOR con el 1 por mil del valor total del contrato por cada día de demora injustificada, exceptuándose casos fortuitos o fuerza mayor;Mantener un control permanente sobre su personal y el desarrollo del trabajo, a fin de que no cause deterioro o de cualquier manera se afecte a las instalaciones de LA CONTRATANTE;
                    <br>Proveer a su costo y en forma oportuna todas las maquinarias, equipos y herramientas, sin excepción, que requiera su personal para la realización del trabajo contratado en forma tal que se garantice la continuidad de las labores dentro de un esquema de seguridad, eficiencia y prontitud;
                    <br>EL PROVEEDOR asume la total responsabilidad de los materiales, maquinarias, equipos y herramientas que se encuentren en el interior de las instalaciones de LA 
                    <br>CONTRATANTE, sin que ésta esté obligada a reconocer pago alguno o reposición por perdida o mal uso de estos bienes;
                    <br>EL PROVEEDOR no podrá subcontratar ni ceder ninguna parte de este CONTRATO sin previa autorización escrita de LA CONTRATANTE; de hacerlo, tal cesión o subcontratación no surtirá efecto alguno frente a LA CONTRATANTE y EL PROVEEDOR responderá por los daños y perjuicios ocasionados a la CONTRATANTE;
                    <br>EL PROVEEDOR es el responsable de mantener permanentemente barreras, letreros, luces y señalización adecuada y en general todo medio de seguridad en la ejecución de la obra, que prevenga a terceros de sufrir accidentes;
                    <br>EL PROVEEDOR y todo su personal cumplirán con la legislación sobre seguridad industrial y salud ocupacional en la ejecución de obra objeto de contratación, las consecuencias que se originen por omisión de la legislación sobre seguridad industrial y salud ocupacional serán de responsabilidad del PROVEEDOR; y, En el caso de que se produzcan daños y/o perjuicios a la propiedad, personal del CONTRATISTA y/o terceros durante la ejecución de los trabajos contratados, EL PROVEEDOR será el único responsable de resarcir los daños o perjuicios ocasionados; y, por ninguna razón podrá endilgar responsabilidad de LA CONTRATANTE por tales circunstancias.
                    
                @else
                    EL PROVEEDOR conviene y se obliga a lo siguiente:
                    <br>
                    *Los materiales estarán conformes con las especificaciones técnicas entregadas por la CONTRATANTE.*
                    <br>
                    El PROVEEDOR responderá directamente por la calidad del o los materiales que entregue a la COMPAÑÍA, los que en todo caso deberán cumplir con las especificaciones contenidas en su oferta.
                    <br>
                    *El PROVEEDOR es responsable de daños causados por embalajes inadecuados. No se aceptan gastos de embalaje, salvo que ellos sean especificados.
                @endif
            </td>
        </tr>
        <tr>
            <td colspan=12 style="font-size: 8px; text-align: left;">
                <b>LA CONTRATANTE SE OBLIGA A LO SIGUIENTE: </b> <br>
                Revisar los avances del objeto de contratación;
                <br>Supervisar a través del correspondiente Administrador del servicio, la calidad en los materiales y mano de obra utilizada en el trabajo; y,
                <br>Cumplir con la forma de pago señalado en la orden de 
                @if($data->clasificacion[0]->codigo=='S')
                    trabajo
                @else
                    compra
                @endif
            </td>
        </tr>
        <tr>
            <td colspan=12 style="font-size: 8px; text-align: left;">
                <b>TERMINACIÓN DE LA ORDEN DE 
                @if($data->clasificacion[0]->codigo=='S')
                    TRABAJO:
                @else
                    COMPRA:
                @endif
                </b> <br>
                La orden de trabajo terminará, normalmente, por el cumplimiento de las obligaciones de las partes dentro del plazo convenido.
                <br>Podrá declararse la terminación anticipada de la orden de trabajo por las siguientes causas:
                <br>1. Por mutuo acuerdo, lo cual constará en un documento suscrito por las partes
                <br>2. Por causas imputables al PROVEEDOR, declaración que hará la FEDERACIÓN DEPORTIVA DEL GUAYAS de pleno derecho y sin lugar a reclamos.
                <br>3. Por causas imputables a la FEDERACIÓN DEPORTIVA DEL GUAYAS, debidamente demostradas por el PROVEEDOR, declarada por Juez competente.
                <br>Producida la terminación anticipada de la orden de trabajo, las partes levantarán un acta de estado de cuenta final, que contabilizará todas las partidas pendientes entre la Contratante y el PROVEEDOR, incluyendo retenciones, indemnizaciones por daños y perjuicios, multas, impuestos y gastos de la terminación, para establecer el saldo final de las partes.
            </td>
        </tr>
        <tr>
            <td colspan=12 style="font-size: 8px; text-align: center;">
                <b>CUADRO DE VERIFICACIÓN DE DOCUMENTOS</b>
            </td>
        </tr>
        <tr>
            <td colspan=6 style="font-size: 8px; text-align: left;">
                <b>APROBACIÓN DE INFIMA CUANTÍA</b>
            </td>
            <td colspan=6 style="font-size: 8px; text-align: left;">
                {{date('Y-m-d',strtotime($data->resolucion_compra->fecha_aprobado))}} - {{$data->user_autoriza->name}} {{$data->user_autoriza->last_name}}
            </td>
        </tr>
        <tr>
            <td colspan=6 style="font-size: 8px; text-align: left;">
                <b>CUADRO COMPARATIVO</b>
            </td>
            <td colspan=6 style="font-size: 8px; text-align: left;">
                CC-{{$data->codigo}}
            </td>
        </tr>
        <tr>
            <td colspan=6 style="font-size: 8px; text-align: left;">
                <b>COTIZACIONES</b>
            </td>
            <td colspan=6 style="font-size: 8px; text-align: left;">
                @foreach($data->cuadro_comparativo->cotizaciones as $cot)
                    -{{$cot->proveedor->nombre_comercial}}
                @endforeach
            </td>
        </tr>
        @if($data->tipo_procesos[0]->codigo=='IC')
            <tr>
                <td colspan=6 style="font-size: 8px; text-align: left;">
                    <b>VERIFICACIÓN DE CATÁLOGO ELECTRÓNICO</b>
                </td>
                <td colspan=6 style="font-size: 8px; text-align: left;">
                    {{date('Y-m-d',strtotime($data->fecha_verif_cpc))}}
                </td>
            </tr>   
        @endif
        
        <tr>
            <td colspan=6 style="font-size: 8px; text-align: left;">
                <b>REQUERIMIENTO</b>
            </td>
            <td colspan=6 style="font-size: 8px; text-align: left;">
                {{date('Y-m-d',strtotime($data->fecha_inicio))}} - 
                @foreach($data->tipo_procesos_usuarios_get as $user)
                    @if($user->nombre=='Requieriente')
                        {{$user->name}} {{$user->last_name}}
                    @endif
                @endforeach
            </td>
        </tr>
        <tr>
            <td colspan=12 style="font-size: 8px; text-align: left;">
                <b>OBSERVACIONES:</b>
                <br>
                {{$data->clasificacion[0]->pivot->observaciones}}
                
            </td>
        </tr>
        <tr>
            <td colspan=12 style="font-size: 8px; text-align: center;">
                <b>En virtud a Quipux: {{$data->codigo_documento}} el administrador de cada proceso de compras estara bajo la responsabilidadn única del director del area requiriente.</b>
                
            </td>
        </tr>
    </table> 
    <br>
    <table style="width: 491px;margin-left: 134px;font: 8px;text-align: center" border="1" cellspacing="0" cellpadding="0">
        <tr>
            <td style="width: 224px;">
                <b>ELABORADO POR:</b>
                <br>
                <p>FIRMA:
                    <br>
                    @php
                    $elab="";
                        foreach($data->tipo_procesos_usuarios_get as $us){
                            if($us->codigo == "elb"){
                                $elab="".$us->name." ".$us->last_name;
                            }
                        }
                    @endphp     
                    {{$elab}}
                </p>
                <br>
                <br>
                <br>
            </td>
            <td style="width: 267px;">
                <b>ADMINISTRADOR DE LA ORDEN DE TRABAJO:</b>
                <br>
                <p>FIRMA:
                    <br>
                    @foreach($data->tipo_procesos_usuarios_get as $user)
                        @if($user->codigo=='adm')
                            {{$user->name}} {{$user->last_name}}
                        @endif
                    @endforeach
                </p>
                <br>
                <br>
                <br>
                <br>
            </td>
        </tr>
        <tr>
            <td style="width: 224px;">
                <b>AUTORIZACIÓN EMISIÓN ORDEN DE TRABAJO</b>
                <br>
                <p>FIRMA:
                    <br>
                    {{$data->user_autoriza->name}} {{$data->user_autoriza->last_name}}
                </p>
                <br>
                <br>
                <br>
                <br>
            </td>
            <td style="width: 267px;">
                <b>PROVEEDOR:</b>
                <br>
                <p>
                    {{$data->resolucion_compra->cotizacion->proveedor->ruc}}
                    <br>
                    {{$data->resolucion_compra->cotizacion->proveedor->razon_social}}

                </p>
                <br>
                <br>
                <br>
            </td>
        </tr>
    </table>
  </body>