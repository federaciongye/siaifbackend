<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>VERIFICACIÓN DE CPC VCPC-{{$data->codigo}}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <style>
        body {
        font-family: 'Helvetica';
        font-size:10px;
        margin: 27px 15px 20px 15px; 
        }
        @page { 
            margin: 27px 15px 20px 15px; 
        }

        header {
            font-family: 'Helvetica';
            font-size:10px;
            margin: 5px;
        }

        footer {
            font-family: 'Helvetica';
            font-size:8px;
            margin: 20px;
            position: fixed;
            bottom: -45px; 
            left: 0px; 
            right: 0px;
            height: 50px; 
            text-align: center;
        }
    </style>
  </head>
  <body>
    <footer>
        <font style="font-family:arial; font-size:10px; color:#0f70b7;">
            <b>Oficina: Av. las Américas y Av. Kennedy. Telfs: 3815500. fedeguayas.com.ec. email: fdg@fedeguayas.com.ec<br>FEDEGUAYAS. Guayaquil - Ecuador
            </b>
        </font>
    </footer>
    <main>
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <tr>
                <td colspan="8" style="text-align:center"><img width="200px" src="{{ public_path('storage/images/fdg.png') }}"></td>
            </tr>
            <tr>
                <td colspan="8" style="text-align:center"><font style="font-family:arial; font-size:20px; color:#000000;"><b>FEDERACIÓN DEPORTIVA DEL GUAYAS</b></font></td>
            </tr>
            <tr>
                <td colspan="8" style="text-align:center "><font style="font-family:arial; font-size:20px; color:#000000; text-align: center">
                    CERTIFICACIÓN DE CATALOGO ELECTRÓNICO
                </td>

            </tr>
            <tr>
                <td colspan="8"><br></td>
            </tr>
            <tr>
                <td colspan="8" style="text-align: center"> 
                    <b>No. VCPC-{{$data->codigo}}</b> 
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="8" style="text-align: left">
                @php
                    setlocale(LC_ALL, 'es_ES');
                    $fecha = Carbon\Carbon::parse($data->fecha_verif_cpc);
                    $fecha->format("F"); // Inglés.
                    $mes = $fecha->formatLocalized('%B');
                    $dia=$fecha->day;
                    $ano=$fecha->year;
                @endphp
                <font style="font-family:arial; font-size:12px; color:#000000;">
                Guayaquil, {{$dia}} de 
                @if($mes=='January')
                Enero
                @endif
                @if($mes=='February')
                Febrero
                @endif
                @if($mes=='March')
                Marzo
                @endif
                @if($mes=='April')
                Abril
                @endif
                @if($mes=='May')
                Mayo
                @endif
                @if($mes=='June')
                Junio
                @endif
                @if($mes=='July')
                Julio
                @endif
                @if($mes=='August')
                Agosto
                @endif
                @if($mes=='September')
                Septiembre
                @endif
                @if($mes=='October')
                Octubre
                @endif
                @if($mes=='November')
                Noviembre
                @endif
                @if($mes=='December')
                Diciembre
                @endif
                 de {{$ano}}.</font><br></td>
            </tr>
            <tr>
                <td colspan="8">
                    <br>
                    <br>
                    <br>
                </td>
	        </tr>
            <tr>        
                <td colspan="8" style="text-align: justify">
                    <br>
                    <font style="font-family:arial; font-size:15px; color:#000000;"><br />
                    En referencia al Requerimiento con fechas de {{$dia}} de 
                    @if($mes=='January')
                    Enero
                    @endif
                    @if($mes=='February')
                    Febrero
                    @endif
                    @if($mes=='March')
                    Marzo
                    @endif
                    @if($mes=='April')
                    Abril
                    @endif
                    @if($mes=='May')
                    Mayo
                    @endif
                    @if($mes=='June')
                    Junio
                    @endif
                    @if($mes=='July')
                    Julio
                    @endif
                    @if($mes=='August')
                    Agosto
                    @endif
                    @if($mes=='September')
                    Septiembre
                    @endif
                    @if($mes=='October')
                    Octubre
                    @endif
                    @if($mes=='November')
                    Noviembre
                    @endif
                    @if($mes=='December')
                    Diciembre
                    @endif
                 de {{$ano}}, la unidad de Adquisiciones de la Federación deportiva del Guayas, 
                    certifica que el <b>{{mb_strtoupper($data->objeto, 'UTF-8')}}</b> , NO SE ENCUENTRA 
                        disponible en el Catálogo Electrónico del portal <a href="http://www.compraspublicas.gob.ec">www.compraspublicas.gob.ec</a>.
                    </font>
                </td>
            </tr>
            <tr>
                <td colspan="8">			
			        <br />
                    <br />
                    <br />
                    <br />
                    Atentamente
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="8">___________________________</td>
            </tr>
            <tr>
                <td colspan="8"><font style="font-family:arial; font-size:15px; color:#000000;">
                @foreach($data->tipo_procesos_usuarios_get as $us)
                    @if($us->codigo=="elb") 
                    {{ $us->name}} {{$us->last_name}}
                    @endif
                @endforeach
                </font></td>
            </tr>
            <tr>
                <td>
                    <br />
                    <br />
                    <br />
                    <br />
                    <p>
                        Se adjunta impresión de consulta de página: https://catalogo.compraspublicas.gob.ec
                    </p>
                </td>
                
            </tr>
        </table>
        
        
    </main>
    
    
  </body>
</html>