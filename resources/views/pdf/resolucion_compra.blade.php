<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>RESOLUCIÓN DE COMPRA RC-{{$data->codigo}}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <style>
        body {
        font-family: 'Helvetica';
        font-size:10px;
        margin: 27px 15px 20px 15px; 
        }
        @page { 
            margin: 27px 15px 20px 15px; 
        }

        header {
            font-family: 'Helvetica';
            font-size:10px;
            margin: 5px;
        }

        footer {
            font-family: 'Helvetica';
            font-size:8px;
            margin: 20px;
            position: fixed;
            bottom: -45px; 
            left: 0px; 
            right: 0px;
            height: 50px; 
            text-align: center;
        }
    </style>
  </head>
  <body>
    <footer>
        <font style="font-family:arial; font-size:10px; color:#0f70b7;">
            <b>Oficina: Av. las Américas y Av. Kennedy. Telfs: 3815500. fedeguayas.com.ec. email: fdg@fedeguayas.com.ec<br>FEDEGUAYAS. Guayaquil - Ecuador
            </b>
        </font>
    </footer>
    <main>
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <tr>
                <td colspan="8" style="text-align:center"><img width="300px" src="{{ public_path('/storage/images/fdg2.jpg') }}"></td>
            </tr>
                <tr>
                <td colspan="8" style="text-align:center"><font style="font-family:arial; font-size:20px; color:#000000;"><b>FEDERACIÓN DEPORTIVA DEL GUAYAS</b></font></td>
            </tr>
            <tr>
                @if($data->tipo_procesos[0]->codigo=='IC')
                    <td colspan="8" style="text-align:center;"><font style="font-family:arial; font-size:20px; color:#000000; text-align: center">
                    MEMORANDO DE APROBACIÓN DE ÍNFIMA CUANTÍA</font>
                    </td>
                @else
                    <td colspan="8" style="text-align:center;"><font style="font-family:arial; font-size:20px; color:#000000; text-align: center">
                    MEMORANDO DE APROBACIÓN DE AUTOGESTIÓN</font>
                    </td>
                @endif
            </tr>
            <tr>
                <td colspan="8"><br></td>
            </tr>
            <tr>
                <td colspan="8" style="text-align: center"> 
                    <b>No. RC-{{$data->codigo}}</b> 
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="8" style="text-align: left">
                @php
                    setlocale(LC_ALL, 'es_ES');
                    $fecha = Carbon\Carbon::parse($data->resolucion_compra->fecha_aprobado);
                    $fecha->format("F"); // Inglés.
                    $mes = $fecha->formatLocalized('%B');
                    $dia=$fecha->day;
                    $ano=$fecha->year;
                @endphp
                <font style="font-family:arial; font-size:12px; color:#000000;">
                Guayaquil, {{$dia}} de 
                @if($mes=='January')
                Enero
                @endif
                @if($mes=='February')
                Febrero
                @endif
                @if($mes=='March')
                Marzo
                @endif
                @if($mes=='April')
                Abril
                @endif
                @if($mes=='May')
                Mayo
                @endif
                @if($mes=='June')
                Junio
                @endif
                @if($mes=='July')
                Julio
                @endif
                @if($mes=='August')
                Agosto
                @endif
                @if($mes=='September')
                Septiembre
                @endif
                @if($mes=='October')
                Octubre
                @endif
                @if($mes=='November')
                Noviembre
                @endif
                @if($mes=='December')
                Diciembre
                @endif
                 de {{$ano}}.</font><br></td>
            </tr>
            <tr>
                <td colspan="8">
                    <br>
                    <br>
                    <br>
                </td>
	        </tr>
            <tr>        
                <td colspan="8" style="text-align: justify">
                    <br>
                    <font style="font-family:arial; font-size:15px; color:#000000;"><br />
                    Mediante la presente se aprueba la CONTRATACIÓN POR 
                    @if($data->tipo_procesos[0]->codigo=='IC')
                    INFIMA CUANTIA 
                    @else
                    AUTOGESTIÓN
                    @endif
                    
                    @php
                        $subtotal = 0;
                        $iva=0;
                        $total=0;
                    @endphp
                    @foreach($data->resolucion_compra->cotizacion->items as $it)
                        @php
                            $cant = $it->cantidad;
                            $valor = $it->pivot->p_unitario;
                            $subtotal = $subtotal+($cant*$valor);
                        @endphp
                    @endforeach
                    @php
                        $iva=0;
                        
                    @endphp
                    @php
                        $total=$subtotal+$iva;
                    @endphp
                    de:  <b>{{mb_strtoupper($data->objeto, 'UTF-8')}}</b>, por el subtotal de ${{number_format((float)$total, 2, '.', '')}} dólares de Estados Unidos de América.
                    </font>
                </td>
            </tr>
            <tr>
                <td colspan="8">			
			        <br />
                    <br />
                    <br />
                    <br />
                    Sin otro particular,
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
                <tr>
                <td colspan="8"><font style="font-family:arial; font-size:15px; color:#000000;">Ing. Washington Alfredo Guerrero Lara</font></td>
            </tr>
                <tr>
                <td colspan="8"><font style="font-family:arial; font-size:15px; color:#000000;"><b>Administrador</b></font></td>
            </tr>
                <tr>
                <td colspan="8"><font style="font-family:arial; font-size:15px; color:#000000;"><b>Federación Deportiva del Guayas</b></font></td>
            </tr>
            <tr>
                <td colspan="8">
                    <br>
                    <br>
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="8"><font style="font-family:arial; font-size:15px; color:#000000;"><b>Elaborado por:</b>
                @foreach($data->tipo_procesos_usuarios_get as $us)
                    @if($us->codigo=="elb") 
                    {{ $us->name}} {{$us->last_name}}
                    @endif
                @endforeach
                </font></td>
            </tr>
        </table>
        
    </main>
    
    
  </body>
</html>