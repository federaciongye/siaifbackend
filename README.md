
## Acerca de Laravel

Laravel es un framework de aplicación web con una sintaxis expresiva y elegante. 

## Requerimientos
- PHP >=7.3
- [Composer](https://getcomposer.org)
- [etc](https://laravel.com/docs/8.x#server-requirements)

## Documentación de Laravel

Laravel cuenta con una [documentaciónn](https://laravel.com/docs) extensa y bien explicada y cuenta con una libreria de de video tutoriales con todo lo necesario para aprender de la plataforma.

En [Laracasts](https://laracasts.com) hay videos tutoriales con diferentes temas de aplicación del framework, entre otros.

## [Estructura](https://laravel.com/docs/8.x/structure)
El proyecto cuenta con el directorio básico de Laravel de los cuales se explicará los mas importantes o los mas usados.
Adicionalmente cada archivo que se use tendrá su debida documentación.

**app**: Contiene la codficación principal del proyecto. La mayoria de clases estarán contenidos aqui. 

**config**: Contiene los archivos de configuracón del proyecto.

**database**: Contiene las migraciones de las bases de datos, factories de modelos y seeders.

**resources**: Contiene las vistas y otros recursos necesarios como archivos de js, css, less, sass, etc.

**routes**: Contiene todas las rutas del proyecto. En este caso solo se usa el archivo `api.php` que es el que nos permite difinir las rutas para la API.

**storage**: Contiene las plantillas en formato blade compiladas, entre otros archivos generados por los usuarios.

## Modelos
El proyecto tiene modelos que definen la estructura de objetos y sus relaciones como en la siguiente imagen del diseño de la base de datos.

![Base de datos](BD.png)

Los modelos pueden ser encontrados en el directorio **app/** y no tienen carpeta específica. **app/User.php**, **app/Sistema.php**, etc.

**Tomar en cuenta que la relacion de los roles en la imagen son como referencia pero la tabla esta relacionada de otra manera por el paquete spatie/laravel-permission**

## Controladores
Los controladores son los que "Controlan" el comportamiento de la aplicación o en este caso el API del proyecto. Los controladores pueden agrupar varias operaciones lógicas en una sola clase.

Los controladores pueden ser encontrados en el directorio **app/Http/Controllers**. **app/Http/Controllers/AuthController.php**, **app/Http/Controllers/UserController.php**, etc.

## Mailables
En Laravel, cada tipo de correo enviado por la aplicación es representado por un "mailable". Estas clases son almacenadas en el directorio **app/Mail**. 
Para crear un mailable se debe ejecutar en la terminal el siguiente comando: `php artisan make:mail EnviarCorreo`.
Es recomandable crear un nuevo "mailable" para cada tipo de correo que desee enviar.

## Base de datos
La lógica de base de datos se almacena en el derectorio `database` la cual contiene otros directorios:

- [Migrations](https://laravel.com/docs/8.x/migrations): En este directorio se almacenan las migraciones. Estos manejan el versionamiento de la base de datos, es decir, que permiten modificar y compartir estructuras en la base de datos para que puedan ser usados por el equipo de trabajo. 

- [Seeds](https://laravel.com/docs/8.x/seeding): En este directorio se almacenan las semillas. Estos permiten ingresar contenido de prueba en la base de datos.

## Rutas

- `api.php`: En este archivo se definen las rutas del Api y cada una de las rutas esta anexado a los recursos que tienen los controladores asignados a estos.

## Plugins

- [tymon/jwt-auth](https://jwt-auth.readthedocs.io/en/develop/): Es un estandar que se transmite a través de JSON. Contiene información necesaria básica de usuario y de autenticacion con tokens de sesión.

- [spatie/laravel-permission](https://spatie.be/docs/laravel-permission/v3/introduction): Permite manejar roles y permisos a usuarios en la base de datos.

- [nesbot/carbon](https://carbon.nesbot.com/docs/): Es una clase heredada de PHP DateTime. Este plugin permite manejar de mejor manera las fechas y sus variantes.


## Ambiente de desarrollo
- [XAMPP](https://www.apachefriends.org/es/index.html): Simulador de servidor con MariaDB (MySQL), php, apache y otras herramientas importantes.

- [GIT](https://git-scm.com): Sistema de control de versiones de software.

## Proyecto
Para producción o ambiente de desarrollo se debe realizar:

1. Clonar proyecto desde el [repositorio remoto](https://bitbucket.org).

2. Usar el comando `cd` para moverse a la carpeta recién clonada en la terminal.

3. Ejecutar `composer install`.

4. Copiar el contenido del archivo `.env.example` en un nuevo archivo `.env`. En cmd de windows ejecutar: `copy .env.example .env`. En linux o mac ejecutar: `cp .env.example .env`.

5. En el archivo `.env` ingresar la informacion de base de datos (nombre de la base de datos, usuario y contraseña).

6. Ejecutar: `php artisan key:generate`.

7. Ejecutar: `php artisan jwt:secret`.

8. Ejecutar: `php artisan migrate`.

9. Ejecutar seeder en caso que haya. `php artisan make:seeder UserSeeder`

Versiones:

1.0:

- Base de datos:
![Base de datos](BD_BASICA.png)

2.0:

- Base de datos
![Base de datos](BD.png)
- Modelos básicos
- Estructura básica desk.
- Controladores y rutas básicas.
- Envío de correos para confirmación de registro.
- Departamentos, sistemas y derechos sobre sistemas (Ver, crear, editar, eliminar).
- Validación de ingreso.
