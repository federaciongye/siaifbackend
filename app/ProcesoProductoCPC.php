<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcesoProductoCPC extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'proceso_productos_cpc';

    //Atributos de ProcesoProductoCPC
    protected $fillable = [
        'proceso_id',
    'producto_cpc_id'];


    //Relacion a proceso
    public function proceso()
	{
        //Relación de ProcesoProductoCPC pertenece a proceso
		return $this->belongsTo('App\Proceso');
    }

    //Relacion a proceso
    public function ProductoCPC()
	{
        //Relación de ProcesoProductoCPC pertenece a ProductoCPC
		return $this->belongsTo('App\ProductoCPC');
    }
}
