<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prioridad extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'prioridades';

    //Atributos de Prioridad
    protected $fillable = [
        'nombre',
    'descripcion', 
    'horas', 
    'dias'];
}
