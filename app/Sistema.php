<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Crypt;


class Sistema extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'sistemas';

    //Atributos de Sistema
    protected $fillable = [
        'nombre',
    'descripcion',
    'sistema_sup_id'];

    //Desencriptacion de contenido en BD
    public function getnombreAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    //Encriptacion de contenido en BD
    public function setnombreAttribute($value) {
        $this->attributes['nombre'] = Crypt::encryptString($value);
    }

    //Desencriptacion de contenido en BD
    public function getDescripcionAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    //Encriptacion de contenido en BD
    public function setDescripcionAttribute($value) {
        $this->attributes['descripcion'] = Crypt::encryptString($value);
    }

    //Relacion con usuarios
    public function users()
    {
        //Relación de Sistema pertenece a muchos usuarios
        return $this->belongsToMany('App\User','App\UserSistema')->withPivot('sistema_id','user_id');
    }

    //Relacion con derechos
    public function derechos()
    {
        //Relación de Sistema pertenece a un derecho
        return $this->belongsTo('App\Derecho','App\UserSistemaDerecho');
    }

    //Relacion con sistema superior
    public function parent()
    {
        //Relación de Sistema pertenece a un sistema superior
        return $this->belongsTo('App\Sistema','sistema_sup_id');
    }

    public function children()
    {
    	return $this->hasMany('App\Sistema','sistema_sup_id');
    }
    
}
