<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cotizacion extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'cotizaciones';

    //Atributos de Cotizacion
    protected $fillable = [
        'cuadro_comparativo_id',
    'proveedor_id',
    'califica',
    'garantia',
    'f_pago',
    'f_entrega',
    'observaciones',
    'iva'
   ];

   protected $casts = [
    'items.pivot.iva_ap' => 'boolean'
   ];

   //Relacion a CuadroComparativo
   public function cuadro_comparativo()
   {
       //Relación de Cotizacion pertenece a CuadroComparativo
       return $this->belongsTo('App\CuadroComparativo');
   }

   //Relacion a Proveedor
   public function proveedor()
   {
       //Relación de Cotizacion pertenece a IteProveedorms
       return $this->belongsTo('App\Proveedor');
   }

   public function resolucion_compra()
    {
        return $this->hasOne('App\ResolucionCompra');
    }

   public function items(){
    return $this->belongsToMany(
        'App\Items',
        'items_cotizaciones',
        'cotizacion_id',
        'item_id')->withPivot('p_unitario',
        'desc_porcentaje',
        'desc_valor',
        'subtotal',
        'iva',
        'total',
        'iva_ap');
   }
}
