<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'proveedores';

    //Atributos de Cotizacion
    protected $fillable = [
        'nombre_comercial',
        'razon_social',
        'nombres',
        'apellidos',
        'ruc',
        'telefono',
        'celular',
        'email_personal',
        'email_empresa',
        'observaciones'
   ];


   //Relacion con cotizacion
   public function cotizacion()
   {
       //Relación de proveedor tienes muchas cotizaciones
       return $this->hasMany('App\Cotizacion');
   }
}
