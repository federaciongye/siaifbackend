<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Edificio extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'edificios';

    //Atributos de Edificio
    protected $fillable = [
        'nombre',
    'descripcion', 
    'direccion', 
    'tipo_edificio_id'];

    //Relacion con TipoEdificio
    public function tipo_edificio()
	{
        //Relación de Edificio pertenece a un TipoEdificio
		return $this->belongsTo('App\TipoEdificio');
    }

    //Relacion con Departamentos
    public function departamentos()
    {
        //Relación de Edificio tiene muchos Departamentos
        return $this->hasMany('App\Departamento');
    }
}
