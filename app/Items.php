<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'items';

    //Atributos de Items
    protected $fillable = [
        'descripcion',
    'cantidad',
    'unidad',
    'dimension',
    'iva',
    'tipocpc'
    ];

    public function cuadro_comp()
    {
        return $this->belongsTo('App\CuadroComparativo', 'cuadro_comp_id');
    }

    public function cpc()
    {
        return $this->belongsTo('App\ProductoCPC', 'cpc_id');
    }

    public function cotizaciones(){
        return $this->belongsToMany(
            'App\Cotizacion',
            'items_cotizaciones',
            'item_id',
            'cotizacion_id')->withPivot('p_unitario',
            'desc_porcentaje',
            'desc_valor',
            'subtotal',
            'iva',
            'total');
    }


}
