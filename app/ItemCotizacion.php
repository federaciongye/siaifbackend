<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCotizacion extends Model
{
     //Referencia a la tabla en BD
     protected $table = 'items_cotizaciones';

     //Atributos de Items
     protected $fillable = [
         'cotizacion_id',
     'item_id',
     'p_unitario',
     'desc_porcentaje',
     'desc_valor',
     'subtotal',
     'iva',
     'total',
     'iva_ap'
    ];

    protected $casts = [
        'iva_ap' => 'boolean',
    ];

     //Relacion a Cotizacion
     public function cotizacion()
     {
         //Relación de ItemCotizacion pertenece a Cotizacion
         return $this->belongsTo('App\Cotizacion');
     }
 
     //Relacion a item
     public function item()
     {
         //Relación de ItemCotizacion pertenece a Items
         return $this->belongsTo('App\Items');
     }

}
