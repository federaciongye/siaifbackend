<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResolucionCompra extends Model
{
    //
    //Referencia a la tabla en BD
    protected $table = 'resolucion_compra';

    //Atributos de ProductoCPC
    protected $fillable = [
        'fecha_aprobado',
        'proceso_id',
        'cot_id'];

    public function proceso()
    {
        return $this->belongsTo('App\Proceso', 'proceso_id');
    }

    public function cotizacion()
    {
        return $this->belongsTo('App\Cotizacion', 'cot_id');
    }
}
