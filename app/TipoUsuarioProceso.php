<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class TipoUsuarioProceso extends Model
{
    //
     //Referencia a la tabla en BD
     protected $table = 'tipo_usuario_proceso';

     //Atributos de Departamento
     protected $fillable = [
         'nombre',
     'descripcion',
     'codigo'];

    // //Relacion de usuarios a traves de procesos_usuarios
    // public function users()
    // {
    //     return $this->hasManyThrough('App\User', 'App\ProcesosUsuarios');
    // }

    // //Relacion de procesos a traves de procesos_usuarios
    // public function procesos()
    // {
    //     return $this->hasManyThrough('App\Proceso', 'App\ProcesosUsuarios');
    // }

    public function users()
    {
        return $this->belongsToMany('App\User',
        'App\ProcesosUsuarios',
        'user_id',
        'tipo_usuario_proceso_id')->withPivot('proceso_id');
    }

    //Desencriptacion de contenido en BD
    public function getcodigoAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    //Encriptacion de contenido en BD
    public function setcodigoAttribute($value) {
        $this->attributes['codigo'] = Crypt::encryptString($value);
    }

    //Desencriptacion de contenido en BD
    public function getnombreAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    //Encriptacion de contenido en BD
    public function setnombreAttribute($value) {
        $this->attributes['nombre'] = Crypt::encryptString($value);
    }

    //Desencriptacion de contenido en BD
    public function getdescripcionAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    //Encriptacion de contenido en BD
    public function setdescripcionAttribute($value) {
        $this->attributes['descripcion'] = Crypt::encryptString($value);
    }

}
