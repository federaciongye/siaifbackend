<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoProceso extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'tipo_proceso';

    //Atributos de TipoProceso
    protected $fillable = [
        'nombre',
    'descripcion',
    'codigo'];

    //Relacion de procesos a traves de TipoProcesoProcesos
    public function procesos()
    {
        return $this->hasManyThrough('App\Proceso', 'App\TipoProcesoProcesos');
    }
}
