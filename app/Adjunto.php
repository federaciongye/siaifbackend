<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adjunto extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'adjuntos';

    //Atributos de Adjunto
    protected $fillable = [
        'nombre',
    'descripcion', 
    'linkArchivo', 
    'ticket_id', 
    'comentario_id'];

    //Relacion con ticket
    public function ticket()
	{
        //Relación de Adjunto pertenece a un Ticket
		return $this->belongsTo('App\Ticket');
    }

    //Relacion con comentario
    public function comentario()
	{
        //Relación de Adjunto pertenece a un Comentario
		return $this->belongsTo('App\Comentario');
    }
}
