<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clasificacion extends Model
{
    //
    //Referencia a la tabla en BD
    protected $table = 'clasificacion';

    //Atributos de Clasificacion
    protected $fillable = [
        'nombre',
    'descripcion',
    'codigo'];

    //Relacion con ClasificacionProceso
    public function clasificacion_proceso()
    {
        //Relación de Clasificacion tienes muchos ClasificacionProceso
        return $this->hasMany('App\ClasificacionProceso');
    }

    public function procesos()
    {
        return $this->belongsToMany(
            'App\Proceso',
        'App\ClasificacionProceso',
        'clasificacion_id',
        'proceso_id');
    }

    
}
