<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketPrioridad extends Model
{
    
    //Referencia a la tabla en BD
    protected $table = 'tickets_prioridad';

    //Atributos de TicketPrioridad
    protected $fillable = [
        'ticket_id',
    'prioridad_id', 
    'admin_id'];

    //Relacion con ticket
    public function ticket()
	{
        //Relación de TicketPrioridad pertenece a un Ticket
		return $this->belongsTo('App\Ticket');
    }

    //Relacion con usuario admin
    public function admin()
	{
        //Relación de TicketPrioridad pertenece a un Usuario
		return $this->belongsTo('App\User');
    }

    //Relacion con prioridad
    public function prioridad()
	{
        //Relación de TicketPrioridad pertenece a una prioridad
		return $this->belongsTo('App\Prioridad');
    }
}
