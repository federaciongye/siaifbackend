<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Derecho extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'derechos';

    //Atributos de Derecho
    protected $fillable = [
        'nombre',
    'descripcion'];

    //Relacion con UserSistema
    public function usersistemaderechos()
	{
         //Relación de Derecho que pertenence a UserSistema
		return $this->belongsTo('App\UserSistema');
    }

     //Desencriptacion de contenido en BD
    public function getNombreAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    //Encriptacion de contenido en BD
    public function setNombreAttribute($value) {
        $this->attributes['nombre'] = Crypt::encryptString($value);
    }

     //Desencriptacion de contenido en BD
    public function getDescripcionAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    //Encriptacion de contenido en BD
    public function setDescripcionAttribute($value) {
        $this->attributes['descripcion'] = Crypt::encryptString($value);
    }
}
