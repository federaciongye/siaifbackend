<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoMontoContratacion extends Model
{
     //Referencia a la tabla en BD
     protected $table = 'productos_montos_contratacion';

     //Atributos de ProductoMontoContratacion
     protected $fillable = [
         'monto_contratacion_id',
     'producto_id',
     'acumulado'];

     //Relacion a monto contratacion
    public function monto_contratacion()
	{
        //Relación de ProductoMontoContratacion pertenece a MontoContratacion
		return $this->belongsTo('App\MontoContratacion');
    }

    //Relacion a ProductoCPC
    public function ProductoCPC()
	{
        //Relación de ProductoMontoContratacion pertenece a ProductoCPC
		return $this->belongsTo('App\ProductoCPC');
    }
}
