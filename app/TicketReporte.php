<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketReporte extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'tickets_reporte';

    //Atributos de TicketReporte
    protected $fillable = [
        'ticket_id',
    'reporte_id'];

    //Relacion con ticket
    public function ticket()
	{
        //Relación de TicketReporte pertenece a un ticket
		return $this->belongsTo('App\Ticket');
    }

    //Relacion con reporte
    public function reporte()
	{
        //Relación de TicketReporte pertenece a un reporte
		return $this->belongsTo('App\Reporte');
    }
}
