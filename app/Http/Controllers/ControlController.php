<?php

namespace App\Http\Controllers;

use App\Control;
use Illuminate\Http\Request;

class ControlController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api',['except' => ['index','fillControl']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $controles = Control::all();
            return $controles;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $controles = Control::create($request->all());
            $controles = Control::all();
            return $controles;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Control  $control
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $control = Control::findOrFail($id);
            return $control;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Control  $control
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $control = Control::findOrFail($id);
            $control->update($request->all());
            $controles = Control::all();
            return $controles;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Control  $control
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Control::destroy($id);
            $controles = Control::all();
            return $controles;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function fillControl(Request $request){
        $controles = Control::create($request->all());

    }
}
