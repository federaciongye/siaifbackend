<?php

namespace App\Http\Controllers;

use App\MontoContratacion;
use Illuminate\Http\Request;

class MontoContratacionController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $montosContrataciones = MontoContratacion::all();
            return $montosContrataciones;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $montosContrataciones = MontoContratacion::create($request->all());
            $montosContrataciones = MontoContratacion::all();
            return $montosContrataciones;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MontoContratacion  $MontoContratacion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $MontoContratacion = MontoContratacion::findOrFail($id);
            return $MontoContratacion;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MontoContratacion  $MontoContratacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $MontoContratacion = MontoContratacion::findOrFail($id);
            $MontoContratacion->update($request->all());
            $montosContrataciones = MontoContratacion::all();
            return $montosContrataciones;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MontoContratacion  $MontoContratacion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            MontoContratacion::destroy($id);
            $montosContrataciones = MontoContratacion::all();
            return $montosContrataciones;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    
}
