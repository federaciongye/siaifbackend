<?php

namespace App\Http\Controllers;

use App\ProductoCPC;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Departamento;
use App\User;
use App\Sistema;
use App\Derecho;
use App\UserSistema;
use App\TipoUsuarioProceso;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use DB;
use Carbon\Carbon;
use Rap2hpoutre\FastExcel\FastExcel;

class ProductoCPCController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api', ['except' => ['addTipoUserProceso']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $productosCPCs = ProductoCPC::all();
            return $productosCPCs;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $productosCPCs = ProductoCPC::create($request->all());
            $productosCPCs = ProductoCPC::all();
            return $productosCPCs;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductoCPC  $productoCPC
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $productoCPC = ProductoCPC::findOrFail($id);
            return $productoCPC;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductoCPC  $productoCPC
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $productoCPC = ProductoCPC::findOrFail($id);
            $productoCPC->update($request->all());
            //$productosCPCs = ProductoCPC::all();
            return $productoCPC;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductoCPC  $productoCPC
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            ProductoCPC::destroy($id);
            $productosCPCs = ProductoCPC::all();
            return $productosCPCs;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function parseCPCtest(){

        $csv=$this->csv_to_array("../storage/app/public/csv/personal.csv",",");
        foreach ($csv as $user){
            $us=null;
            $us_sup=null;

            if($user['cedula']!=""){
                if(strlen($user['cedula'])==9){
                    $user['cedula']="0".$user['cedula'];
                }
                $splitName=explode(' ',$user['nombres']);
                $splitLastName=explode(' ',$user['apellidos']);
                $us=User::create(['name' => ucwords(mb_strtolower($user['nombres'],'UTF-8')),
                'last_name'=>ucwords(mb_strtolower($user['apellidos'],'UTF-8')), 
                'cargo'=>ucwords(mb_strtolower($user['cargo'],'UTF-8')),
                'email'=> mb_strtolower($user['correo'],'UTF-8'),
                'password' => Hash::make(mb_strtolower(substr($splitName[0], 0,3),'UTF-8')."".mb_strtolower(substr($splitLastName[0], 0,3),'UTF-8')."".substr($user['cedula'], -3).""),
                'cedula'=>$user['cedula']]);

                $jefesplt=explode(' ',$user['jefe']);
                $search=ucwords(mb_strtolower($jefesplt[0]),'UTF-8');
                $lastNames=ucwords(mb_strtolower($jefesplt[0],'UTF-8'))." ".ucwords(mb_strtolower($jefesplt[1],'UTF-8'));
                $names=ucwords(mb_strtolower($jefesplt[2],'UTF-8'));
                $us_sup = User::all()->filter(function($record) use($lastNames, $names) {
                    if(strpos($record->last_name,$lastNames) !== false && strpos($record->name,$names) !== false) {
                        return $record;
                    }
                })->first();
                $us->jefe()->associate($us_sup);
                $us->save();
            }
            else{
                $splitName=explode(' ',$user['nombres']);
                $splitLastName=explode(' ',$user['apellidos']);
                $us=User::create(['name' => ucwords(mb_strtolower($user['nombres'],'UTF-8')),
                'last_name'=>ucwords(mb_strtolower($user['apellidos'],'UTF-8')), 
                'cargo'=>ucwords(mb_strtolower($user['cargo'],'UTF-8')),
                'email'=> mb_strtolower($user['correo'],'UTF-8'),
                'password' => Hash::make(mb_strtolower(substr($splitName[0], 0,3),'UTF-8')."".mb_strtolower(substr($splitLastName[0], 0,3),'UTF-8'))]);
            }

            $split=explode(' - ',$user['departamento']);
            $dept=null;
            $dept2=null;
            if(count($split)==1){
                $clean=ucwords(mb_strtolower($split[0],'UTF-8'));
                $dept=Departamento::where(['nombre'=>$clean,'codigo'=>$user['codigo']])->first();
                if(!$dept){
                    $dept=Departamento::create(
                        [
                            'nombre'=>ucwords(mb_strtolower($split[0],'UTF-8')),
                            'descripcion'=>"...",
                            'dep_sup_id'=>null,
                            'edificio_id'=>null,
                            'codigo'=>$user['codigo']
                        ]
                    );
                    if($us_sup){
                        $dept_sup=Departamento::find($us_sup->departamento_id);
                        if($dept->dep_sup_id == null){
                            $dept->dep_sup()->associate($dept_sup);
                            $dept->save();
                        }
                    }
                }
                $us->departamento()->associate($dept);
                $us->save();
            }
            else{
                $clean=ucwords(mb_strtolower($split[0],'UTF-8'));
                $clean2=ucwords(mb_strtolower($split[1],'UTF-8'));

                $dept=Departamento::where(['nombre'=>$clean])->first();
                $dept2=Departamento::where(['nombre'=>$clean2,'codigo'=>$user['codigo']])->first();
                if(!$dept){
                    $dept=Departamento::create(
                        [
                            'nombre'=>ucwords(mb_strtolower($split[0],'UTF-8')),
                            'descripcion'=>"...",
                            'dep_sup_id'=>null,
                            'edificio_id'=>null,
                            'codigo'=>$user['codigo']
                        ]
                    );
                }
                if(!$dept2){
                    $dept2=Departamento::create(
                        [
                            'nombre'=>ucwords(mb_strtolower($split[1],'UTF-8')),
                            'descripcion'=>"...",
                            'dep_sup_id'=>null,
                            'edificio_id'=>null,
                            'codigo'=>$user['codigo']
                        ]
                    );
                }
                $dept2->dep_sup()->associate($dept);
                $dept2->save();

                $us->departamento()->associate($dept2);
                $us->save();
            }
        }
    }

    function csv_to_array($filename, $delimiter)
    {
        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = NULL;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                if(!$header)
                    $header = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row);
                else
                    $data[] = array_combine(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $header), $row);
            }
            fclose($handle);
        }
        return $data;
    }

    public function testRights(){
        // $coord = Role::create(['name' => "Coordinador",'descripcion'=>"NaN",'codigo'=>"002"]);
        
        // $admin = Role::create(['name' => "Director",'descripcion'=>"NaN",'codigo'=>"001",'rol_sup_id'=>$coord->id]);


        // $analista = Role::create(['name' => "Analista",'descripcion'=>"NaN",'codigo'=>"003",'rol_sup_id'=>$admin->id]);

        $coord =Role::where('name','Coordinador')->first();
        $dir =Role::where('name','Director')->first();
        $analista =Role::where('name','Analista')->first();

        $user = User::where('email','jfierropal@outlook.com')->first();
        $user->assignRole($analista);

        $user2 = User::where('email','angiem_182@hotmail.com')->first();
        $user2->assignRole($coord);

        $user3 = User::where('email','yolanda.beltran@fedeguayas.com.ec')->first();
        $user3->assignRole($dir);

        //Derechos
        // $derecho1 = Derecho::create([
        //     'nombre' => "Crear",
        //     'descripcion' => "Este derecho permite crear objetos de un sistema."
        // ]);

        // $derecho2 = Derecho::create([
        //     'nombre' => "Editar",
        //     'descripcion' => "Este derecho permite editar objetos de un sistema."
        // ]);

        // $derecho3 = Derecho::create([
        //     'nombre' => "Eliminar",
        //     'descripcion' => "Este derecho permite eliminar objetos de un sistema."
        // ]);

        // $derecho4= Derecho::create([
        //     'nombre' => "Ver",
        //     'descripcion' => "Este derecho permite ver objetos de un sistema."
        // ]);

        //Derechos
        // $derecho1 = Derecho::all()->filter(function($record) {
        //     if(strpos($record->nombre,"Crear") !== false) {
        //         return $record;
        //     }
        // })->first();

        // $derecho2 = Derecho::all()->filter(function($record) {
        //     if(strpos($record->nombre,"Editar") !== false) {
        //         return $record;
        //     }
        // })->first();

        // $derecho3 = Derecho::all()->filter(function($record) {
        //     if(strpos($record->nombre,"Eliminar") !== false) {
        //         return $record;
        //     }
        // })->first();

        // $derecho4= Derecho::all()->filter(function($record) {
        //     if(strpos($record->nombre,"Ver") !== false) {
        //         return $record;
        //     }
        // })->first();


        // //Sistemas
        // $sis1 = Sistema::all()->filter(function($record) {
        //     if(strpos($record->nombre,"Departamentos") !== false) {
        //         return $record;
        //     }
        // })->first();
        // $sis1=Sistema::all()->filter(function($record) {
        //     if(strpos($record->nombre,"Sistemas") !== false) {
        //         return $record;
        //     }
        // })->first();
        // $sis2=Sistema::all()->filter(function($record) {
        //     if(strpos($record->nombre,"Roles") !== false) {
        //         return $record;
        //     }
        // })->first();
        // $sis3=Sistema::all()->filter(function($record) {
        //     if(strpos($record->nombre,"Personal") !== false) {
        //         return $record;
        //     }
        // })->first();
        // $sis4=Sistema::all()->filter(function($record) {
        //     if(strpos($record->descripcion,"Nuevo Proceso | Adquisiciones") !== false) {
        //         return $record;
        //     }
        // })->first();
        // $sis7=Sistema::all()->filter(function($record) {
        //     if(strpos($record->descripcion,"Infimas | Aquisiciones") !== false) {
        //         return $record;
        //     }
        // })->first();
        // $sis8=Sistema::all()->filter(function($record) {
        //     if(strpos($record->descripcion,"Autogestión | Adquisiciones") !== false) {
        //         return $record;
        //     }
        // })->first();
        // $sis9=Sistema::all()->filter(function($record) {
        //     if(strpos($record->descripcion,"Productos | Adquisiciones") !== false) {
        //         return $record;
        //     }
        // })->first();
        // $sis10=Sistema::all()->filter(function($record) {
        //     if(strpos($record->descripcion,"Reportes | Adquisiciones") !== false) {
        //         return $record;
        //     }
        // })->first();
        // $sis11=Sistema::all()->filter(function($record) {
        //     if(strpos($record->descripcion,"Configuraciones | Adquisiciones") !== false) {
        //         return $record;
        //     }
        // })->first();
        // $sis12=Sistema::all()->filter(function($record) {
        //     if(strpos($record->descripcion,"Nuevo Ticket | Help Desk") !== false) {
        //         return $record;
        //     }
        // })->first();
        // $sis13=Sistema::all()->filter(function($record) {
        //     if(strpos($record->descripcion,"En Proceso | Help Desk") !== false) {
        //         return $record;
        //     }
        // })->first();
        // $sis14=Sistema::all()->filter(function($record) {
        //     if(strpos($record->descripcion,"Historial | Help Desk") !== false) {
        //         return $record;
        //     }
        // })->first();
        // $sis15=Sistema::all()->filter(function($record) {
        //     if(strpos($record->descripcion,"Reportes | Help Desk") !== false) {
        //         return $record;
        //     }
        // })->first();


        //Bindings
        // $sisUser1=UserSistema::create(['user_id'=>$user->id,'sistema_id'=>$sis1->id]);
        // $sisUser1->derechos()->attach($derecho1);
        // $sisUser1->derechos()->attach($derecho2);
        // $sisUser1->derechos()->attach($derecho3);
        // $sisUser1->derechos()->attach($derecho4);

        // $sisUser2=UserSistema::create(['user_id'=>$user->id,'sistema_id'=>$sis2->id]);
        // $sisUser2->derechos()->attach($derecho1);
        // $sisUser2->derechos()->attach($derecho2);
        // $sisUser2->derechos()->attach($derecho3);
        // $sisUser2->derechos()->attach($derecho4);

        // $sisUser3=UserSistema::create(['user_id'=>$user->id,'sistema_id'=>$sis3->id]);
        // $sisUser3->derechos()->attach($derecho1);
        // $sisUser3->derechos()->attach($derecho2);
        // $sisUser3->derechos()->attach($derecho3);
        // $sisUser3->derechos()->attach($derecho4);

        // $sisUser4=UserSistema::create(['user_id'=>$user->id,'sistema_id'=>$sis4->id]);
        // $sisUser4->derechos()->attach($derecho1);
        // $sisUser4->derechos()->attach($derecho2);
        // $sisUser4->derechos()->attach($derecho3);
        // $sisUser4->derechos()->attach($derecho4);

        // $sisUser7=UserSistema::create(['user_id'=>$user->id,'sistema_id'=>$sis7->id]);
        // $sisUser7->derechos()->attach($derecho1);
        // $sisUser7->derechos()->attach($derecho2);
        // $sisUser7->derechos()->attach($derecho3);
        // $sisUser7->derechos()->attach($derecho4);

        // $sisUser8=UserSistema::create(['user_id'=>$user->id,'sistema_id'=>$sis8->id]);
        // $sisUser8->derechos()->attach($derecho1);
        // $sisUser8->derechos()->attach($derecho2);
        // $sisUser8->derechos()->attach($derecho3);
        // $sisUser8->derechos()->attach($derecho4);
        
        // $sisUser9=UserSistema::create(['user_id'=>$user->id,'sistema_id'=>$sis9->id]);
        // $sisUser9->derechos()->attach($derecho1);
        // $sisUser9->derechos()->attach($derecho2);
        // $sisUser9->derechos()->attach($derecho3);
        // $sisUser9->derechos()->attach($derecho4);

        // $sisUser10=UserSistema::create(['user_id'=>$user->id,'sistema_id'=>$sis10->id]);
        // $sisUser10->derechos()->attach($derecho1);
        // $sisUser10->derechos()->attach($derecho2);
        // $sisUser10->derechos()->attach($derecho3);
        // $sisUser10->derechos()->attach($derecho4);

        // $sisUser11=UserSistema::create(['user_id'=>$user->id,'sistema_id'=>$sis11->id]);
        // $sisUser11->derechos()->attach($derecho1);
        // $sisUser11->derechos()->attach($derecho2);
        // $sisUser11->derechos()->attach($derecho3);
        // $sisUser11->derechos()->attach($derecho4);

        // $sisUser12=UserSistema::create(['user_id'=>$user->id,'sistema_id'=>$sis12->id]);
        // $sisUser12->derechos()->attach($derecho1);
        // $sisUser12->derechos()->attach($derecho2);
        // $sisUser12->derechos()->attach($derecho3);
        // $sisUser12->derechos()->attach($derecho4);

        // $sisUser13=UserSistema::create(['user_id'=>$user->id,'sistema_id'=>$sis13->id]);
        // $sisUser13->derechos()->attach($derecho1);
        // $sisUser13->derechos()->attach($derecho2);
        // $sisUser13->derechos()->attach($derecho3);
        // $sisUser13->derechos()->attach($derecho4);

        // $sisUser14=UserSistema::create(['user_id'=>$user->id,'sistema_id'=>$sis14->id]);
        // $sisUser14->derechos()->attach($derecho1);
        // $sisUser14->derechos()->attach($derecho2);
        // $sisUser14->derechos()->attach($derecho3);
        // $sisUser14->derechos()->attach($derecho4);

        // $sisUser15=UserSistema::create(['user_id'=>$user->id,'sistema_id'=>$sis15->id]);
        // $sisUser15->derechos()->attach($derecho1);
        // $sisUser15->derechos()->attach($derecho2);
        // $sisUser15->derechos()->attach($derecho3);
        // $sisUser15->derechos()->attach($derecho4);

        // return $user;



    }

    public function getCPCcsv(){
        $csv=$this->csv_to_array("../storage/app/public/csv/cpc.csv",",");
        foreach ($csv as $cpc){

            $pcpc = ProductoCPC::all()->filter(function($record) use ($cpc) {
                if($record->codigo === $cpc['CPC']) {
                    return $record;
                }
            })->first();
            // $pcpc=ProductoCPC::where('codigo',$cpc['CPC'])->first();

            // return $pcpc->descripcion;

            if($pcpc->descripcion !== $cpc['Descripcion']){
                return $pcpc;
            }
        }

        return "nada";
    }

    public function saveCPC(Request $request){
        $now = Carbon::now();
        $now->diffForHumans();
        $file      = $request->file('cpc');
        // $filename  = $file->getClientOriginalName();
        // $extension = $file->getClientOriginalExtension();
        $image_uploaded_path=$file->store($now->year."/cpc",'public');
        // $path=url('/').'/storage/'.$image_uploaded_path;
        $path=public_path() . '/' .$image_uploaded_path;
        $collection = (new FastExcel)->import(storage_path('app/public/'.$image_uploaded_path));
        // return url('/').'/storage/'.$image_uploaded_path;
        // $collection = (new FastExcel)->import(storage_path($path));

        foreach ($collection as $cpc){
            $cpcDB = ProductoCPC::where('codigo', '=',$cpc["CPC N9"])->first();
            if($cpcDB === null){
                if($cpc["Objeto de la contratación"]=="CATALOGADO"){
                    $cc = ProductoCPC::create(['codigo' => $cpc["CPC N9"],'descripcion'=>$cpc["Descripción del Producto"],'catalogado'=>true]);
                    // return $cc;
                }
                else{
                    $cc = ProductoCPC::create(['codigo' => $cpc["CPC N9"],'descripcion'=>$cpc["Descripción del Producto"],'catalogado'=>false]);

                }

            }
        }
        // return $collection;
    }

    public function addTipoUserProceso(){
        $tipo1 = new TipoUsuarioProceso;
        $tipo1->nombre="Requieriente";
        $tipo1->descripcion="Director de departamento";
        $tipo1->codigo="req";
        $tipo1->save();

        $tipo2 = new TipoUsuarioProceso;
        $tipo2->nombre="Administrador";
        $tipo2->descripcion="Director de departamento";
        $tipo2->codigo="adm";
        $tipo2->save();

        $tipo3 = new TipoUsuarioProceso;
        $tipo3->nombre="Encargada de proceso";
        $tipo3->descripcion="Personal del sub-departamento";
        $tipo3->codigo="enc";
        $tipo3->save();

        $tipo4 = new TipoUsuarioProceso;
        $tipo4->nombre="Elaborado por";
        $tipo4->descripcion="Elabora el proceso en el sistema";
        $tipo4->codigo="elb";
        $tipo4->save();
    }
}
