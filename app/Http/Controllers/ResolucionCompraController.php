<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ResolucionCompra;
use App\Proceso;
use App\Cotizacion;
use App\Control;
use App\UserSistemaControl;
use Carbon\Carbon;
use PDF;
use Illuminate\Support\Facades\Crypt;
use Luecano\NumeroALetras\NumeroALetras;
use App\User;

class ResolucionCompraController extends Controller
{
    //
    public function __construct()
    {
        
        $this->middleware('auth:api', ['except' => []]);
    }

    public function store(Request $request)
    {
        $res_comp = new ResolucionCompra();
        $res_comp->fecha_aprobado=$request->fecha_aprobado;
        $res_comp->save();

        $proceso = Proceso::findOrFail($request->proceso_id);
        $proceso->load('clasificacion');
        
        $res_comp->proceso()->associate($proceso)->save();

        // // if(isset($request->f_emision) ){

        // $proceso->clasificacion()->first()->pivot->f_aprobado=;

        // $proceso->clasificacion()->sync(
        //     $request->fecha_aprobado
        // );
        
        // $proceso->clasificacion()->updateExistingPivot(['f_emisión'=>$proceso->fecha_inicio,
        // 'f_aprobado'=>$request->fecha_aprobado]);
        // $proceso->clasificacion()->pivot->save();
        // $proceso->clasificacion->first()->pivot->f_aprobado=$request->fecha_aprobado;
         
        $proceso->clasificacion()->updateExistingPivot($proceso->clasificacion->first(),
        ['f_emisión'=>$proceso->fecha_inicio],false);

        // return $proceso;
        // // }
        if(isset($request->fecha_aprobado) ){
            $proceso->clasificacion()->updateExistingPivot($proceso->clasificacion->first(),
            ['f_aprobado'=>$request->fecha_aprobado],false);
            // $proceso->clasificacion()->attach(['f_aprobado'=>$request->fecha_aprobado]);
            // $proceso->save();
        }

        $cotizacion = Cotizacion::with('proveedor')->where('id',$request->cot_id)->first();
        $res_comp->cotizacion()->associate($cotizacion)->save();


        $splt = explode("-", $proceso->codigo);
        $path='storage/'.$splt[2];

        if($proceso->tipo_procesos[0]->codigo=='IC'){
            $path=$path.'/infima/'.$splt[1].'/'.$proceso->codigo.'/'.'RC-'.$proceso->codigo.'.pdf';
        }
        else{
            $path=$path.'/autogestion/'.$splt[1].'/'.$proceso->codigo.'/'.'RC-'.$proceso->codigo.'.pdf';
        }

        foreach($proceso->tipo_procesos_usuarios_get as $tps){
            $tps->nombre=Crypt::decryptString($tps->nombre);
            $tps->descripcion=Crypt::decryptString($tps->descripcion);
            $tps->codigo=Crypt::decryptString($tps->codigo);
        }

        $proceso->load('tipo_procesos','tipo_procesos_usuarios_get','resolucion_compra.cotizacion.proveedor','resolucion_compra.cotizacion.items');
        view()->share('data',$proceso);
        $pdf=PDF::loadView('pdf/resolucion_compra', $proceso)->setPaper('a4', 'portrait')->setWarnings(false)->save(public_path($path));

        $url=url('/').'/'.$path;
        $res_comp->document=$url;
        $res_comp->save();

        return $res_comp;

    }

    public function update(Request $request, $id)
    {
        $res_comp = ResolucionCompra::findOrFail($id);
        $res_comp->fecha_aprobado=$request->fecha_aprobado;
        $res_comp->update();

        $proceso = Proceso::findOrFail($request->proceso_id);

        $proceso->load('clasificacion');
        $proceso->clasificacion()->updateExistingPivot($proceso->clasificacion->first(),
        ['f_emisión'=>$proceso->fecha_inicio],false);

        // return $proceso;
        // // }
        if(isset($request->fecha_aprobado) ){
            $proceso->clasificacion()->updateExistingPivot($proceso->clasificacion->first(),
            ['f_aprobado'=>$request->fecha_aprobado],false);
            // $proceso->clasificacion()->attach(['f_aprobado'=>$request->fecha_aprobado]);
            // $proceso->save();
        }


        $res_comp->proceso()->associate($proceso)->save();
        $proceso->load('tipo_procesos','tipo_procesos_usuarios_get','resolucion_compra.cotizacion.proveedor','resolucion_compra.cotizacion.items');

        $cotizacion = Cotizacion::with('proveedor')->where('id',$request->cot_id)->first();
        $res_comp->cotizacion()->associate($cotizacion)->save();

        $splt = explode("-", $proceso->codigo);
        $path='storage/'.$splt[2];

        if($proceso->tipo_procesos[0]->codigo=='IC'){
            $path=$path.'/infima/'.$splt[1].'/'.$proceso->codigo.'/'.'RC-'.$proceso->codigo.'.pdf';
        }
        else{
            $path=$path.'/autogestion/'.$splt[1].'/'.$proceso->codigo.'/'.'RC-'.$proceso->codigo.'.pdf';
        }

        foreach($proceso->tipo_procesos_usuarios_get as $tps){
            $tps->nombre=Crypt::decryptString($tps->nombre);
            $tps->descripcion=Crypt::decryptString($tps->descripcion);
            $tps->codigo=Crypt::decryptString($tps->codigo);
        }

        view()->share('data',$proceso);
        $pdf=PDF::loadView('pdf/resolucion_compra', $proceso)->setPaper('a4', 'portrait')->setWarnings(false)->save(public_path($path));

        $url=url('/').'/'.$path;
        $res_comp->document=$url;
        $res_comp->save();
        
        return $res_comp;

    }

    public function updateOrden(Request $request, $id){
        $proceso = Proceso::findOrFail($id);

        $proceso->load('clasificacion','resolucion_compra.cotizacion.items.cpc','resolucion_compra.cotizacion.proveedor','user_autoriza','cuadro_comparativo.cotizaciones.proveedor');

         //User autoriza
         $control= Control::all()->filter(function($record)  {
            if(($record->codigo) == '001') {
            return $record;
        }
        })->first();

        $userSisCont=UserSistemaControl::whereHas(
            'control' , function ($query) use ($control){
                $query->where('id', $control->id);
            }
            
        )->orderBy('created_at', 'desc')->first();

        if(isset($userSisCont)){
            $userSisCont->load('SistemaUser.user');
            
            $user_auth=User::find($userSisCont->SistemaUser->user->id);
            $proceso->user_autoriza()->associate($user_auth);
        }
        $proceso->save();
        
        if(isset($request->observacion) ){
            $proceso->clasificacion()->updateExistingPivot($proceso->clasificacion->first(),
            ['observaciones'=>$request->observacion],false);
            // $proceso->clasificacion()->attach(['f_aprobado'=>$request->fecha_aprobado]);
            $proceso->save();
        }

        $splt = explode("-", $proceso->codigo);
        $path='storage/'.$splt[2];
        // return $proceso;

        if($request->finalizado){
            // $proceso->clasificacion()->updateExistingPivot($proceso->clasificacion->first(),
            // ['observaciones'=>$request->observaciones],false);
            // $proceso->clasificacion()->attach(['f_aprobado'=>$request->fecha_aprobado]);
            // $proceso->save();

            if($proceso->tipo_procesos[0]->codigo=='IC'){
                if($proceso->clasificacion[0]->codigo=='S'){
                    $path=$path.'/infima/'.$splt[1].'/'.$proceso->codigo.'/'.'OT-'.$proceso->codigo.'.pdf';
                }
                else{
                    $path=$path.'/infima/'.$splt[1].'/'.$proceso->codigo.'/'.'OC-'.$proceso->codigo.'.pdf';
                }
            }
            else{
                if($proceso->clasificacion[0]->codigo=='S'){
                    $path=$path.'/autogestion/'.$splt[1].'/'.$proceso->codigo.'/'.'OT-'.$proceso->codigo.'.pdf';
                }
                else{
                    $path=$path.'/autogestion/'.$splt[1].'/'.$proceso->codigo.'/'.'OC-'.$proceso->codigo.'.pdf';
                }
                
            }
    
            foreach($proceso->tipo_procesos_usuarios_get as $tps){
                $tps->nombre=Crypt::decryptString($tps->nombre);
                $tps->descripcion=Crypt::decryptString($tps->descripcion);
                $tps->codigo=Crypt::decryptString($tps->codigo);
            }
            // return $proceso;

            view()->share('data',$proceso);
            $pdf=PDF::loadView('pdf/orden', $proceso)->setPaper('a4', 'portrait')->setWarnings(false)->save(public_path($path));

            $url=url('/').'/'.$path;
            $proceso->clasificacion()->updateExistingPivot($proceso->clasificacion->first(),
            ['file_ordenes'=>$url],false);
            // $res_comp->document=$url;
            // $res_comp->save();
            // $fmt = new \NumberFormatter('es', \NumberFormatter::SPELLOUT);
            $proceso->activo=false;
            $proceso->save();
            return $proceso;
            // return $formatter->toMoney($proceso->resolucion_compra->cotizacion->items[0]->pivot->p_unitario, 2, 'DÓLARES', '');
            // return NumeroALetras::convertir($data->resolucion_compra->cotizacion->items[0]->pivot[0]->p_unitario);
            // return $fmt->format($data->resolucion_compra->cotizacion->items[0]->pivot[0]->p_unitario);;
        }
    }
}
