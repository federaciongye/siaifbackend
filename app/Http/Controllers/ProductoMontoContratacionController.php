<?php

namespace App\Http\Controllers;

use App\ProductoMontoContratacion;
use Illuminate\Http\Request;

class ProductoMontoContratacionController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $productoMontoContrataciones = ProductoMontoContratacion::all();
            return $productoMontoContrataciones;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $productoMontoContrataciones = ProductoMontoContratacion::create($request->all());
            $productoMontoContrataciones = ProductoMontoContratacion::all();
            return $productoMontoContrataciones;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductoMontoContratacion  $productoMontoContratacion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $productoMontoContratacion = ProductoMontoContratacion::findOrFail($id);
            return $productoMontoContratacion;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductoMontoContratacion  $productoMontoContratacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $productoMontoContratacion = ProductoMontoContratacion::findOrFail($id);
            $productoMontoContratacion->update($request->all());
            $productoMontoContrataciones = ProductoMontoContratacion::all();
            return $productoMontoContrataciones;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductoMontoContratacion  $productoMontoContratacion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            ProductoMontoContratacion::destroy($id);
            $productoMontoContrataciones = ProductoMontoContratacion::all();
            return $productoMontoContrataciones;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
