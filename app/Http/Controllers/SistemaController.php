<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sistema;
use Exception;

class SistemaController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Listado de sistemas
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $sistemas = Sistema::with('users','parent','children')->get();
            return $sistemas;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Creación de sistemas
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $sistema = Sistema::create($request->all());
            $sis = $this->index();
            return $sis;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Muestra de información de sistema
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $sistema = Sistema::findOrFail($id);
            return $sistema;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Edicion de sistema
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {

            $sistema = Sistema::findOrFail($id);
            $sistema->update($request->all());
            $sis = $this->index();
            return $sis;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Eliminacion de sistema.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            Sistema::destroy($id);
            $sis = $this->index();
            return $sis;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
