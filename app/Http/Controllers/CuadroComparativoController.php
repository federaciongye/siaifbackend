<?php

namespace App\Http\Controllers;

use App\CuadroComparativo;
use Illuminate\Http\Request;
use App\Proceso;
use App\ItemCuadroComparativo;
use PDF;
use Illuminate\Support\Facades\Crypt;

class CuadroComparativoController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $cuadrosComparativos = CuadroComparativo::all();
            return $cuadrosComparativos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $cuadrosComparativos = CuadroComparativo::create($request->all());
            // $cuadrosComparativos = CuadroComparativo::all();
            // return $cuadrosComparativos;

        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CuadroComparativo  $cuadroComparativo
     * @return \Illuminate\Http\Response
     */
    public function getById($request)
    {
        try {
            $cuadroComparativo = CuadroComparativo::findOrFail($request->id)
            ->with('proceso','items','cotizaciones');
            return $cuadroComparativo;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CuadroComparativo  $cuadroComparativo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $cuadroComparativo = CuadroComparativo::findOrFail($id);
            $cuadroComparativo->update(array(
                'fecha_aprobado'=>$request->fecha_aprobado,
                'observaciones'=>$request->observaciones,
            ));
            if($cuadroComparativo->completado){
                $cc=$cuadroComparativo->load('proceso.tipo_procesos','proceso.clasificacion','proceso.tipo_procesos_usuarios_get','items.cotizaciones.items','cotizaciones.proveedor','cotizaciones.items');
                $proc=$cc->proceso;

                $splt = explode("-", $proc->codigo);
                $path='storage/'.$splt[2];


                if($proc->tipo_procesos[0]->codigo=='IC'){
                    $path=$path.'/infima/'.$splt[1].'/'.$proc->codigo.'/'.'CC-'.$proc->codigo.'.pdf';
                }
                else{
                    $path=$path.'/autogestion/'.$splt[1].'/'.$proc->codigo.'/'.'CC-'.$proc->codigo.'.pdf';
                }

                foreach($cc->proceso->tipo_procesos_usuarios_get as $tps){
                    $tps->nombre=Crypt::decryptString($tps->nombre);
                    $tps->descripcion=Crypt::decryptString($tps->descripcion);
                    $tps->codigo=Crypt::decryptString($tps->codigo);
                }
        
                view()->share('data',$cc);
                $pdf=PDF::loadView('pdf/cuadro_comparativo', $proc)->setPaper('a4', 'landscape')->setWarnings(false)->save(public_path($path));
                // return $pdf;

                $url=url('/').'/'.$path;
                $cc->document=$url;
                $cc->save();
                // return $url;
            }
            // $cuadrosComparativos = CuadroComparativo::all();
            // return $cuadroComparativo->load('proceso','items',);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CuadroComparativo  $cuadroComparativo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            CuadroComparativo::destroy($id);
            $cuadrosComparativos = CuadroComparativo::all();
            return $cuadrosComparativos;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function completarCuadComp($id)
    {
        
        $cuadroComparativo = CuadroComparativo::find($id);
        $cuadroComparativo->completado=true;
        // $cuadroComparativo->update(array(
        //     'completado'=>true
        // ));
        $cuadroComparativo->save();
        $cc=$cuadroComparativo->load('proceso.tipo_procesos','proceso.clasificacion','proceso.tipo_procesos_usuarios_get','items.cotizaciones.items','cotizaciones.proveedor','cotizaciones.items');
        $proc=$cc->proceso;
        
        $splt = explode("-", $proc->codigo);
        $path='storage/'.$splt[2];


        if($proc->tipo_procesos[0]->codigo=='IC'){
            $path=$path.'/infima/'.$splt[1].'/'.$proc->codigo.'/'.'CC-'.$proc->codigo.'.pdf';
        }
        else{
            $path=$path.'/autogestion/'.$splt[1].'/'.$proc->codigo.'/'.'CC-'.$proc->codigo.'.pdf';
        }

        foreach($cc->proceso->tipo_procesos_usuarios_get as $tps){
            $tps->nombre=Crypt::decryptString($tps->nombre);
            $tps->descripcion=Crypt::decryptString($tps->descripcion);
            $tps->codigo=Crypt::decryptString($tps->codigo);
        }
        view()->share('data',$cc);
        $pdf=PDF::loadView('pdf/cuadro_comparativo', $cc)->setPaper('a4', 'landscape')->setWarnings(false)->save(public_path($path));

        $url=url('/').'/'.$path;
        $cuadroComparativo->document=$url;
        $cuadroComparativo->save();
    }
}
