<?php

namespace App\Http\Controllers;

use App\UserSistemaControl;
use App\UserSistema;
use App\User;
use App\MontoContratacion;
use Carbon\Carbon;
use App\ProductoMontoContratacion;
use App\Control;
use Illuminate\Http\Request;
use DB;

class UserSistemasControlController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api', ['except' => []]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $usersSistemasControls = UserSistemaControl::with('SistemaUser.user','SistemaUser.control')->latest();
            return $usersSistemasControls;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $usersSistemasControls = UserSistemaControl::create($request->all());
            $usersSistemasControls = UserSistemaControl::all();
            return $usersSistemasControls;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserSistemaControl  $userSistemaControl
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $userSistemaControl = UserSistemaControl::findOrFail($id);
            return $userSistemaControl;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserSistemaControl  $userSistemaControl
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $userSistemaControl = UserSistemaControl::findOrFail($id);
            $userSistemaControl->update($request->all());
            $usersSistemasControls = UserSistemaControl::all();
            return $usersSistemasControls;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserSistemaControl  $userSistemaControl
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            UserSistemaControl::destroy($id);
            $usersSistemasControls = UserSistemaControl::all();
            return $usersSistemasControls;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function getConfigAdq(){
        try {
            $now = Carbon::now();
            $now->diffForHumans();
            $user=User::whereHas(
                'departamento' , function ($query) {
                    $query->where('codigo', 'GG');
                }
                
            )->whereHas(
                'roles' , function ($query) {
                    $query->where('codigo', '001');
                }
            )->get();

            $control= Control::all()->filter(function($record)  {
                if(($record->codigo) == '001') {
                return $record;
            }
            })->first();

            $userSisCont=UserSistemaControl::whereHas(
                'control' , function ($query) use ($control){
                    $query->where('id', $control->id);
                }
                
            )->whereYear('created_at', '=', $now->year)->first();

            if(isset($userSisCont)){
                $userSisCont->load('SistemaUser.user');
            }
            

            // $monto=MontoContratacion::where('ano', $now->year)->first();
            $monto=MontoContratacion::where('ano', $now->year)->orderBy('created_at', 'desc')->first();
            return response()->json([
                'user' => $user,
                'userSisCont' => $userSisCont,
                'monto'=>$monto
            ]);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    public function postConfigAdq(Request $request){

        //Administrador

        $now = Carbon::now();
        $now->diffForHumans();

        // $userSistemaControl = UserSistemaControl::findOrFail($id);

        $control= Control::all()->filter(function($record)  {
            if(($record->codigo) == '001') {
            return $record;
        }
        })->first();

        $userSisCont1=UserSistemaControl::whereHas(
            'control' , function ($query) use ($control){
                $query->where('id', $control->id);
            }
            
        )->whereHas(
            'SistemaUser' , function ($query) use ($request){
                $query->where('user_id', (int)$request->id);
            }
            
        )->whereYear('created_at', '=', $now->year)->first();

       

        $userSisCont2=UserSistemaControl::whereHas(
            'control' , function ($query) use ($control){
                $query->where('id', $control->id);
            }
            
        )->where( DB::raw('YEAR(created_at)'), '=', $now->year )->latest()->first();
        // ->whereYear('created_at', '=', $now->year)->first();
        

        $userSis=UserSistema::where(
            'user_id', (int)$request->id
        )->first();

        $userSisCont=new UserSistemaControl;
        // return $userSisCont1;
        if(isset($userSisCont1)&& isset($userSisCont2)){
            $userSisCont1->load('SistemaUser.user');
            $userSisCont2->load('SistemaUser.user');
            
            if(isset($userSisCont2->SistemaUser)){
                if($userSisCont1->SistemaUser->user->id != $userSisCont2->SistemaUser->user->id){
                
                    $userSisCont->SistemaUser()->associate($userSis);
                    $userSisCont->control()->associate($control);
                    $userSisCont->save();
                    // return $userSisCont;
                    return response()->json([
                        'u1' => $userSisCont
                    ]);
                }

            }
            else{
                // return $userSisCont2;
                // $userSisCont->SistemaUser()->associate($userSis);
                //     $userSisCont->control()->associate($control);
                //     $userSisCont->save();
                //     // return $userSisCont;
                //     return response()->json([
                //         'u2' => $userSisCont
                //     ]);
            }
            
        }
        else{
            
            $userSisCont->SistemaUser()->associate($userSis);
            $userSisCont->control()->associate($control);
            $userSisCont->save();
            // return $userSisCont;
           
        }
        
        $monto1=MontoContratacion::orderBy('created_at', 'desc')->first();

        $monto2=MontoContratacion::where('ano', $now->year)->orderBy('created_at', 'desc')->first();

        $flag=false;
        if(isset($monto1)&& isset($monto2))
        {
            if($monto1->ano==$monto2->ano && (int)$monto1->monto!=(int)$monto2->monto){
                $flag=true;
            }
            if((int)$monto1->ano<(int)$monto2->ano){
                $flag=true;
            }
            if((int)$monto2->ano!=$request->monto){
                $flag=true;
            }
        }

        if(!isset($monto2)){
            $flag=true;
        }

        if($flag){
            $newMonto=new MontoContratacion;
            $newMonto->ano=$now->year;
            $newMonto->monto=$request->monto;
            $newMonto->save();
        }
        return $this->getConfigAdq();
        
    }
}


