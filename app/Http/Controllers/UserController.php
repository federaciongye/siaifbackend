<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

use Validator;
use App\User;
use App\UserSistema;
use App\UserSistemaDerecho;
use App\Derecho;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Arr;
use App\Departamento;
use App\Sistema;
use App\Mail\AuthMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;

class UserController extends Controller
{
    //

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    //Listado de usuarios con sus respectivos roles, sistemas, departamentos y derechos.
    public function getUsers(){
        try {
            $users=User::with('roles','sistemas.parent.children','sistemas.children.parent','departamento')->get();
            $roles = Role::with('users')->get();
            $departamentos = Departamento::all();
            $sistemas = Sistema::with('users','parent.children','parent.parent','children')->get();
            $derechos= Derecho::all();
            return response()->json([
                'users' => $users,
                'roles' => $roles,
                'departamentos' => $departamentos,
                'sistemas'=>$sistemas,
                'derechos'=>$derechos,
                'deruser'=>$this->rightsByUser($users)
            ]);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    //Funcion que retorna derechos de usuario por cada sistema
    private function rightsByUser($users){
        try {
            $rights=[];

            foreach($users as $u){
                $sis = [];
                foreach ($u['sistemas'] as $s) {
                    $der=DB::table('users')
                    ->join('sistema_user','users.id','sistema_user.user_id')
                    ->join('sistemas','sistema_user.sistema_id','sistemas.id')
                    ->join('sistema_user_derecho','sistema_user.id','sistema_user_derecho.user_sistema_id')
                    ->join('derechos','sistema_user_derecho.derecho_id','derechos.id')
                    ->select( 
                        'derechos.id'
                    )
                    ->where('users.id',$u->id)
                    ->where('sistemas.id',$s->id)->get('id')->toArray();
        
                    $deres=Derecho::whereIn('id',Arr::pluck($der,'id'))->get();
        
                    $sis[$s->id]=$deres;
                }
                $rights[$u->id]=$sis;
            }
            return $rights;
        } catch(Exception $e) {
            return response()->json(['error' => 'no se pudo ingresar registro'], 500);
        }
    }

    //Creacion de usuario. Al crear envia un correo para activación de cuenta.
    public function createUser(Request $request){
        try {
            $pass=substr($request->name, 3)."".substr($request->last_name, 3)."".substr($request->cedula, -3)."";
            $user = new User();
            $user->name=$request->name;
            $user->last_name=$request->last_name;
            $user->email=$request->email;
            $user->password=bcrypt($pass);
            $user->telef=$request->telef;
            $user->direccion=$request->direccion;
            $user->cedula=$request->cedula;

            $role = Role::findOrFail($request->rol_id);
            $user->assignRole($role);

            $departamento = Departamento::findOrFail($request->departamento_id);
            $user->departamento()->associate($departamento);
            $user->save();
            foreach($request->derechosSis as $key => $value) 
            {
                $sisUser=UserSistema::create(['user_id'=>$user->id,'sistema_id'=>$key]);
                foreach($value as $d){
                    $derecho = Derecho::findOrFail($d);
                    $sisUser->derechos()->attach($derecho);
                }
            }

            //Enviar Correo de confirmación de registro (Revisar readme para mas información)
            Mail::to($user->email)->send(new AuthMail($user,$pass));
            return $this->getUsers();
        } catch(Exception $e) {
            return response()->json(['error' => 'no se pudo ingresar registro'], 500);
        }
    }

    //Edición de usuario
    public function editUser(Request $request){
        try {
            $user = User::with('roles','sistemas','departamento')
            ->where('id',$request->id)->first();
            $user->name=$request->name;
            $user->last_name=$request->last_name;
            $user->email=$request->email;
            $user->telef=$request->telef;
            $user->direccion=$request->direccion;
            $user->cedula=$request->cedula;

            if(count($user->roles)==0){
                $role = Role::findOrFail($request->rol_id);
                $user->assignRole($role);
            }
            else{
                //Edicion de rol de usuario
                if($user->roles[0]->id != $request->rol_id){
                    $user->removeRole($user->roles[0]->name);
                    $role = Role::findOrFail($request->rol_id);
                    $user->assignRole($role);
                }
            }
            
            

            

            //Edicion de departamento
            if(is_null($user->departamento)){
                $departamento = Departamento::findOrFail($request->departamento_id);
                $user->departamento()->associate($departamento);
            }

            if($user->departamento->id != $request->departamento_id){
                $user->departamento()->dissociate();
                $departamento = Departamento::findOrFail($request->departamento_id);
                $user->departamento()->associate($departamento);
            }
            $user->save();

            //Edicion de sistemas que puede acceder el usuario
            $SisUser1=UserSistema::where('user_id',$user->id)->get();

            $SisUser2=[];
            foreach($request->derechosSis as $key => $value) 
            {
                $SisUser=UserSistema::where(['user_id'=>$user->id,'sistema_id'=>$key])->first();
                if($SisUser!=null){
                    array_push($SisUser2, $SisUser);
                }
                else{
                    $SisUserOther=UserSistema::create(['user_id'=>$user->id,'sistema_id'=>$key]);
                    array_push($SisUser2, $SisUserOther);
                }
            }
            $out=$this->returnArrayNotInArryay($SisUser1,$SisUser2);

            UserSistema::destroy($this->array_pluck($out,'id'));


            //DERECHOS DE SISTEMAS
            $SisUserDerNew=[];
            foreach($request->derechosSis as $key => $value) 
            {
                $SisUser=UserSistema::where(['user_id'=>$user->id,'sistema_id'=>$key])->first();
                $SisUserDer=UserSistemaDerecho::whereIn('derecho_id', $value)
                ->where('user_sistema_id',$SisUser->id)->get();
                array_push($SisUserDerNew, $SisUserDer);
                if(sizeof($SisUserDer)==0){
                    foreach($value as $d){
                        $derecho = Derecho::findOrFail($d);
                        $SisUser->derechos()->attach($derecho);
                    }
                }
                else{
                    foreach($value as $d){
                        $SisUserDer3=UserSistemaDerecho::where('derecho_id', $d)
                        ->where('user_sistema_id',$SisUser->id)->first();
                        if(is_null($SisUserDer3)){
                            $derecho = Derecho::findOrFail($d);
                            $SisUser->derechos()->attach($derecho);
                        }
                    }
                }
            }
            $SisUserDerNow=UserSistemaDerecho::whereIn('user_sistema_id',Arr::pluck($SisUser1,'id'))->get();
            $out2=$this->returnArrayNotInArryay($SisUserDerNow,$this->returnArrOfObj($SisUserDerNew));

            return $this->getUsers();
        } catch(Exception $e) {
            return response()->json(['error' => 'no se pudo ingresar registro'], 500);
        }
    }

    function returnArrOfObj($arr){
        $new=[];
        foreach($arr as $el){
            if(sizeof($el)>0){
                foreach($el as $obj){
                    array_push($new, $obj);
                }
                
            }
        }
        return $new;
    }

    function returnArrayNotInArryay($arr1, $arr2){
        $out=array();
        foreach($arr1 as $ob){
            if (!in_array($ob,$arr2)) {
                array_push($out, $ob);
            }
        }
        return $out;
    }

    function array_pluck($array, $key) {
        return array_map(function($v) use ($key)	{
            return is_object($v) ? $v->$key : $v[$key];
        }, $array);
    }

    /**
     * Eliminación de usuario
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            User::destroy($id);
            return $this->getUsers();
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
