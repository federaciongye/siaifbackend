<?php

namespace App\Http\Controllers;

use App\TipoUsuarioProceso;
use Illuminate\Http\Request;

class TipoUsuarioProcesoController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $tipoUsuarioProcesos = Clasificacion::all();
            return $tipoUsuarioProcesos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $tipoUsuarioProcesos = Clasificacion::create($request->all());
            $tipoUsuarioProcesos = Clasificacion::all();
            return $tipoUsuarioProcesos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Clasificacion  $tipoUsuarioProceso
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $tipoUsuarioProceso = Clasificacion::findOrFail($id);
            return $tipoUsuarioProceso;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clasificacion  $tipoUsuarioProceso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $tipoUsuarioProceso = Clasificacion::findOrFail($id);
            $tipoUsuarioProceso->update($request->all());
            $tipoUsuarioProcesos = Clasificacion::all();
            return $tipoUsuarioProcesos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clasificacion  $tipoUsuarioProceso
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Clasificacion::destroy($id);
            $tipoUsuarioProcesos = Clasificacion::all();
            return $tipoUsuarioProcesos;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
