<?php

namespace App\Http\Controllers;

use App\ItemCPC;
use Illuminate\Http\Request;

class ItemCPCController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $ItemsCPCs = ItemCPC::all();
            return $ItemsCPCs;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $ItemsCPCs = ItemCPC::create($request->all());
            $ItemsCPCs = ItemCPC::all();
            return $ItemsCPCs;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemCPC  $itemCPC
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $itemCPC = ItemCPC::findOrFail($id);
            return $itemCPC;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemCPC  $itemCPC
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $itemCPC = ItemCPC::findOrFail($id);
            $itemCPC->update($request->all());
            $ItemsCPCs = ItemCPC::all();
            return $ItemsCPCs;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemCPC  $itemCPC
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            ItemCPC::destroy($id);
            $ItemsCPCs = ItemCPC::all();
            return $ItemsCPCs;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
