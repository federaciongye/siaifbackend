<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

use Validator;
use App\User;
use App\UserSistema;
use App\UserSistemaDerecho;
use App\Derecho;

use Arr;

use Illuminate\Support\Facades\Crypt;

class AuthController extends Controller
{
    //

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','logout']]);
    }

    
    /**
     * Registro de usuario.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'last_name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create(array_merge(
                    $validator->validated(),
                    ['password' => bcrypt($request->password)]
                ));

        $credentials = $request->only('email', 'password');

        if ($token = auth()->attempt($credentials)) {
            return $this->respondWithToken($token);
        }
        return response()->json(['error' => 'Unauthorized'], 401);
    }

    /**
     * Ingreso de usuario.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        try {
            $credentials = request(['email', 'password']);
            $token = auth()->attempt($credentials);
            if ( $token ) {
                return $this->respondWithToken($token);
            }
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }

        // return $this->respondWithToken($token);
    }

    /**
     * Informacion de usuario.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Salida de usuario.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Obtencion de informacion de autenticacion e informacion basica de derechos, sistemas, roles, etc.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $user=User::with('roles','sistemas.parent.children','departamento')->find(Auth::id());
        
        $sis = array();

        foreach ($user['sistemas'] as $s) {
            $der=DB::table('users')
            ->join('sistema_user','users.id','sistema_user.user_id')
            ->join('sistemas','sistema_user.sistema_id','sistemas.id')
            ->join('sistema_user_derecho','sistema_user.id','sistema_user_derecho.user_sistema_id')
            ->join('derechos','sistema_user_derecho.derecho_id','derechos.id')
            ->select( 
                'derechos.id'
            )
            ->where('users.id',$user->id)
            ->where('sistemas.id',$s->id)->get('id')->toArray();

            $deres=Derecho::whereIn('id',Arr::pluck($der,'id'))->get();

            $j=[
                'sistema'=>$s,
                'parent'=>$s->parent,
                'derechos'=>$deres
            ];
            array_push($sis,$j);
        }

        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'sistemas'=>$sis,
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => $user
        ]);
        return $user;
    }    
}
