<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\TipoTicket;
use Exception;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\EstadoTicket;
use App\User;
use App\Prioridad;

class TicketController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $tickets = Ticket::with('user.departamento',['estado_ticket' => function ($query) { 
                $query->orderBy('id', 'desc')->first();
        }])->get();
            return $tickets;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $date=Carbon::parse($request->fecha_evento)->format('Y-m-d H:i:s');
            $now = Carbon::now();
            $dept=Auth::user()->departamento;
            $estadoTicket=EstadoTicket::where('nombre', 'En proceso')->get();
            $ticket = Ticket::create([
                'titulo' => $request->titulo,
                'descripcion' => $request->descripcion,
                'activo' => $request->activo,
                'evento' => $request->evento,
                'fecha_evento' => $date,
                'user_id' => Auth::id(),
                'tipo_ticket_id' => $request->tipo_ticket_id,
            ]);
            $ticket->codigo=$dept->codigo.'-'.$now->year.'-'.$ticket->id;
            $ticket->estado_ticket()->attach($estadoTicket);
            $ticket->save();

            return $this->getActiveTickets();
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $ticket = Ticket::findOrFail($id);
            return $ticket;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {

            $ticket = Ticket::findOrFail($id);
            $ticket->update($request->all());
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            Ticket::destroy($id);
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function getTipoTicket(){
        try {
            $tipoTickets = TipoTicket::all();
            return $tipoTickets;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function getActiveTickets()
    {
        //
        try {
            $tickets = Ticket::with(['estado_ticket' => function ($query) { 
                $query->orderBy('id', 'desc')->first();
            },'tipo_ticket','user.departamento'])->where('activo','1')->get();
            
            $users=User::whereHas('departamento', function ($query) {
                $query->where('codigo','=','SIS');
            })->whereHas('roles', function ($query) {
                $query->where('codigo','=','001');
            })->with('sistemas.parent.children',
            'sistemas.children.parent','departamento' ,'roles')->get();

            $prioridades = Prioridad::all();
            return response()->json([
                'users' => $users,
                'tickets'=>$tickets,
                'prioridades'=>$prioridades
            ]);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    public function getInactiveTickets()
    {
        //
        try {
            $tickets = Ticket::with('user','tipo_ticket')->where('activo','0')->get();
            return $tickets;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    public function getActiveTicketsByTech()
    {
        //
        try {
            $tickets = Ticket::with('user','tipo_ticket')->where('activo','1')->where('tech_id',Auth::id())->get();
            return $tickets;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    public function getActiveTicketsByUser()
    {
        //
        try {
            $tickets = Ticket::with('user.departamento',['estado_ticket' => function ($query) { 
                $query->orderBy('id', 'desc')->first();
        }])->where('activo','1')->where('user_id',Auth::id())->get();
            return $tickets;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    public function getInactiveTicketsByTech()
    {
        //
        try {
            $tickets = Ticket::with('user','tipo_ticket')->where('activo','0')->where('tech_id',Auth::id())->get();
            return $tickets;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    public function getInactiveTicketsByUser()
    {
        //
        try {
            $tickets = Ticket::with('user','tipo_ticket')->where('activo','0')->where('user_id',Auth::id())->get();
            return $tickets;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    public function getDeskData(){
        try{
            

            

        }
        catch(Exception $e){
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

}
