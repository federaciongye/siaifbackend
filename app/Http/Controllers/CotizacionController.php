<?php

namespace App\Http\Controllers;

use App\Cotizacion;
use App\Items;
use App\Proveedor;
use App\CuadroComparativo;

use Illuminate\Http\Request;

class CotizacionController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api',['except' => ['store','update']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $cotizaciones = Cotizacion::all();
            return $cotizaciones;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $cotizacion = Cotizacion::create($request->only(
                1
                , $request->garantia
                , $request->f_pago
                ,$request->f_entrega,
                $request->seleccionado
                , $request->observaciones
                , $request->califica
                ,$request->iva
            ));

            if(isset($request->items)){
                foreach ($request->items as $item){ 
                    $it=Items::find($item['id']);
                    $sub=round(intval($item['cantidad'])*floatval($item['p_unitario']),2);
                    $descVal=round($sub*(intval($item['descuento'])/100),2);
                    $sub=round($sub-$descVal,2);

                    $iva=0;
                    if($item['iva_ap']){
                        // if($request->iva){
                            $iva=round($sub*0.12,2);
                        // }
                     
                    }
                    else{
                        $iva=0;
                    }
                    

                    $total=round($sub+$iva,2);

                    $cotizacion->items()->attach($it,[
                        'p_unitario'=>$item['p_unitario'],
                        'desc_porcentaje'=>$item['descuento'],
                        'desc_valor'=>$descVal,
                        'subtotal'=>$sub,
                        'iva'=>$iva,
                        'iva_ap'=>$item['iva_ap'],
                        'total'=>$total
                    ]);
                    $cotizacion->save();
                }
            }
            $proveedor=Proveedor::find($request->empresa);
            $cotizacion->proveedor()->associate($proveedor)->save();

            $ccomp=CuadroComparativo::find($request->cc_id);
            $cotizacion->cuadro_comparativo()->associate($ccomp)->save();

            return $cotizacion->load('items','cuadro_comparativo.proceso','cuadro_comparativo.cotizaciones.items.cpc','proveedor');
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cotizacion  $cotizacion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $cotizacion = Cotizacion::findOrFail($id);
            return $cotizacion;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cotizacion  $cotizacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $cotizacion = Cotizacion::findOrFail($id);
            $cotizacion->update(array(
                'garantia'=>$request->garantia,
                'f_pago'=>$request->f_pago,
                'f_entrega'=>$request->f_entrega,
                'observaciones'=>$request->observaciones,
                'califica'=>$request->califica,
                // 'iva'=>$request->iva
            ));

            if(isset($request->items)){
                foreach ($request->items as $item){ 
                    $it=Items::find($item['id']);
                    $sub=round(intval($item['cantidad'])*floatval($item['pivot']['p_unitario']),2);
                    $descVal=round($sub*(intval($item['pivot']['desc_porcentaje'])/100),2);
                    $sub=round($sub-$descVal,2);

                    $iva=0;
                    if($item['pivot']['iva_ap']){
                        // if($request->iva){
                            $iva=round($sub*0.12,2);
                        // }
                        
                    }
                    else{
                        $iva=0;
                    }

                    $total=round($sub+$iva,2);

                    $cotizacion->items()->updateExistingPivot($it,[
                        'p_unitario'=>$item['pivot']['p_unitario'],
                        'desc_porcentaje'=>$item['pivot']['desc_porcentaje'],
                        'desc_valor'=>$descVal,
                        'subtotal'=>$sub,
                        'iva'=>$iva,
                        'iva_ap'=>$item['pivot']['iva_ap'],
                        'total'=>$total
                    ]);
                    $cotizacion->save();
                }
            }
            $proveedor=Proveedor::find($request->empresa);
            $cotizacion->proveedor()->associate($proveedor)->save();

            $ccomp=CuadroComparativo::find($request->cc_id);
            $cotizacion->cuadro_comparativo()->associate($ccomp)->save();

            return $cotizacion->load('items','cuadro_comparativo.proceso','cuadro_comparativo.cotizaciones.items.cpc','proveedor');
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cotizacion  $cotizacion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Cotizacion::destroy($id);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
