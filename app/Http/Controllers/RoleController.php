<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
// use Illuminate\Http\Request;
use Exception;

class RoleController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Listado de roles
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $roles = Role::with('users')->get();
            return $roles;
        } catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros'], 500);
        }
    }

    /**
     * Creacion de roles
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $role = Role::create($request->all());
            $roles = Role::with('users')->get();
            return $roles;
        } catch(Exception $e) {
            return response()->json(['error' => 'no se pudo ingresar registro'], 500);
        }
    }

    /**
     * Informacion de rol por id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $role = Role::findById($id);
            return response()->json(['data' => $role], 200);
        } catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registro'], 500);
        }
    }

    /**
     * Edicion de rol por id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $role = Role::findOrFail($id);
            $role->update($request->all());
            $roles = Role::with('users')->get();
            return $roles;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Eliminacion de rol
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Role::destroy($id);
            $roles = Role::with('users')->get();
            return $roles;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
