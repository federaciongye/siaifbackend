<?php

namespace App\Http\Controllers;

use App\Items;
use App\Proceso;
use App\CuadroComparativo;
use Illuminate\Http\Request;
use App\ProductoCPC;

class ItemsController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api',['except' => []]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $items = Items::all();
            return $items;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $cuadroComp=null;
            $items = Items::create($request->only(
                $request->descripcion
                , $request->cantidad
                , $request->unida
                ,$request->dimension
                ,$request->iva
                ,$request->tipocpc
            ));
            $items->save();

            $proceso=Proceso::find($request->proceso_id);
            $var=$proceso->cuadro_comparativo()->get();
            // return $var;
            if(count($var)==0){
                $cuadroComp = new CuadroComparativo();
                $cuadroComp->observaciones=$request->observaciones;
                $cuadroComp->proceso()->associate($proceso)->save();
                $items->cuadro_comp()->associate($cuadroComp->id)->save();
            }
            else{
                $cuadroComp =CuadroComparativo::find($var[0]->id);
                // return $cuadroComp;
                $items->cuadro_comp()->associate($cuadroComp->id)->save();

                $cotizaciones=$cuadroComp->cotizaciones()->get();
                foreach($cotizaciones as $cot){
                    $cot->load('proveedor',
                    'items.cpc');
                    $cot->items()->attach($items,[
                        'p_unitario'=>"0",
                        'desc_porcentaje'=>"0",
                        'desc_valor'=>"0",
                        'subtotal'=>"0",
                        'iva'=>"0",
                        'total'=>"0"
                    ]);
                    $cot->save();
                }
                // return $cotizaciones;
            }
            

            
            $productoCPC = ProductoCPC::all()->filter(function($record) use ($request) {
                if($record->codigo === $request->productoCPC_id) {
                    return $record;
                }
            })->first();

            $items->save();

            $items->cpc()->associate($productoCPC)->save();

            if(count($var)==0){
                return $cuadroComp->load('cotizaciones.items.cpc','cotizaciones.proveedor','items.cpc','items.cotizaciones','proceso');
            }
            else{
                return $items->load('cotizaciones','cpc','cuadro_comp');
            }
            
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Items  $item
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $item = Items::findOrFail($id);
            return $item;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Items  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $item = Items::findOrFail($id);
            $item->update(array(
                'descripcion'=>$request->descripcion,
                'cantidad'=>$request->cantidad,
                'unidad'=>$request->unidad,
                'dimension'=>$request->dimension,
                'iva'=>$request->iva,
                'tipocpc'=>$request->tipocpc
            ));

            $productoCPC = ProductoCPC::all()->filter(function($record) use ($request) {
                if($record->codigo === $request->productoCPC_id) {
                    return $record;
                }
            })->first();

            $item->cpc()->associate($productoCPC)->save();
            return $item->load('cotizaciones','cpc','cuadro_comp');
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Items  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Items::destroy($id);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
