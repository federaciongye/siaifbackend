<?php

namespace App\Http\Controllers;

use App\DepartamentoProcesos;
use Illuminate\Http\Request;

class DeparamentoProcesosController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $departamentosProcesos = DepartamentoProcesos::all();
            return $departamentosProcesos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $departamentosProcesos = DepartamentoProcesos::create($request->all());
            $departamentosProcesos = DepartamentoProcesos::all();
            return $departamentosProcesos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DepartamentoProcesos  $departamentoProceso
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $departamentoProceso = DepartamentoProcesos::findOrFail($id);
            return $departamentoProceso;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DepartamentoProcesos  $departamentoProceso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $departamentoProceso = DepartamentoProcesos::findOrFail($id);
            $departamentoProceso->update($request->all());
            $departamentosProcesos = DepartamentoProcesos::all();
            return $departamentosProcesos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DepartamentoProcesos  $departamentoProceso
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DepartamentoProcesos::destroy($id);
            $departamentosProcesos = DepartamentoProcesos::all();
            return $departamentosProcesos;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
