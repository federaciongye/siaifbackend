<?php

namespace App\Http\Controllers;

use App\Proceso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Departamento;
use App\ProductoCPC;
use App\User;
use App\TipoUsuarioProceso;
use App\TipoProceso;
use App\Clasificacion;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App;
use Response;
use PDF;
use App\Control;
use App\UserSistemaControl;
use DB;
use Rap2hpoutre\FastExcel\FastExcel;
use Rap2hpoutre\FastExcel\StyleBuilder;
use App\MontoContratacion;




class ProcesoController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api', ['except' => []]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $procesos = Proceso::all();
            return $procesos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Carbon::setLocale('es');

            $re=json_decode($request->proceso);

            $now = Carbon::parse($re->fecha_inicio);
            $now->diffForHumans();

            $dept_code=strtoupper($re->dep_req);

            // $countProceso=Proceso::where('codigo', 'LIKE', "%{$dept_code}%")->get();
            $count=0;
            $countProceso=Proceso::with('clasificacion')->get();

            foreach($countProceso as $cp){
                if(($re->clasificacion=='S' && $cp->clasificacion[0]->codigo=="S")||($re->clasificacion=='O' && $cp->clasificacion[0]->codigo=="O")){
                    $count++;
                }
                if($re->clasificacion=='B' && $cp->clasificacion[0]->codigo=="B"){
                    $count++;
                }
            }
            
            $m;
            switch ($now->month) {
                case 1:
                    $m='Ene';
                    break;
                case 2:
                    $m='Feb';
                    break;
                case 3:
                    $m='Mar';
                    break;
                case 4:
                    $m='Abr';
                    break;
                case 5:
                    $m='May';
                    break;
                case 6:
                    $m='Jun';
                    break;
                case 7:
                    $m='Jul';
                    break;
                case 8:
                    $m='Ago';
                    break;
                case 9:
                    $m='Sept';
                    break;
                case 10:
                    $m='Oct';
                    break;
                case 11:
                    $m='Nov';
                    break;
                case 12:
                    $m='Dic';
                    break;
            }


            $procesos = new Proceso;
            
            $procesos->fecha_inicio = $re->fecha_inicio;
            $procesos->objeto = $re->objeto;
            $procesos->codigo_documento = $re->codigo_documento;
            $procesos->observaciones = $re->observaciones;
            $procesos->codigo="FDG-".$dept_code."-".$now->year."-".$m."-".str_pad($count+1,4,'0',STR_PAD_LEFT);
            // return $procesos;
            $procesos->save();

            $dept=Departamento::where(['codigo'=>$re->dep_req])->first();

            $procesos->departamentos()->attach($dept);

            $procesos->save();

            //Tipo de proceso infima o autogestion
            if($re->tipo=='IC'){
                $tipo1=TipoProceso::where(['codigo'=>'IC'])->first();
                $procesos->tipo_procesos()->attach($tipo1);
            }

            if($re->tipo=='A'){
                $tipo2=TipoProceso::where(['codigo'=>'A'])->first();
                $procesos->tipo_procesos()->attach($tipo2);         
            }

            //Clasificacion Bienes o servicios
            if($re->clasificacion=='S'){
                $clasif1=Clasificacion::where(['codigo'=>'S'])->first();
                $procesos->clasificacion()->attach($clasif1);
            }

            if($re->clasificacion=='B'){
                $clasif2=Clasificacion::where(['codigo'=>'B'])->first();
                $procesos->clasificacion()->attach($clasif2);
            }

            if($re->clasificacion=='O'){
                $clasif2=Clasificacion::where(['codigo'=>'O'])->first();
                $procesos->clasificacion()->attach($clasif2);
            }

            
            $procesos->save();
            

            $user_req=User::find($re->user_req_id);
            $tipoUserProceso1=TipoUsuarioProceso::all()
            ->filter(function($record) {
                if(strpos($record->codigo,"req") !== false) {
                    return $record;
                }
            })->first();
            // return response()->json([
            //     'usereq' => $user_req,
            //     'tuser'=>$tipoUserProceso1
            // ]);
            $procesos->tipo_procesos_usuarios()->attach($user_req,['tipo_usuario_proceso_id' => $tipoUserProceso1->id,'proceso_id' => $procesos->id]);

            $user_admin=User::find($re->admin_req_id);
            $tipoUserProceso2=TipoUsuarioProceso::all()->filter(function($record) {
                if(strpos($record->codigo,"adm") !== false) {
                    return $record;
                    }
            })->first();
            $procesos->tipo_procesos_usuarios()->attach($user_admin,['tipo_usuario_proceso_id' => $tipoUserProceso2->id,'proceso_id' => $procesos->id]);


            $user_enc=User::find($re->pers_encrg_id);
            $tipoUserProceso3=TipoUsuarioProceso::all()->filter(function($record) {
                if(strpos($record->codigo,"enc") !== false) {
                    return $record;
                    }
            })->first();
            $procesos->tipo_procesos_usuarios()->attach($user_enc,['tipo_usuario_proceso_id' => $tipoUserProceso3->id,'proceso_id' => $procesos->id]);


            $user_elab=User::find(Auth::id());
            $tipoUserProceso4=TipoUsuarioProceso::all()->filter(function($record) {
                if(strpos($record->codigo,"elb") !== false) {
                        return $record;
                    }
                    
            })->first();
            $procesos->tipo_procesos_usuarios()->attach($user_elab,['tipo_usuario_proceso_id' => $tipoUserProceso4->id,'proceso_id' => $procesos->id]);


            //User autoriza
            $control= Control::all()->filter(function($record)  {
                if(($record->codigo) == '001') {
                return $record;
            }
            })->first();
    
            $userSisCont=UserSistemaControl::whereHas(
                'control' , function ($query) use ($control){
                    $query->where('id', $control->id);
                }
                
            )->orderBy('created_at', 'desc')->first();
    
            if(isset($userSisCont)){
                $userSisCont->load('SistemaUser.user');
                
                $user_auth=User::find($userSisCont->SistemaUser->user->id);
                $procesos->user_autoriza()->associate($user_auth);
            }

            $procesos->save();

            if(isset($re->cpc)){
                foreach ($re->cpc as $c){ 
                    $procesos->cpc()->attach(ProductoCPC::find($c->id));
    
                }
                $procesos->save();
            }

            // //Tipo de proceso infima o autogestion
            if($re->tipo=='IC'){
                if($request->req_scan){
                    $file      = $request->file('req_scan');
                    $filename  = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $image_uploaded_path = $file->store($now->year.'/infima/'.$dept_code.'/'.$procesos->codigo, 'public');
                    $url=url('/').'/storage/'.$image_uploaded_path;
                    $procesos->autorizado_scan=$url;
                    $procesos->save();
                }
            }
            if($re->tipo=='A'){
                if($request->req_scan){
                    $file      = $request->file('req_scan');
                    $filename  = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $image_uploaded_path = $file->store($now->year.'/autogestion/'.$dept_code.'/'.$procesos->codigo, 'public');
                    $url=url('/').'/storage/'.$image_uploaded_path;
                    $procesos->autorizado_scan=$url;
                    $procesos->save();
                }
            }
            

        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Proceso  $Proceso
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $Proceso = Proceso::findOrFail($id);
            return $Proceso;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Proceso  $Proceso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
         try {
            
            $re=json_decode($request->proceso);
            $now = Carbon::parse($re->fecha_inicio);
            $now->diffForHumans();
            $dept_code=strtoupper($re->dep_req);
            // return $request;
            $Proceso = Proceso::findOrFail($re->id);
            $url="";
            if($re->tipo=='IC'){
                if($request->req_scan){
                    $file      = $request->file('req_scan');
                    $filename  = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $image_uploaded_path = $file->store($now->year.'/infima/'.$dept_code.'/'.$Proceso->codigo, 'public');
                    $url=url('/').'/storage/'.$image_uploaded_path;
                }
            }
            if($re->tipo=='A'){
                if($request->req_scan){
                    $file      = $request->file('req_scan');
                    $filename  = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $image_uploaded_path = $file->store($now->year.'/autogestion/'.$dept_code.'/'.$Proceso->codigo, 'public');
                    $url=url('/').'/storage/'.$image_uploaded_path;
                }
            }
            $Proceso->update(array(
                'fecha_inicio'=>$re->fecha_inicio,
                'codigo_documento'=>$re->codigo_documento,
                'objeto'=>$re->objeto,
                'autorizado_scan'=>$url
            ));
            // $procesos = Proceso::all();
            return $Proceso;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Proceso  $Proceso
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Proceso::destroy($id);
            $procesos = Proceso::all();
            return $procesos;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function getDept(){
        $departamentos = Departamento::with('dep_sup.users.roles','dep_sup.dep_inf','users.roles','dep_inf')->get();
        // return $departamentos;

        return response()->json([
            'departamentos' => $departamentos
        ]);
    }

    public function getCPCS(){
        $pcpc=ProductoCPC::All();

        return response()->json([
            'cpc' => $pcpc
        ]);
    }



    public function getData(){
        $now = Carbon::now();
        $now->diffForHumans();

        $monto=MontoContratacion::where('ano', $now->year)->orderBy('created_at', 'desc')->first();

        $departamentos = Departamento::with('dep_sup.users.roles','dep_sup.dep_inf','users.roles','dep_inf')->get();
        $pcpc=ProductoCPC::with('procesos.resolucion_compra.cotizacion.proveedor','procesos.resolucion_compra.cotizacion.items','procesos.clasificacion','procesos.tipo_procesos_usuarios_get')->get();
        // return $departamentos;

        $control= Control::all()->filter(function($record)  {
            if(($record->codigo) == '001') {
            return $record;
        }
        })->first();

        $userSisCont=UserSistemaControl::whereHas(
            'control' , function ($query) use ($control){
                $query->where('id', $control->id);
            }
            
        )->orderBy('created_at', 'desc')->first();

        if(isset($userSisCont)){
            $userSisCont->load('SistemaUser.user');
        }

        return response()->json([
            'departamentos' => $departamentos,
            'userSisCont' => $userSisCont,
            'cpc' => $pcpc,
            'monto'=>$monto
        ]);
    }

    public function getDataAuto(){
        $now = Carbon::now();
        $now->diffForHumans();


        $departamentos = Departamento::with('dep_sup.users.roles','dep_sup.dep_inf','users.roles','dep_inf')->get();
        // return $departamentos;

        $control= Control::all()->filter(function($record)  {
            if(($record->codigo) == '001') {
            return $record;
        }
        })->first();

        $userSisCont=UserSistemaControl::whereHas(
            'control' , function ($query) use ($control){
                $query->where('id', $control->id);
            }
            
        )->orderBy('created_at', 'desc')->first();

        if(isset($userSisCont)){
            $userSisCont->load('SistemaUser.user');
        }

        return response()->json([
            'departamentos' => $departamentos,
            'userSisCont' => $userSisCont
        ]);
    }

    public function getInfimas(){
        $procesos_activos=Proceso::with(['resolucion_compra.cotizacion.proveedor','cpc.procesos','departamentos','tipo_procesos','clasificacion','tipo_procesos_usuarios_get','cuadro_comparativo.items.cpc.procesos','cuadro_comparativo.items.cotizaciones','cuadro_comparativo.cotizaciones.proveedor','cuadro_comparativo.cotizaciones.items.cpc.procesos'])
        ->whereHas('tipo_procesos', function ($query) {
            return $query->where('codigo', '=', 'IC');
        })->where('activo',1)->get();

        foreach($procesos_activos as $p) {
            foreach($p->tipo_procesos_usuarios_get as $tps){
                $tps->nombre=Crypt::decryptString($tps->nombre);
                $tps->descripcion=Crypt::decryptString($tps->descripcion);
                $tps->codigo=Crypt::decryptString($tps->codigo);
            }

        }

        // $procesos_inactivos=Proceso::with(['resolucion_compra.cotizacion.proveedor','cpc.procesos.resolucion_compra.cotizacion.items','departamentos','tipo_procesos','clasificacion','tipo_procesos_usuarios_get','cuadro_comparativo.items.cpc.procesos.resolucion_compra.cotizacion.items','cuadro_comparativo.items.cotizaciones','cuadro_comparativo.cotizaciones.proveedor','cuadro_comparativo.cotizaciones.items.cpc.procesos.resolucion_compra.cotizacion.items'])
        // ->whereHas('tipo_procesos', function ($query) {
        //     return $query->where('codigo', '=', 'IC');
        // })->where('activo',0)->get();

        // foreach($procesos_inactivos as $p) {
        //     foreach($p->tipo_procesos_usuarios_get as $tps){
        //         $tps->nombre=Crypt::decryptString($tps->nombre);
        //         $tps->descripcion=Crypt::decryptString($tps->descripcion);
        //         $tps->codigo=Crypt::decryptString($tps->codigo);
        //     }

        // }

        return response()->json([
            'activos' => $procesos_activos,
            //'inactivos' => $procesos_inactivos
        ]);

    }

    public function getAutogestion(){
        $procesos_activos=Proceso::with(['resolucion_compra.cotizacion.proveedor','departamentos','tipo_procesos','clasificacion','tipo_procesos_usuarios_get','cuadro_comparativo.items','cuadro_comparativo.items.cotizaciones','cuadro_comparativo.cotizaciones.proveedor','cuadro_comparativo.cotizaciones.items'])
        ->whereHas('tipo_procesos', function ($query) {
            return $query->where('codigo', '=', 'A');
        })->where('activo',1)->get();

        foreach($procesos_activos as $p) {
            foreach($p->tipo_procesos_usuarios_get as $tps){
                $tps->nombre=Crypt::decryptString($tps->nombre);
                $tps->descripcion=Crypt::decryptString($tps->descripcion);
                $tps->codigo=Crypt::decryptString($tps->codigo);
            }

        }

        

        return response()->json([
            'activos' => $procesos_activos,
            // 'inactivos' => $procesos_inactivos
        ]);

    }

    public function searchProc(Request $request){

        if($request->flag==1){
            $procesos_inactivos=Proceso::with(['resolucion_compra.cotizacion.proveedor','departamentos','tipo_procesos','clasificacion','tipo_procesos_usuarios_get','cuadro_comparativo.items','cuadro_comparativo.items.cotizaciones','cuadro_comparativo.cotizaciones.proveedor','cuadro_comparativo.cotizaciones.items'])
            ->whereHas('tipo_procesos', function ($query) {
                return $query->where('codigo', '=', 'A');
            })
            ->where('activo','=',0)
            ->get();
        }
        else{
            $procesos_inactivos=Proceso::with(['resolucion_compra.cotizacion.proveedor','departamentos','tipo_procesos','clasificacion','tipo_procesos_usuarios_get','cuadro_comparativo.items','cuadro_comparativo.items.cotizaciones','cuadro_comparativo.cotizaciones.proveedor','cuadro_comparativo.cotizaciones.items'])
            ->whereHas('tipo_procesos', function ($query) {
                return $query->where('codigo', '=', 'IC');
            })
            ->where('activo','=',0)
            ->get();
        }
       

        $procesos_inactivos2=[];

        foreach($procesos_inactivos as $pi) {
            if(strpos($pi->codigo, $request->search) !== false){
                array_push($procesos_inactivos2,$pi);
            }
            if(strpos($pi->objeto, $request->search) !== false){
                array_push($procesos_inactivos2,$pi);
            }
        }

        foreach($procesos_inactivos2 as $p) {
            foreach($p->tipo_procesos_usuarios_get as $tps){
                $tps->nombre=Crypt::decryptString($tps->nombre);
                $tps->descripcion=Crypt::decryptString($tps->descripcion);
                $tps->codigo=Crypt::decryptString($tps->codigo);
            }

        }
        return $procesos_inactivos2;
    }

    public function verifCPC(Request $request){
        $proceso = Proceso::findOrFail($request->id);
        $proceso->fecha_verif_cpc=$request->fecha_verif;
        $proceso->save();
        $proceso->load('tipo_procesos','tipo_procesos_usuarios_get','resolucion_compra.cotizacion.proveedor','resolucion_compra.cotizacion.items');


        $splt = explode("-", $proceso->codigo);
        $path='storage/'.$splt[2];

        if($proceso->tipo_procesos[0]->codigo=='IC'){
            $path=$path.'/infima/'.$splt[1].'/'.$proceso->codigo.'/'.'VCPC-'.$proceso->codigo.'.pdf';
        }
        else{
            $path=$path.'/autogestion/'.$splt[1].'/'.$proceso->codigo.'/'.'VCPC-'.$proceso->codigo.'.pdf';
        }

        foreach($proceso->tipo_procesos_usuarios_get as $tps){
            $tps->nombre=Crypt::decryptString($tps->nombre);
            $tps->descripcion=Crypt::decryptString($tps->descripcion);
            $tps->codigo=Crypt::decryptString($tps->codigo);
        }
        
        view()->share('data',$proceso);
        $pdf=PDF::loadView('pdf/verificacion_cpc', $proceso)->setPaper('a4', 'portrait')->setWarnings(false)->save(public_path($path));

        $url=url('/').'/'.$path;
        $proceso->verif_cpc_file=$url;
        $proceso->save();

        return $proceso;
    
    }

    public function generarReporte(Request $request){
        $inicio = Carbon::parse($request->fecha_inicio);
        $fin = Carbon::parse($request->fecha_fin);

        // $procesos = Proceso::whereBetween('fecha_inicio', [$inicio, $fin])->get();
        // select('*')
        // ->where('fecha_inicio', '>=', $inicio)
        // ->where('fecha_inicio', '<=', $fin)
        // ->get();

        if($request->tipo=="estado"){
            $procesos=Proceso::with(['cpc','departamentos','tipo_procesos','clasificacion','tipo_procesos_usuarios_get','cuadro_comparativo.items.cpc','cuadro_comparativo.items.cotizaciones','cuadro_comparativo.cotizaciones.proveedor','cuadro_comparativo.cotizaciones.items.cpc','resolucion_compra.cotizacion.items','resolucion_compra.cotizacion.proveedor'])
            ->whereHas('tipo_procesos', function ($query) {
                return $query->where('codigo', '=', 'IC');
            })->where('activo',0)->get()->filter(function($item) use ($inicio,
            $fin){
               if (Carbon::parse($item->fecha_inicio)->between($inicio, $fin)) {
                   return $item;
               }
           });

            foreach($procesos as $p) {
                foreach($p->tipo_procesos_usuarios_get as $tps){
                    $tps->nombre=Crypt::decryptString($tps->nombre);
                    $tps->descripcion=Crypt::decryptString($tps->descripcion);
                    $tps->codigo=Crypt::decryptString($tps->codigo);
                }

            }
        }
        else{
            $procesos=Proceso::with(['resolucion_compra.cotizacion.proveedor','resolucion_compra.cotizacion.items','departamentos','tipo_procesos','clasificacion','tipo_procesos_usuarios_get','cuadro_comparativo.items','cuadro_comparativo.items.cotizaciones','cuadro_comparativo.cotizaciones.proveedor','cuadro_comparativo.cotizaciones.items'])
            ->whereHas('tipo_procesos', function ($query) {
                return $query->where('codigo', '=', 'A');
            })->where('activo',0)->get()->filter(function($item) use ($inicio,
             $fin){
                if (Carbon::parse($item->fecha_inicio)->between($inicio, $fin)) {
                    return $item;
                }
            });

            foreach($procesos as $p) {
                
                foreach($p->tipo_procesos_usuarios_get as $tps){
                   
                    $tps->nombre=Crypt::decryptString($tps->nombre);
                    $tps->descripcion=Crypt::decryptString($tps->descripcion);
                    $tps->codigo=Crypt::decryptString($tps->codigo);
                }
    
            }

        }

        // $procesos =Proceso::all()->filter(function($item) use ($inicio,
        // $fin){
        //     if (Carbon::parse($item->fecha_inicio)->between($inicio, $fin)) {
        //         return $item;
        //     }
        // });
        // if(isset($procesos)){
        //     (new FastExcel($procesos))->export('file.xlsx', function ($proc) {
        //         return [
        //             'Documento' => $proc->codigo_documento
        //         ];
        //     });
        // }

        // return $procesos;

        $list = collect([]);
        $i=0;
        foreach($procesos as $p) {
            $i++;
            $fecha = Carbon::parse($p->fecha_inicio)->format('d/m/Y');
            $cc = Carbon::parse($p->cuadro_comparativo->fecha_aprobado)->format('d/m/Y');
            if(!isset($p->resolucion_compra->cotizacion->proveedor->nombre_comercial) && isset($p->resolucion_compra->cotizacion->proveedor->razon_social)){
                $prov=$p->resolucion_compra->cotizacion->proveedor->razon_social;
            }
            elseif(isset($p->resolucion_compra->cotizacion->proveedor->nombre_comercial) && !isset($p->resolucion_compra->cotizacion->proveedor->razon_social)){
                $prov=$p->resolucion_compra->cotizacion->proveedor->nombre_comercial;
            }
            else{
                $prov=$p->resolucion_compra->cotizacion->proveedor->razon_social;
            }
            $sub=0;
            $desc=0;
            foreach($p->resolucion_compra->cotizacion->items as $it){
                $sub=$sub+$it->pivot->subtotal;
            }
            $elab;
            foreach($p->tipo_procesos_usuarios_get as $us){
                if($us->codigo=="elb") {
                    $elab=$us->name." ".$us->last_name;
                }
                
            }

            $enc;
            foreach($p->tipo_procesos_usuarios_get as $us){
                if($us->codigo=="enc") {
                    $enc=$us->name." ".$us->last_name;
                }
                
            }
            // $desc=($subtotal*$it->pivot->desc_porcentaje)/100;
            $list->push([ 'id' => $i, 
            'DOCUMENTO DE REQUERIMIENTO' => $p->codigo_documento,
            'FECHA DE REQUERIMIENTO'=>$fecha ,
            'OBJETO DE COMPRA'=>$p->objeto,
            'ÁREA SOLICITANTE'=>$p->departamentos[0]->nombre,
            'FECHA CUADRO COMPARATIVO'=>$cc,
            'NOMBRE DEL PROVEEDOR ADJUDICADO'=>$prov,
            'RUC'=>$p->resolucion_compra->cotizacion->proveedor->ruc,
            'SUBTOTAL'=>$sub,
            'TIPO DE COMPRA'=>$p->tipo_procesos[0]->nombre,
            'DERIVADO A'=>$elab,
            'FECHA ORDEN DE COMPRA'=>Carbon::parse($p->clasificacion[0]->pivot->f_aprobado)->format('d/m/Y'),
            'SUPERVISOR DE LA ORDEN'=>$enc,
            'OBSERVACIONES'=>$p->clasificacion[0]->pivot->observaciones,
        ]);
        }

        // $list = collect([
        //     [ 'id' => 1, 'name' => 'Jane' ],
        //     [ 'id' => 2, 'name' => 'John' ],
        // ]);
        
        // return (new FastExcel($list))->download('file3.xlsx');

        // $header_style = (new StyleBuilder())->setFontBold()->build();
        // $rows_style = (new StyleBuilder())
        // ->setFontSize(15)
        // ->setShouldWrapText()
        // ->setBackgroundColor("EDEDED")
        // ->build();

        (new FastExcel($list))->export('storage/file.xlsx');

        return response()->json([
            'url' => url('/').'/storage/file.xlsx'
        ]);

        // return url('/').'/storage/file.xlsx';
    }
}
