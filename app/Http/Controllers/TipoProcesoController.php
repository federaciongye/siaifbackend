<?php

namespace App\Http\Controllers;

use App\TipoProceso;
use Illuminate\Http\Request;

class TipoProcesoController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $tiposprocesos = TipoProceso::all();
            return $tiposprocesos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $tiposprocesos = TipoProceso::create($request->all());
            $tiposprocesos = TipoProceso::all();
            return $tiposprocesos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoProceso  $clasificacion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $clasificacion = TipoProceso::findOrFail($id);
            return $clasificacion;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoProceso  $clasificacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $clasificacion = TipoProceso::findOrFail($id);
            $clasificacion->update($request->all());
            $tiposprocesos = TipoProceso::all();
            return $tiposprocesos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoProceso  $clasificacion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            TipoProceso::destroy($id);
            $tiposprocesos = TipoProceso::all();
            return $tiposprocesos;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
