<?php

namespace App\Http\Controllers;

use App\Proveedor;
use Illuminate\Http\Request;

class ProveedorController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $proveedores = Proveedor::all();
            return $proveedores;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $proveedor = Proveedor::create($request->all());
            // $proveedores = Proveedor::all();
            return $proveedor;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $proveedor = Proveedor::findOrFail($id);
            return $proveedor;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $proveedor = Proveedor::findOrFail($id);
            $proveedor->update($request->all());
            $proveedores = Proveedor::all();
            return $proveedores;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Proveedor::destroy($id);
            $proveedores = Proveedor::all();
            return $proveedores;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function searchProveedor(Request $request){
        $users = Proveedor::where('nombre_comercial', 'like', "%{$request->search}%")
        ->orWhere('razon_social', 'like', '%'.$request->search.'%')
        ->orWhere('nombres', 'like', '%'.$request->search.'%')
        ->orWhere('apellidos', 'like', '%'.$request->search.'%')
        ->orWhere('ruc', 'like', '%'.$request->search.'%')
        ->orWhere('telefono', 'like', '%'.$request->search.'%')
        ->orWhere('celular', 'like', '%'.$request->search.'%')
        ->orWhere('email_personal', 'like', '%'.$request->search.'%')
        ->orWhere('email_empresa', 'like', '%'.$request->search.'%')->get();

        return $users;
    }
}
