<?php

namespace App\Http\Controllers;

use App\ItemCuadroComparativo;
use Illuminate\Http\Request;

class ItemCuadroComparativoController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $ItemsCuadrosComparativos = ItemCuadroComparativo::all();
            return $ItemsCuadrosComparativos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $ItemsCuadrosComparativos = ItemCuadroComparativo::create($request->all());
            $ItemsCuadrosComparativos = ItemCuadroComparativo::all();
            return $ItemsCuadrosComparativos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemCuadroComparativo  $itemCuadroComparativo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $itemCuadroComparativo = ItemCuadroComparativo::findOrFail($id);
            return $itemCuadroComparativo;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemCuadroComparativo  $itemCuadroComparativo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $itemCuadroComparativo = ItemCuadroComparativo::findOrFail($id);
            $itemCuadroComparativo->update($request->all());
            $ItemsCuadrosComparativos = ItemCuadroComparativo::all();
            return $ItemsCuadrosComparativos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemCuadroComparativo  $itemCuadroComparativo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            ItemCuadroComparativo::destroy($id);
            $ItemsCuadrosComparativos = ItemCuadroComparativo::all();
            return $ItemsCuadrosComparativos;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
