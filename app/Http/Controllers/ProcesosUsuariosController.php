<?php

namespace App\Http\Controllers;

use App\ProcesosUsuarios;
use Illuminate\Http\Request;

class ProcesosUsuariosController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $procesosUsuarios = ProcesosUsuarios::all();
            return $procesosUsuarios;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $procesosUsuarios = ProcesosUsuarios::create($request->all());
            $procesosUsuarios = ProcesosUsuarios::all();
            return $procesosUsuarios;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProcesosUsuarios  $procesoUsuario
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $procesoUsuario = ProcesosUsuarios::findOrFail($id);
            return $procesoUsuario;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProcesosUsuarios  $procesoUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $procesoUsuario = ProcesosUsuarios::findOrFail($id);
            $procesoUsuario->update($request->all());
            $procesosUsuarios = ProcesosUsuarios::all();
            return $procesosUsuarios;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProcesosUsuarios  $procesoUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            ProcesosUsuarios::destroy($id);
            $procesosUsuarios = ProcesosUsuarios::all();
            return $procesosUsuarios;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
