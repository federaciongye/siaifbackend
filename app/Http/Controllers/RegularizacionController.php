<?php

namespace App\Http\Controllers;

use App\Regularizacion;
use Illuminate\Http\Request;

class RegularizacionController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $regularizaciones = Regularizacion::all();
            return $regularizaciones;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $regularizaciones = Regularizacion::create($request->all());
            $regularizaciones = Regularizacion::all();
            return $regularizaciones;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Regularizacion  $regularizacion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $regularizacion = Regularizacion::findOrFail($id);
            return $regularizacion;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Regularizacion  $regularizacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $regularizacion = Regularizacion::findOrFail($id);
            $regularizacion->update($request->all());
            $regularizaciones = Regularizacion::all();
            return $regularizaciones;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Regularizacion  $regularizacion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Regularizacion::destroy($id);
            $regularizaciones = Regularizacion::all();
            return $regularizaciones;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
