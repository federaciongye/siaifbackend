<?php

namespace App\Http\Controllers;

use App\ProcesoProductoCPC;
use Illuminate\Http\Request;

class ProcesoProductoCPCController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $procesosProductosCPCs = ProcesoProductoCPC::all();
            return $procesosProductosCPCs;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $procesosProductosCPCs = ProcesoProductoCPC::create($request->all());
            $procesosProductosCPCs = ProcesoProductoCPC::all();
            return $procesosProductosCPCs;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProcesoProductoCPC  $procesoProductoCPC
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $procesoProductoCPC = ProcesoProductoCPC::findOrFail($id);
            return $procesoProductoCPC;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProcesoProductoCPC  $procesoProductoCPC
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $procesoProductoCPC = ProcesoProductoCPC::findOrFail($id);
            $procesoProductoCPC->update($request->all());
            $procesosProductosCPCs = ProcesoProductoCPC::all();
            return $procesosProductosCPCs;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProcesoProductoCPC  $procesoProductoCPC
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            ProcesoProductoCPC::destroy($id);
            $procesosProductosCPCs = ProcesoProductoCPC::all();
            return $procesosProductosCPCs;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
