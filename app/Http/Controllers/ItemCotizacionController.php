<?php

namespace App\Http\Controllers;

use App\ItemCotizacion;
use Illuminate\Http\Request;

class ItemCotizacionController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $itemCotizaciones = ItemCotizacion::all();
            return $itemCotizaciones;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $itemCotizaciones = ItemCotizacion::create($request->all());
            $itemCotizaciones = ItemCotizacion::all();
            return $itemCotizaciones;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemCotizacion  $itemCotizacion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $itemCotizacion = ItemCotizacion::findOrFail($id);
            return $itemCotizacion;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemCotizacion  $itemCotizacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $itemCotizacion = ItemCotizacion::findOrFail($id);
            $itemCotizacion->update($request->all());
            $itemCotizaciones = ItemCotizacion::all();
            return $itemCotizaciones;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemCotizacion  $itemCotizacion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            ItemCotizacion::destroy($id);
            $itemCotizaciones = ItemCotizacion::all();
            return $itemCotizaciones;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
