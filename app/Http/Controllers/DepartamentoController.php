<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departamento;
use Exception;

class DepartamentoController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Listado de departamentos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $departamentos = Departamento::with('dep_sup')->get();
            return $departamentos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Creacion de departamento.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $departamento = Departamento::create($request->all());
            $departamentos = Departamento::with('dep_sup')->get();
            return $departamentos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Informacion de departamento por id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $departamento = Departamento::findOrFail($id);
            return $departamento;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Edicion de departamento por id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {

            $departamento = Departamento::findOrFail($id);
            $departamento->update($request->all());
            $departamentos = Departamento::with('dep_sup')->get();
            return $departamentos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Eliminacion de departamento por id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            Departamento::destroy($id);
            $departamentos = Departamento::with('dep_sup')->get();
            return $departamentos;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
