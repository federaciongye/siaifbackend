<?php

namespace App\Http\Controllers;

use App\TipoProcesoProcesos;
use Illuminate\Http\Request;

class TipoProcesoProcesosController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $tipoProcesoProcesos = TipoProcesoProcesos::all();
            return $tipoProcesoProcesos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $tipoProcesoProcesos = TipoProcesoProcesos::create($request->all());
            $tipoProcesoProcesos = TipoProcesoProcesos::all();
            return $tipoProcesoProcesos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoProcesoProcesos  $tipoProcesoProceso
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $tipoProcesoProceso = TipoProcesoProcesos::findOrFail($id);
            return $tipoProcesoProceso;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoProcesoProcesos  $tipoProcesoProceso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $tipoProcesoProceso = TipoProcesoProcesos::findOrFail($id);
            $tipoProcesoProceso->update($request->all());
            $tipoProcesoProcesos = TipoProcesoProcesos::all();
            return $tipoProcesoProcesos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoProcesoProcesos  $tipoProcesoProceso
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            TipoProcesoProcesos::destroy($id);
            $tipoProcesoProcesos = TipoProcesoProcesos::all();
            return $tipoProcesoProcesos;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
