<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Control extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'control';

    //Atributos de control
    protected $fillable = [
        'nombre',
    'descripcion',
    'codigo'];


    //Relacion con User Sistema Control
    public function UserSistemaControl()
    {
        //Relación de control tienes muchos UserSistemaControl
        return $this->hasMany('App\UserSistemaControl');
    }

    public function setNombreAttribute($value) {
        $this->attributes['nombre'] = Crypt::encryptString($value);
    }

    public function getNombreAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    public function setDescripcionAttribute($value) {
        $this->attributes['descripcion'] = Crypt::encryptString($value);
    }

    public function getDescripcionAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }
    
    public function setCodigoAttribute($value) {
        $this->attributes['codigo'] = Crypt::encryptString($value);
    }

    public function getCodigoAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }
}
