<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoProcesoProcesos extends Model
{
    //
    //Referencia a la tabla en BD
    protected $table = 'tipo_proceso_procesos';

    //Atributos de TipoProcesoProcesos
    protected $fillable = [
        'tipo_proceso_id',
    'proceso_id'];

    //Relacion a proceso
    public function proceso()
	{
        //Relación de TipoProcesoProcesos pertenece a proceso
		return $this->belongsTo('App\Proceso');
    }

    //Relacion a TipoProceso
    public function TipoProceso()
	{
        //Relación de TipoProcesoProcesos pertenece a TipoProceso
		return $this->belongsTo('App\TipoProceso');
    }
}
