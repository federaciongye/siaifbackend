<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSistemaControl extends Model
{
    //
     //Referencia a la tabla en BD
     protected $table = 'user_sistemas_control';

     //Atributos de UserSistemaControl
     protected $fillable = [
         'sistema_user_id',
     'control_id'];
 
     //Relacion con SistemaUser
     public function SistemaUser()
     {
         //Relación de UserSistemaControl pertenece a un SistemaUser
         return $this->belongsTo('App\UserSistema');
     }

     //Relacion con control
     public function control()
     {
         //Relación de UserSistemaControl pertenece a un control
         return $this->belongsTo('App\Control');
     }
}
