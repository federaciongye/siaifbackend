<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regularizacion extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'regularizaciones';

    //Atributos de regularizaciones
    protected $fillable = [
        'codigo',
    'req_scan'];
}
