<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoTicket extends Model
{
     //Referencia a la tabla en BD
    protected $table = 'tipos_ticket';

    //Atributos de TipoTicket
    protected $fillable = [
        'nombre',
    'descripcion'];
}
