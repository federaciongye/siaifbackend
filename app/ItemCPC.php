<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCPC extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'item_cpc';

    //Atributos de ItemCPC
    protected $fillable = [
        'producto_cpc_id',
    'item_id'];

    //Relacion a proceso
    public function ProductoCPC()
	{
        //Relación de ItemCPC pertenece a ProductoCPC
		return $this->belongsTo('App\ProductoCPC');
    }

    //Relacion a item
    public function item()
	{
        //Relación de ItemCPC pertenece a Items
		return $this->belongsTo('App\Items');
    }
}
