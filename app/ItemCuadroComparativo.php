<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCuadroComparativo extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'items_cuadro_comparativo';

    //Atributos de Cotizacion
    protected $fillable = [
        'item_id',
    'cuadro_comparativo_id'
   ];

   //Relacion a CuadroComparativo
   public function cuadro_comparativo()
   {
       //Relación de ItemCuadroComparativo pertenece a CuadroComparativo
       return $this->belongsTo('App\CuadroComparativo');
   }

   //Relacion a item
   public function item()
   {
       //Relación de ItemCuadroComparativo pertenece a Items
       return $this->belongsTo('App\Items');
   }
}
