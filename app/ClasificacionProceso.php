<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClasificacionProceso extends Model
{
    //
    //Referencia a la tabla en BD
    protected $table = 'clasificacion_procesos';

    //Atributos de UserSistemaControl
    protected $fillable = [
        'clasificacion_id',
    'proceso_id',
    'plazo',
    'f_pago',
    'f_emisión',
    'f_aprobado',
    'observaciones',
    'file_ordenes',
    ];


    //Relacion con proceso
    public function proceso()
    {
        //Relación de ClasificacionProceso pertenece a un proceso
        return $this->belongsTo('App\Proceso');
    }

     //Relacion con proceso
     public function clasificacion()
     {
         //Relación de ClasificacionProceso pertenece a una Clasificacion
         return $this->belongsTo('App\Clasificacion');
     }
}
