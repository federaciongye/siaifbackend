<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MontoContratacion extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'montos_contratacion';

    //Atributos de TipoProceso
    protected $fillable = [
        'ano',
    'monto'];

    //Relacion con Producto Monto Contratacion
    public function ProductoMontoContratacion()
    {
        //Relación de control tienes muchos UserSistemaControl
        return $this->hasMany('App\ProductoMontoContratacion');
    }

    // public function setAnoAttribute($value) {
    //     $this->attributes['ano'] = Crypt::encryptString($value);
    // }

    // public function getAnoAttribute($value) {
    //     if(!is_null($value)){
    //         return Crypt::decryptString($value);
    //     }
    //     else{
    //         return '';
    //     }
    // }

    // public function setMontoAttribute($value) {
    //     $this->attributes['monto'] = Crypt::encryptString($value);
    // }

    // public function getMontoAttribute($value) {
    //     if(!is_null($value)){
    //         return Crypt::decryptString($value);
    //     }
    //     else{
    //         return '';
    //     }
    // }
}
