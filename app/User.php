<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Tymon\JWTAuth\Contracts\JWTSubject;

use Illuminate\Support\Facades\Crypt;

use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable,HasRoles;

    protected $guard_name = 'api';

    /**
     * Atributos de usuario.
     *
     * @var array
     */
    protected $fillable = [
        
        'name',
        'last_name', 
        'email', 
        'password',
        'telef',
        'direccion',
        'cedula',
        'departamento_id',
        'cargo',
        'user_sup'
    ];

    //Relacion con departamentos
    public function departamento()
	{
        //Relación de usuario pertenece a un departamento
		return $this->belongsTo('App\Departamento','departamento_id');
    }

    //Relacion con Tickets de soporte
    public function tickets(){
        //Relación de usuario que tiene muchos tickets
        return $this->hasMany('App\Ticket');
    }

    //Relacion con comentarios
    public function comentarios(){
        //Relación de usuario que tiene muchos comentarios
        return $this->hasMany('App\Comentario');
    }

    //Relacion con sistemas
    public function sistemas()
    {
        //Relación de usuario que tiene accesos a varios sistemas.
        return $this->belongsToMany('App\Sistema','App\UserSistema')->withPivot('sistema_id','user_id')->orderBy('id', 'ASC');
    }

    //Relacion con jefe
    public function jefe()
    {
        //Relación de usuario con usuario jefe.
        return $this->belongsTo('App\User','user_sup');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getNameAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($this->attributes['name']);
        }
        else{
            return '';
        }
    }

    public function setNameAttribute($value) {
        $this->attributes['name'] = Crypt::encryptString($value);
    }

    public function getLastNameAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    public function setLastNameAttribute($value) {
        $this->attributes['last_name'] = Crypt::encryptString($value);
    }

    public function getTelefAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    public function setTelefAttribute($value) {
        $this->attributes['telef'] = Crypt::encryptString($value);
    }

    public function getDireccionAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    public function setDireccionAttribute($value) {
        $this->attributes['direccion'] = Crypt::encryptString($value);
    }

    public function getCedulaAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    public function setCedulaAttribute($value) {
        $this->attributes['cedula'] = Crypt::encryptString($value);
    }

    public function getDepIdAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    public function setDepIdAttribute($value) {
        $this->attributes['dep_id'] = Crypt::encryptString($value);
    }


    public function getCargoAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    public function setCargoAttribute($value) {
        $this->attributes['cargo'] = Crypt::encryptString($value);
    }

    // public function tipo_user_proceso(){
    //     return $this->belongsToMany('App\TipoUsuarioProceso',
    //     'procesos_usuarios','proceso_id','tipo_usuario_proceso_id')
    //             ->withPivot('user_id')
    //             ->join('users','user_id','=','users.id')->withTimestamps();
    //     // return $this->hasOneThrough('App\ProcesosUsuarios', 'App\TipoUsuarioProceso');
    // }

    public function tipo_user_proceso(){
        return $this->belongsToMany('App\TipoUsuarioProceso'
        ,'procesos_usuarios'
        ,'user_id','tipo_usuario_proceso_id')
            ->withPivot('proceso_id')
            ->join('procesos','proceso_id','=','procesos.id');
    }
}
