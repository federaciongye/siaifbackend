<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSistemaDerecho extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'sistema_user_derecho';

    //Atributos de UserSistemaDerecho
    protected $fillable = [
        'derecho_id', 
    'user_sistema_id'];

    //Relacion con derechos
    public function derechos()
    {
        //Relación de UserSistemaDerecho que tiene muchos derechos
        return $this->hasMany('App\Derecho');
    }
}
