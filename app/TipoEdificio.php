<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoEdificio extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'tipos_edificio';

    //Atributos de TipoEdificio
    protected $fillable = [
        'nombre',
    'descripcion'];
}
