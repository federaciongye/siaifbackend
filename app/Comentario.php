<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'comentarios';

    //Atributos de Comentario
    protected $fillable = [
        'comentario',
    'calificacion', 
    'user_id', 
    'ticket_estado_id'];

    //Relacion con usuario
    public function user()
	{
        //Relación de Comentario pertenece a un usuario
		return $this->belongsTo('App\User');
    }

    //Relacion con TicketEstadoTicket
    public function estado_ticket()
	{
        //Relación de Comentario pertenece a TicketEstadoTicket
		return $this->belongsTo('App\TicketEstadoTicket');
    }

    //Relacion con Adjunto
    public function adjuntos()
    {
        //Relación de Comentario tienes muchos adjuntos
        return $this->hasMany('App\Adjunto');
    }
}
