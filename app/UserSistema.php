<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class UserSistema extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'sistema_user';

    //Atributos de UserSistema
    protected $fillable = [
        'user_id', 
    'sistema_id'];

    //Relacion con sistema
    public function sistema()
	{
        //Relación de UserSistema que pertenece a un sistema
		return $this->belongsTo('App\Sistema');
    }

    //Relacion con usuario
    public function user()
	{
        //Relación de UserSistema que pertenece a un usuario
		return $this->belongsTo('App\User');
    }

    //Relacion con derechos
    public function derechos()
	{
        //Relación de UserSistema que pertenece a varios derechos
		return $this->belongsToMany('App\Derecho','App\UserSistemaDerecho');
    }

    //Relacion con User Sistema Control
    public function UserSistemaControl()
    {
        //Relación de control tienes muchos UserSistemaControl
        return $this->hasMany('App\UserSistemaControl');
    }
}
