<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Crypt;

class ProductoCPC extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'productos_cpc';

    //Atributos de ProductoCPC
    protected $fillable = [
        'codigo',
    'descripcion',
    'catalogado',
    'unblocked'];

    //codigo
    public function getCodigoAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($this->attributes['codigo']);
        }
        else{
            return '';
        }
    }

    public function setCodigoAttribute($value) {
        $this->attributes['codigo'] = Crypt::encryptString($value);
    }

    //Descripcion
    public function getDescripcionAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($this->attributes['descripcion']);
        }
        else{
            return '';
        }
    }

    public function setDescripcionAttribute($value) {
        $this->attributes['descripcion'] = Crypt::encryptString($value);
    }

    //Catalogado
    public function getCatalogadoAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($this->attributes['catalogado']);
        }
        else{
            return '';
        }
    }

    public function setCatalogadoAttribute($value) {
        $this->attributes['catalogado'] = Crypt::encryptString($value);
    }

    public function item_cuad_comp()
    {
        return $this->hasOne('App\Items', 'cpc_id');
    }

    public function procesos(){
        return $this->belongsToMany('App\Proceso', 
        'App\ProcesoProductoCPC',
        'producto_cpc_id','proceso_id')->withTimestamps();

    }
}
