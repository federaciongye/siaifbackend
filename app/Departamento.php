<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'departamentos';

    //Atributos de Departamento
    protected $fillable = [
        'nombre',
    'descripcion', 
    'dep_sup_id', 
    'edificio_id',
    'codigo'];

    //Relacion a departamento superior
    public function dep_sup()
	{
        //Relación de Departamento pertenece a departamento
		return $this->belongsTo('App\Departamento');
    }

    //Relacion a departamento superior
    public function dep_inf()
	{
        //Relación de Departamento pertenece a departamento
		return $this->hasMany('App\Departamento','dep_sup_id');
    }

    //Relacion a Edificio
    public function edificio()
	{
        //Relación de Departamento pertenece a Edificio
		return $this->belongsTo('App\Edificio');
    }

    //Relacion a usuarios
    public function users()
    {
        //Relación de Departamento tiene muchos usuarios
        return $this->hasMany('App\User');
    }

    //Relacion con User DepartamentoProcesos Control
    public function DepartamentoProcesos()
    {
        //Relación de departamento tiene muchos DepartamentoProcesos
        return $this->hasMany('App\DepartamentoProcesos');
    }
}
