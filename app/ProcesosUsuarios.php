<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcesosUsuarios extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'procesos_usuarios';

    //Atributos de procesos_usuarios
    protected $fillable = [
        'user_id',
    'proceso_id', 
    'tipo_usuario_proceso_id'];

    //Relacion a usuario
    public function user()
	{
        //Relación de procesos_usuarios pertenece a usuario
		return $this->belongsTo('App\User');
    }

    //Relacion a proceso
    public function proceso()
	{
        //Relación de procesos_usuarios pertenece a proceso
		return $this->belongsTo('App\Proceso');
    }

    //Relacion a tipo_usuario_proceso
    public function tipo_usuario_proceso()
	{
        //Relación de procesos_usuarios pertenece a tipo_usuario_proceso
		return $this->belongsTo('App\TipoUsuarioProceso');
    }
}
