<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    
    //Referencia a la tabla en BD
    protected $table = 'tickets';

    //Atributos de Tickets
    protected $fillable = [
        'titulo',
        'codigo',
    'descripcion', 
    'activo', 
    'evento', 
    'fecha_evento',
    'user_id',
    'tech_id',
    'tipo_ticket_id'];

    //Relacion con usuario
    public function user()
	{
        //Relación de Ticket pertenece a un usuario
		return $this->belongsTo('App\User','user_id');
    }

    //Relacion con tecnico
    public function tech()
	{
        //Relación de Ticket pertenece a un usuario tecnico
		return $this->belongsTo('App\User','tech_id');
    }

    //Relacion con TipoTicket
    public function tipo_ticket()
	{
        //Relación de Ticket pertenece a un TipoTicket
		return $this->belongsTo('App\TipoTicket');
    }
    
    //Relacion con TicketReporte
    public function ticket_reporte()
    {
        //Relación de Ticket tiene muchos TicketReporte
        return $this->hasMany('App\TicketReporte');
    }

    //Relacion con Reporte
    public function reportes()
    {
        //Relación de Ticket pertenece a muchos reportes
        return $this->belongsToMany(Reporte::class, 'tickets_reporte');
    }

    //Relacion con Estado Ticket
    public function estado_ticket()
    {
        //Relación de Ticket pertenece a muchos reportes
        return $this->belongsToMany('App\EstadoTicket', 'App\TicketEstadoTicket')->latest();
    }
}
