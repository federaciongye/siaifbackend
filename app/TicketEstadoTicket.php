<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketEstadoTicket extends Model
{
    
    //Referencia a la tabla en BD
    protected $table = 'ticket_estado_ticket';

    //Atributos de TicketEstadoTicket
    protected $fillable = [
        'estado_ticket_id',
    'ticket_id', 
    'tech_id', 
    'admin_id'];

    //Relacion con ticket
    public function ticket()
	{
        //Relación de TicketEstadoTicket pertenece a un ticket
		return $this->belongsTo('App\Ticket');
    }

    //Relacion con ticket_estado
    public function ticket_estado()
	{
        //Relación de TicketEstadoTicket pertenece a un EstadoTicket
		return $this->belongsTo('App\EstadoTicket');
    }

    //Relacion con usuario experto
    public function tech()
	{
        //Relación de TicketEstadoTicket pertenece a un usuario
		return $this->belongsTo(User::class,'tech_id');
    }

    //Relacion con usuario admin
    public function admin()
	{
        //Relación de TicketEstadoTicket pertenece a un usuario admin
		return $this->belongsTo(User::class,'admin_id');
    }

    //Relacion con comentarios
    public function comentarios()
    {
        //Relación de TicketEstadoTicket tiene muchos comentarios
        return $this->hasMany('App\Comentario');
    }
}
