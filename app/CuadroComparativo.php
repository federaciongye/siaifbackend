<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuadroComparativo extends Model
{
     //Referencia a la tabla en BD
     protected $table = 'cuadro_comparativo';

     //Atributos de CuadroComparativo
     protected $fillable = [
         'proceso_id',
     'observaciones',
     'file',
     'file_resolucion',
    'fecha_aprobado'];
     
     public function proceso()
    {
        return $this->belongsTo('App\Proceso', 'proceso_id');
    }

    //Relacion de items a traves de items_cuadro_comparativo
    public function items()
    {
        return $this->hasMany('App\Items','cuadro_comp_id');
    }

    //Relacion de cotizaciones
    public function cotizaciones()
    {
        return $this->hasMany('App\Cotizacion','cuadro_comparativo_id');
    }
}
