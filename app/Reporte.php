<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reporte extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'reportes';

    //Atributos de Reporte
    protected $fillable = [
        'linkarchivo',
    'descripcion'];

    //Relacion con Tickets de soporte
    public function tickets()
    {
        //Relación de Reporte que pertenece a muchos tickets
        return $this->belongsToMany(Ticket::class, 'tickets_reporte');
    }
}
