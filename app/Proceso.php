<?php

namespace App;

use Illuminate\Support\Facades\Crypt;

use Illuminate\Database\Eloquent\Model;

class Proceso extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'procesos';

    //Atributos de TipoProcesoProcesos
    protected $fillable = [
        'fecha_inicio',
    'objeto',
    'codigo',
    'observaciones',
    'autorizado_scan',
    'regularizacion_id',
    'user_revisa_id',
    'user_autoriza_id',
    'fecha_verif_cpc',
    'verif_cpc_file'];


    public function user_revisa()
    {
        return $this->belongsTo('App\User', 'user_revisa_id');
    }

    public function cuadro_comparativo()
    {
        return $this->hasOne('App\CuadroComparativo');
    }

    public function resolucion_compra()
    {
        return $this->hasOne('App\ResolucionCompra');
    }

    public function user_autoriza()
    {
        return $this->belongsTo('App\User', 'user_autoriza_id');
    }

    public function regularizacion()
    {
        return $this->belongsTo('App\Regularizacion', 'regularizacion_id');
    }

    public function departamentos()
    {
        return $this->belongsToMany('App\Departamento', 'App\DepartamentoProcesos')->withTimestamps();
    }

    public function clasificacion()
    {
        return $this->belongsToMany('App\Clasificacion',
        'App\ClasificacionProceso')->withPivot('plazo','f_pago','f_emisión','f_aprobado','observaciones','file_ordenes')->withTimestamps();
    }

    public function tipo_procesos_usuarios(){
        return $this->belongsToMany('App\User',
        'procesos_usuarios')
                ->withPivot('proceso_id','user_id','tipo_usuario_proceso_id')->withTimestamps();
    }

    public function tipo_procesos_usuarios_get(){
        return $this->belongsToMany('App\User'
        ,'procesos_usuarios'
        ,'proceso_id','user_id')
            ->withPivot('tipo_usuario_proceso_id')
            ->join('tipo_usuario_proceso','tipo_usuario_proceso_id','=','tipo_usuario_proceso.id')
            ->select('*')
            ;
    }

    public function tipo_procesos()
    {
        // return $this->belongsToMany('App\TipoProceso', 
        // 'tipo_proceso_procesos')
        // ->withPivot('proceso_id')->withTimestamps();
        return $this->belongsToMany('App\TipoProceso', 'App\TipoProcesoProcesos')->withTimestamps();

    }
    
    public function cpc(){
        return $this->belongsToMany('App\ProductoCPC', 
        'App\ProcesoProductoCPC','proceso_id',
        'producto_cpc_id')->withTimestamps();

    }

    public function getFechaInicioAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    public function setFechaInicioAttribute($value) {
        $this->attributes['fecha_inicio'] = Crypt::encryptString($value);
    }

    public function getobjetoAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    public function setobjetoAttribute($value) {
        $this->attributes['objeto'] = Crypt::encryptString($value);
    }

    public function getcodigoAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    public function setcodigoAttribute($value) {
        $this->attributes['codigo'] = Crypt::encryptString($value);
    }

    public function getobservacionesAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    public function setobservacionesAttribute($value) {
        $this->attributes['observaciones'] = Crypt::encryptString($value);
    }

    public function getAutorizadoScanAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    public function setAutorizadoScanAttribute($value) {
        $this->attributes['autorizado_scan'] = Crypt::encryptString($value);
    }

    public function getFechaVerifCpcAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    public function setFechaVerifCpcAttribute($value) {
        $this->attributes['fecha_verif_cpc'] = Crypt::encryptString($value);
    }

    public function getVerifCpcFileAttribute($value) {
        if(!is_null($value)){
            return Crypt::decryptString($value);
        }
        else{
            return '';
        }
    }

    public function setVerifCpcFileAttribute($value) {
        $this->attributes['verif_cpc_file'] = Crypt::encryptString($value);
    }

}
