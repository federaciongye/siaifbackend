<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartamentoProcesos extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'departamento_procesos';

    //Atributos de DepartamentoProcesos
    protected $fillable = [
        'departamento_id',
    'proceso_id'];

    //Relacion con UserSistema
    public function SistemaUser()
    {
        //Relación de DepartamentoProcesos pertenece a un UserSistema
        return $this->belongsTo('App\UserSistema');
    }

    //Relacion con control
    public function control()
    {
        //Relación de DepartamentoProcesos pertenece a un control
        return $this->belongsTo('App\Control');
    }
}
