<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoTicket extends Model
{
    //Referencia a la tabla en BD
    protected $table = 'estados_ticket';

    //Atributos de EstadoTicket
    protected $fillable = [
        'nombre',
    'descripcion'];
}
